//
//  FTCCenterPanelSegue.h
//  Fate Companion
//
//  Created by Joshua Barron on 2/8/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CHARACTER_DETAILS_PANEL @"characterDetailPanel"

@interface FTCCenterPanelSegue : UIStoryboardSegue

@property (strong) NSString *panelName;
//@property bool setDelegate;

@end
