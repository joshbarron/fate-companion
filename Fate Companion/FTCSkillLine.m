//
//  FTCSkillLine.m
//  Fate Companion
//
//  Created by Joshua Barron on 2/15/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCSkillLine.h"
#import "FTCDetailSectionBox.h"
#import "FTCSkillValueTranslator.h"
#import "FontAwesomeKit/FAKIonIcons.h"
#import "FontAwesomeKit/FAKFontAwesome.h"
#import "UIControl+BlocksKit.h"
#import "UIColor+LightAndDark.h"
#import "FTCLayoutHelper.h"
#import "FTCNotificationHelper.h"
#import "SIAlertView.h"

#define GLYPH_FONT @"FateCoreGlyphs"

#define HEIGHT 60

@interface FTCSkillLine()

@property MGBox *mainBox;

@end

@implementation FTCSkillLine
{
    FTCSkillValueTranslator *skillValueTranslator;
}
@synthesize skill = _skill;
@synthesize mainBox = _mainBox;
@synthesize delegate = _delegate;
@synthesize lineWidth = _lineWidth;

- (void) layout
{
    for (MGLine *line in self.mainBox.boxes) {
        [line setSize:(CGSize){self.lineWidth - 35, line.size.height}];
    }
    [self.mainBox setSize:(CGSize){self.lineWidth - 35, self.mainBox.size.height}];
    [self setSize:(CGSize){self.lineWidth, HEIGHT}];
    //[self printWidths:self withDepth:0];
    //[self layoutSubviews];
    [super layout];
}

- (void) printWidths:(UIView *)view withDepth:(int)depth;
{
    int spaceDepth = depth * 4;
    NSLog(@"%*s- %f", spaceDepth, "", view.width);
    
    for (UIView *subView in view.subviews) {
        [self printWidths:subView withDepth:depth + 1];
    }
}

+ (FTCSkillLine *)skillLineWithSkill:(FTCSkill *)skill delegate:(id<FTCSkillLineDelegate>)delegate
{
    double totalWidth = [FTCLayoutHelper getRowWidth:NO] - 8;
    CGSize size = (CGSize){totalWidth, HEIGHT};
    CGSize mainSize = CGSizeMake(size.width - 35, size.height / 2);
    
    FAKIcon *settingsIcon = [FAKIonIcons ios7GearOutlineIconWithSize:24];
    [settingsIcon addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor]];
    UIImage *settingsIconImage = [settingsIcon imageWithSize:(CGSize){35, 25}];
    
    UIButton *settingsButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, settingsIconImage.size.width, settingsIconImage.size.height)];
    [settingsButton setBackgroundImage:settingsIconImage forState:UIControlStateNormal];
    settingsButton.showsTouchWhenHighlighted = YES;
    
    FAKIcon *deleteIcon = [FAKIonIcons ios7CloseOutlineIconWithSize:24];
    [deleteIcon addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]];
    UIImage *deleteIconImage = [deleteIcon imageWithSize:(CGSize){35, 25}];
    
    UIButton *actionButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 30, deleteIconImage.size.width, deleteIconImage.size.height)];
    [actionButton setBackgroundImage:deleteIconImage forState:UIControlStateNormal];
    actionButton.showsTouchWhenHighlighted = YES;
    
    
    MGBox *imgBox = [MGBox boxWithSize:(CGSize){35, HEIGHT}];
    [imgBox addSubview:settingsButton];
    [imgBox addSubview:actionButton];

    NSMutableString *boldedName = [[NSMutableString alloc] initWithString:@"**"];
    [boldedName appendString:skill.name];
    [boldedName appendString:@"**|mush"];
    
    MGLine *mainLine = [MGLine lineWithMultilineLeft:boldedName right:nil width:mainSize.width minHeight:mainSize.height];
    mainLine.bottomPadding = 2;
    mainLine.sidePrecedence = MGSidePrecedenceRight;
    [mainLine sizeToFit];
    
    UIButton *rollDice = [[UIButton alloc] initWithFrame:CGRectZero];

    UIFont *glyphFont = [UIFont fontWithName:GLYPH_FONT size:20];
    UIColor *tintColor = [FTCLayoutHelper getTintColor];
    NSMutableAttributedString *buttonText = [[NSMutableAttributedString alloc] initWithString:@"+ Roll Dice" attributes:@{NSForegroundColorAttributeName : tintColor}];
    [buttonText addAttribute:NSFontAttributeName value:glyphFont range:NSMakeRange(0, 1)];
    [buttonText addAttribute:NSBaselineOffsetAttributeName value:@(2) range:NSMakeRange(0, 1)];
    
    [rollDice setAttributedTitle:buttonText forState:UIControlStateNormal];
    [rollDice sizeToFit];
    rollDice.showsTouchWhenHighlighted = YES;
    MGLine *subLine = [MGLine lineWithLeft:nil right:rollDice size:mainSize];
    
    MGBox *mainBox = [MGBox boxWithSize:(CGSize){mainSize.width, mainSize.height * 2}];
    mainBox.contentLayoutMode = MGLayoutTableStyle;
    [mainBox.boxes addObject:mainLine];
    [mainBox.boxes addObject:subLine];
    
    FTCSkillLine *skillLine = [FTCSkillLine lineWithLeft:mainBox right:imgBox size:size];
    [skillLine setSkillDetails:skill];
    skillLine.delegate = delegate;
    skillLine.mainBox = mainBox;
    skillLine.lineWidth = totalWidth;
    
    [settingsButton bk_addEventHandler:^(id sender) {
        [skillLine.delegate skillLineWasTapped:skillLine];
    } forControlEvents:UIControlEventTouchUpInside];
    
    [actionButton bk_addEventHandler:^(id sender) {
        NSString *message = [NSString stringWithFormat:@"Are you sure you want to remove %@?", skillLine.skill.name];
        
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Remove Skill?" andMessage:message];
        //alertView.messageFont = [UIFont systemFontOfSize:24];
        
        [alertView addButtonWithTitle:@"Cancel"
                                 type:SIAlertViewButtonTypeDefault
                              handler:nil];
        [alertView addButtonWithTitle:@"Remove Skill" type:SIAlertViewButtonTypeDestructive handler:^(SIAlertView *alert) {
            [skillLine.delegate skillLineWasRemoved:skillLine];
        }];
        alertView.transitionStyle = SIAlertViewTransitionStyleFade;
        
        [alertView show];

    } forControlEvents:UIControlEventTouchUpInside];
    
    [rollDice bk_addEventHandler:^(id sender) {
        [skillLine.delegate rollDiceForSkillLine:skillLine];
    } forControlEvents:UIControlEventTouchUpInside];
    
    return skillLine;
}

- (void) setSkillDetails:(FTCSkill *)skill
{
    self.skill = skill;
    MGBox *mainBox = (MGBox *)[self.leftItems objectAtIndex:0];
    
    MGLine *mainLine = (MGLine *)[mainBox.boxes objectAtIndex:0];
    MGLine *subLine = (MGLine *)[mainBox.boxes objectAtIndex:1];
    NSString *valueLabel = [skillValueTranslator nameAndSkillValue:[skill.value intValue]];
    NSString *actionsLabel = [@"\n" stringByAppendingString:[skillValueTranslator actionsStringForActions:skill.actions]];
    
    subLine.font = [UIFont fontWithName:GLYPH_FONT size:22];
    [subLine setMultilineLeft:actionsLabel];
    [mainLine setMultilineRight:valueLabel];
    [self layout];
}

- (id) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        skillValueTranslator = [[FTCSkillValueTranslator alloc] init];
    }
    
    return self;
}

- (void)setup {
    [super setup];
    self.sizingMode = MGResizingShrinkWrap;
    self.bottomMargin = 4;
    self.bottomPadding = 4;
    self.font = [UIFont fontWithName:@"HelveticaNeue" size:24];
    
    
    self.borderStyle = MGBorderEtchedBottom;
    self.bottomBorderColor = [UIColor blackColor];
    self.rasterize = YES;
}




@end
