//
//  FTCSkillValueTranslator.h
//  Fate Companion
//
//  Created by Joshua Barron on 2/15/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FTCSkill.h"

@interface FTCSkillValueTranslator : NSObject

- (NSString *) nameForSkillValue:(int)value;
- (NSString *) nameAndSkillValue:(int)value;

- (NSArray *) allSkillNamesAndValues;

- (NSString *) actionsStringForActions:(SkillActions)actions;
@end
