//
//  FTCDiceRollBox.m
//  Fate Companion
//
//  Created by Joshua Barron on 3/14/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCDiceRollBox.h"
#import "MGLine.h"
#import "FontAwesomeKit/FAKIonIcons.h"
#import "UIControl+BlocksKit.h"
#import "FTCSkillValueTranslator.h"
#import "FTCLayoutHelper.h"

#define INPUT_WIDTH 296

#define LINE_SIZE (CGSize){INPUT_WIDTH, 30}
#define GLYPH_FONT @"FateCoreGlyphs"

@interface FTCDiceRollBox()
{
    //CGSize lineSize;
    FTCSkillValueTranslator *translator;
    MGLine *modifierLine;
    MGLine *resultLine;
    int modifier;
}

@end

@implementation FTCDiceRollBox
@synthesize diceRoll = _diceRoll;
@synthesize delegate = _delegate;

- (void) setupDiceRoll:(FTCDiceRoll *)diceRoll
{
    [self.middleLines removeAllObjects];
    translator = [[FTCSkillValueTranslator alloc] init];
    //lineSize = (CGSize){self.size.width, 30};
    
    
    self.diceRoll = diceRoll;
    modifier = [self.diceRoll hasRoll] ? self.diceRoll.rollResult.modifier : 0;
    
    modifierLine = [self createModifierLine];
    
    [self.middleLines addObject:modifierLine];
    resultLine = [self createResultLine];
    [self.middleLines addObject:resultLine];
    [self.middleLines addObject:[self createRollButtonLine]];
    [self layout];
}

- (MGLine *)createModifierLine
{
    
    UIStepper *modifierStepper = [[UIStepper alloc] initWithFrame:CGRectMake(0, 10, 0, 0)];
    [modifierStepper addTarget:self action:@selector(modifierValueChanged:) forControlEvents:UIControlEventValueChanged];
    modifierStepper.value = modifier;
    modifierStepper.maximumValue = 10;
    modifierStepper.minimumValue = -10;
    
    NSString *modifierText = [NSString stringWithFormat:@"%d", (int)modifierStepper.value];
    
    MGLine *line = [MGLine lineWithLeft:@"**Modifier**|mush" right:modifierStepper size:LINE_SIZE];
    [line setMultilineMiddle:modifierText];
    line.middleItemsAlignment = NSTextAlignmentCenter;
    line.font = [UIFont systemFontOfSize:18];
    line.middleFont = [UIFont boldSystemFontOfSize:22];
    return line;
}

- (MGLine *)createResultLine
{
    
    id result = [self.diceRoll hasRoll] ? [translator nameAndSkillValue:self.diceRoll.rollResult.finalResult] : @"";
    MGLine *line = [MGLine lineWithLeft:([self.diceRoll hasRoll] ?
                                         [self.diceRoll.rollResult rollDescription:NO] : @"No result yet") right:result size:LINE_SIZE];
    line.font = [UIFont systemFontOfSize:18];
    return line;
}

- (MGLine *)createRollButtonLine
{
    UIButton *rollDice = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, INPUT_WIDTH, 30)];
    
    UIFont *glyphFont = [UIFont fontWithName:GLYPH_FONT size:24];
    UIColor *tintColor = [FTCLayoutHelper getTintColor];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    NSMutableAttributedString *buttonText = [[NSMutableAttributedString alloc] initWithString:@"+ Roll Dice" attributes:@{NSForegroundColorAttributeName : tintColor,
                                NSParagraphStyleAttributeName : paragraphStyle,
                                NSFontAttributeName : [UIFont systemFontOfSize:24]}];
    [buttonText addAttribute:NSFontAttributeName value:glyphFont range:NSMakeRange(0, 1)];
    [buttonText addAttribute:NSBaselineOffsetAttributeName value:@(2) range:NSMakeRange(0, 1)];
    
    [rollDice setAttributedTitle:buttonText forState:UIControlStateNormal];
    //[rollDice sizeToFit];
    rollDice.showsTouchWhenHighlighted = YES;
    
    [rollDice addTarget:self action:@selector(rollButtonWasTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    MGLine *line = [MGLine lineWithLeft:rollDice right:nil size:LINE_SIZE];
    line.topMargin = 4;
    return line;
}

- (void)rollButtonWasTapped:(UIButton *) button
{
    NSLog(@"Rollin the dice");
    //create a new roll result
    //show random rolls, then add the result
    FTCDiceRollResult *roll = [FTCDiceRoller rollDiceWithModifier:modifier];
    self.diceRoll.rollResult = roll;
    
    //Generate fake rolls
    NSMutableArray *fakeRolls = [[FTCDiceRoller rollDiceWithModifier:modifier withRepeat:10] mutableCopy];
    
    button.enabled = NO;
    [resultLine setRightItems:[NSMutableArray arrayWithObject:@"Rolling..."]];
    [self setResultsText:fakeRolls];
}

- (void)setResultsText:(NSMutableArray *)results;
{
    FTCDiceRollResult *result = (FTCDiceRollResult *)[results objectAtIndex:0];
    NSAttributedString *des = [result rollDescription:NO];
    [resultLine setLeftItems:[NSMutableArray arrayWithObject:des]];
    [self layout];
    [results removeObjectAtIndex:0];
    if (results.count > 0)
    {
        [self performSelector:@selector(setResultsText:) withObject:results afterDelay:.1];
    }
    else
    {
        [self setupDiceRoll:self.diceRoll];
        [self.delegate saveDiceRolls];
    }
}

- (void)modifierValueChanged:(UIStepper *) stepper
{
    NSString *modifierText = [NSString stringWithFormat:@"%d", (int)stepper.value];
    [modifierLine setMultilineMiddle:modifierText];
    modifier = (int)stepper.value;
    [self layout];
}

+ (id)boxWithSize:(CGSize)size andTitle:(NSString *)title andActionImage:(UIImage *)actionImage andActionCallback:(void (^)(id sender))actionCallback;
{
    UIColor *tintColor = [FTCLayoutHelper getTintColor];
    
    FTCDiceRollBox *box = [FTCDiceRollBox boxWithSize:size];
    NSString *capitalizedTitle = [title uppercaseString];
    
    MGBox *rightBox = nil;
    if (actionImage != nil)
    {
        UIButton *actionButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, actionImage.size.width, actionImage.size.height)];
        [actionButton setBackgroundImage:actionImage forState:UIControlStateNormal];
        actionButton.showsTouchWhenHighlighted = YES;
        [actionButton bk_addEventHandler:actionCallback forControlEvents:UIControlEventTouchUpInside];
        
        rightBox = [MGBox boxWithSize:actionButton.size];
        [rightBox addSubview: actionButton]; //imgView];
        rightBox.rightMargin = 10;
    }
    
    MGLine *titleLine = [MGLine lineWithLeft:capitalizedTitle right:rightBox size:(CGSize){size.width - 8, 30}];
    titleLine.backgroundColor = tintColor;
    titleLine.textColor = [UIColor whiteColor];
    titleLine.font = [UIFont fontWithName:@"AvenirNext" size:28];
    titleLine.leftPadding = 12;
    titleLine.topPadding = 4;
    titleLine.topMargin = 8;
    titleLine.bottomMargin = 4;
    titleLine.bottomPadding = 4;
    
    [box.topLines addObject:titleLine];
    box.sectionTitle = [title lowercaseString];
    return box;
}

@end
