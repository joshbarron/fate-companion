//
//  FTCAspect.h
//  Fate Companion
//
//  Created by Joshua Barron on 2/17/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FTCAspect : NSObject<NSCoding>

-(id) initWithText:(NSString *)aspectText;

@property (strong) NSString *aspectText;

@end
