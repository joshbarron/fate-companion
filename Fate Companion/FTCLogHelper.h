//
//  FTCLogHelper.h
//  Fate Companion
//
//  Created by Joshua Barron on 4/22/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DDLog.h"

#ifdef CONFIGURATION_Debug

static int ddLogLevel = LOG_LEVEL_VERBOSE;

#else

static int ddLogLevel = LOG_LEVEL_WARN;

#endif


@interface FTCLogHelper : NSObject

//todo: put interesting things here

@end
