//
//  FTCOperationResult.h
//  Fate Companion
//
//  Created by Joshua Barron on 1/26/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FTCOperationResult : NSObject

@property NSString *message;

@property BOOL success;

- (id) initWithSuccess;

- (id) initWithFailure:(NSString *)message;

@end
