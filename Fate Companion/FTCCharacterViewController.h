//
//  FTCFirstViewController.h
//  Fate Companion
//
//  Created by Joshua Barron on 1/25/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FTCCharacterDetailViewController.h"
#import "FTCEntryBox.h"
#import "FTCDocumentManager.h"
#import "FTCCreateCharacterViewController.h"

@class MGScrollView;

@interface FTCCharacterViewController : UIViewController <UITextViewDelegate, UISplitViewControllerDelegate, FTCCharacterDetailViewControllerDelegate, FTCEntryBoxDelegate, FTCDocumentManagerDelegate, FTCCreateCharacterDelegate>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *addButton;
@property (weak, nonatomic) IBOutlet MGScrollView *scroller;

@end
