//
//  FTCExtra.h
//  Fate Companion
//
//  Created by Joshua Barron on 2/27/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FTCExtra : NSObject<NSCoding>

@property (strong) NSString * name;

- (id) initWithName:(NSString *)name;

@end
