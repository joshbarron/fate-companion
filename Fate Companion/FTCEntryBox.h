//
//  FTCEntryTableCell.h
//  Fate Companion
//
//  Created by Joshua Barron on 1/25/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGBox.h"
#import "FTCEntry.h"

@class FTCEntryBox;

@protocol FTCEntryBoxDelegate

- (void) boxWasTapped:(FTCEntryBox *)box;

@end

@interface FTCEntryBox : MGBox


+ (FTCEntryBox *) characterBox:(FTCEntry *)entry withWidth:(float)width forView:(id<FTCEntryBoxDelegate>)viewController;

@property (weak) id<FTCEntryBoxDelegate> delegate;

@property MGBox *overBox;
@property MGBox *actionsBox;

@property NSString *characterName;
@property NSString *characterDescription;
@property UIImage *characterThumbnail;

@end
