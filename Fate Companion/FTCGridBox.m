//
//  FTCGridBox.m
//  Fate Companion
//
//  Created by Joshua Barron on 3/15/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCGridBox.h"
#import "FTCLayoutHelper.h"

@implementation FTCGridBox
@synthesize itemPadding = _itemPadding;

- (void) setup
{
    [super setup];
    
    self.contentLayoutMode = MGLayoutGridStyle;
    //self.leftMargin = 8;
    //self.topMargin = 8;
    self.itemPadding = [FTCLayoutHelper getGridItemPadding];
}

- (void) layout
{
    if ([FTCLayoutHelper deviceIsIPad]) {
        for (int i = 0; i < self.boxes.count; i++) {
            MGBox *box = [self.boxes objectAtIndex:i];
            
            box.leftMargin = box.rightMargin = self.itemPadding;
        }
    }
    
    [super layout];
}

@end
