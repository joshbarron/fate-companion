//
//  FTCStressTrackBox.h
//  Fate Companion
//
//  Created by Joshua Barron on 2/28/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "MGBox.h"
#import "FTCStressTrackLine.h"

@interface FTCStressTrackBox : MGBox

+ (FTCStressTrackBox *)stressTrackBoxWithIndex:(int)index andState:(BOOL)isUsed enabled:(BOOL)isEnabled delegate:(FTCStressTrackLine *)delegate;

@property (weak) FTCStressTrackLine * delegate;
@property int index;
@property BOOL isUsed;
@property BOOL isEnabled;

@end
