//
//  FTCDiceRollResult.h
//  Fate Companion
//
//  Created by Joshua Barron on 3/7/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, DieRollResult) {
    RollNegative = -1,
    RollNeutral = 0,
    RollPositive = 1
};

@interface FTCDiceRollResult : NSObject<NSCoding>

- (id) initWithRoll:(DieRollResult)first andSecond:(DieRollResult)second andThird:(DieRollResult)third andFourth:(DieRollResult)fourth andModifier:(int)modifier;

- (NSString *)originalRollString;
- (NSAttributedString *)rollDescription;
- (NSAttributedString *)rollDescription:(BOOL)includeResult;
@property int originalRoll;
@property int modifier;
@property int finalResult;

@property DieRollResult firstDie;
@property DieRollResult secondDie;
@property DieRollResult thirdDie;
@property DieRollResult fourthDie;

@end
