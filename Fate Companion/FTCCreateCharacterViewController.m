//
//  FTCCreateCharacterViewController.m
//  Fate Companion
//
//  Created by Joshua Barron on 4/12/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCCreateCharacterViewController.h"
#import "MGTableBox.h"
#import "MGLine.h"
#import "CRToast.h"
#import "FTCNotificationHelper.h"
#import "FTCLayoutHelper.h"
#import "UIControl+BlocksKit.h"
#import "JVFloatLabeledTextField.h"

@interface FTCCreateCharacterViewController ()

@property JVFloatLabeledTextField *nameTextField;
@property UISegmentedControl *characterTypeControl;

@end

@implementation FTCCreateCharacterViewController
{
    MGTableBox *masterTableBox;
}
@synthesize scroller = _scroller;
@synthesize delegate = _delegate;
@synthesize nameTextField = _nameTextField;
@synthesize characterTypeControl = _characterTypeControl;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.scroller.contentLayoutMode = MGLayoutGridStyle;
    self.scroller.scrollEnabled = NO;
    if ([FTCLayoutHelper deviceIsIPad])
    {
        self.scroller.keepFirstResponderAboveKeyboard = NO;
    }
    masterTableBox = [MGTableBox boxWithSize:(CGSize){320, 0}];
    
    [self.scroller.boxes addObject:masterTableBox];
    
    MGLine *instructions = [MGLine lineWithMultilineLeft:@"Please enter the name of your character:" right:nil width:280 minHeight:20];
    instructions.leftMargin = instructions.rightMargin = 20;
    instructions.bottomMargin = instructions.topMargin = 5;
    [masterTableBox.topLines addObject:instructions];
    
    self.nameTextField = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(0, 0, 260, 40)];
    [self.nameTextField setPlaceholder:@"Character Name"];
    self.nameTextField.font = [UIFont boldSystemFontOfSize:20];
    self.nameTextField.clearButtonMode = UITextFieldViewModeAlways;
    self.nameTextField.delegate = self;
    
    MGLine *nameLine = [MGLine lineWithLeft:self.nameTextField right:nil size:(CGSize){260, 40}];
    nameLine.leftMargin = nameLine.rightMargin = 30;
    nameLine.bottomPadding = 5;

    [masterTableBox.topLines addObject:nameLine];
    
    //Character type
    MGLine *instructions2 = [MGLine lineWithMultilineLeft:@"Select the type of your character:" right:nil width:280 minHeight:20];
    instructions2.leftMargin = instructions2.rightMargin = 20;
    instructions2.bottomMargin = instructions2.topMargin = 5;
    [masterTableBox.topLines addObject:instructions2];
    
    NSArray *typeOptions = [NSArray arrayWithObjects:@"Fate Core", @"Fate Accelerated", nil];
    self.characterTypeControl = [[UISegmentedControl alloc] initWithItems:typeOptions];
    self.characterTypeControl.selectedSegmentIndex = 0;
    
    MGLine *characterTypeLine = [MGLine lineWithLeft:self.characterTypeControl right:nil size:(CGSize){260, 40}];
    characterTypeLine.leftMargin = characterTypeLine.rightMargin = 30;
    characterTypeLine.topMargin = 5;

    [masterTableBox.topLines addObject:characterTypeLine];
    
    [self.scroller layout];
    [self.scroller scrollToView:masterTableBox withMargin:0];
}

#pragma mark UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
//    if (textField.tag == TAG_NAMEFIELD)
//    {
//        NSUInteger newLength = [textField.text length] + [string length] - range.length;
//        return (newLength > 15) ? NO : YES;
//    }
    return YES;
}

- (void)textChanged:(UITextField *)textField {
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
    [self textChanged:textField];
	return YES;
}


- (IBAction)cancelButtonWasTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveButtonWasTapped:(id)sender {
    NSString *name = self.nameTextField.text;
    
    if (name == nil || name.length == 0)
    {
        [CRToastManager showNotificationWithOptions:[FTCNotificationHelper warningNotification:@"Oops!" andSubtitle:@"Please enter a name for your character."] completionBlock:nil];
        return;
    }
    
    CharacterType type = self.characterTypeControl.selectedSegmentIndex;
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.delegate characterWasCreatedWithName:name andType:type];
}

@end
