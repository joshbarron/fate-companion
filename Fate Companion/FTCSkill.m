//
//  FTCSkill.m
//  Fate Companion
//
//  Created by Joshua Barron on 2/16/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCSkill.h"

@implementation FTCSkill
@synthesize name = _name;
@synthesize value = _value;
@synthesize isCustom = _isCustom;
@synthesize actions = _actions;

-(id) initWithName:(NSString *)name andValue:(NSNumber *)value isCustom:(BOOL)custom andActions:(SkillActions)actions
{
    if (self = [super init])
    {
        self.name = name;
        self.value = value;
        self.isCustom = custom;
        self.actions = actions;
    }
    return self;
}

- (FTCDiceRollResult *)rollDice
{
    return [FTCDiceRoller rollDiceWithModifier:[self.value intValue]];
}

#pragma mark NSCoding

#define kSkillVersionKey @"SkillVersion"
#define kNameKey @"SkillName"
#define kValueKey @"SkillValue"
#define kCustomKey @"SkillIsCustom"
#define kActionsKey @"SkillActions"

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeInt:1 forKey:kSkillVersionKey];
    [encoder encodeObject:self.name forKey:kNameKey];
    [encoder encodeObject:self.value forKey:kValueKey];
    [encoder encodeBool:self.isCustom forKey:kCustomKey];
    [encoder encodeInt:self.actions forKey:kActionsKey];
}

- (id)initWithCoder:(NSCoder *)decoder {
    [decoder decodeIntForKey:kSkillVersionKey];
    NSString *name = [decoder decodeObjectForKey:kNameKey];
    NSNumber *skillValue = [decoder decodeObjectForKey:kValueKey];
    BOOL isCustom = [decoder decodeBoolForKey:kCustomKey];
    SkillActions skillActions = [decoder decodeIntForKey:kActionsKey];
    
    return [self initWithName:name andValue:skillValue isCustom:isCustom andActions:skillActions];
}


@end
