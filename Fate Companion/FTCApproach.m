//
//  FTCApproach.m
//  Fate Companion
//
//  Created by Joshua Barron on 4/14/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCApproach.h"

@implementation FTCApproach
@synthesize name = _name;
@synthesize value = _value;
@synthesize isCustom = _isCustom;

- (id) initWithName:(NSString *)name andValue:(NSNumber *)value isCustom:(BOOL)custom
{
    if (self = [super init]) {
        self.name = name;
        self.value = value;
        self.isCustom = custom;
    }
    return self;
}

- (FTCDiceRollResult *)rollDice
{
    return [FTCDiceRoller rollDiceWithModifier:[self.value intValue]];
}

#pragma mark NSCoding

#define kApproachVersionKey @"ApproachVersion"
#define kNameKey @"ApproachName"
#define kValueKey @"ApproachValue"
#define kCustomKey @"ApproachIsCustom"


- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeInt:1 forKey:kApproachVersionKey];
    [encoder encodeObject:self.name forKey:kNameKey];
    [encoder encodeObject:self.value forKey:kValueKey];
    [encoder encodeBool:self.isCustom forKey:kCustomKey];
}

- (id)initWithCoder:(NSCoder *)decoder {
    [decoder decodeIntForKey:kApproachVersionKey];
    NSString *name = [decoder decodeObjectForKey:kNameKey];
    NSNumber *skillValue = [decoder decodeObjectForKey:kValueKey];
    BOOL isCustom = [decoder decodeBoolForKey:kCustomKey];
    
    return [self initWithName:name andValue:skillValue isCustom:isCustom];
}

@end
