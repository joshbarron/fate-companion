//
//  FTCStressTrackLine.h
//  Fate Companion
//
//  Created by Joshua Barron on 2/28/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "MGLine.h"
#import "FTCStressTrack.h"

@class FTCStressTrackLine;

@protocol FTCStressTrackLineDelegate

- (void)stressTrackWasTapped:(FTCStressTrackLine *)stressTrackLine;
- (void)stressTrackWasRemoved:(FTCStressTrackLine *)stressTrackLine;
- (void)stressTrackBoxStateChanged:(FTCStressTrackLine *)stressTrackLine withIndex:(int)boxIndex;

@end

@interface FTCStressTrackLine : MGLine

+ (FTCStressTrackLine *) stressTrackLineWithStressTrack:(FTCStressTrack *)stressTrack delegate:(id<FTCStressTrackLineDelegate>)delegate;

@property (weak) id<FTCStressTrackLineDelegate> delegate;
@property (weak) FTCStressTrack *stressTrack;

@property double lineWidth;

@end
