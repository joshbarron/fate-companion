//
//  FTCDiceRollBox.h
//  Fate Companion
//
//  Created by Joshua Barron on 3/14/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCDetailSectionBox.h"
#import "FTCDiceRoll.h"

@class FTCDiceRollBox;

@protocol FTCDiceRollBoxDelegate

- (void) saveDiceRolls;

@end

@interface FTCDiceRollBox : FTCDetailSectionBox

- (void) setupDiceRoll: (FTCDiceRoll *)diceRoll;

@property (strong) FTCDiceRoll *diceRoll;

@property (weak) id<FTCDiceRollBoxDelegate> delegate;

@end
