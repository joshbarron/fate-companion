//
//  FTCNewsViewController.m
//  Fate Companion
//
//  Created by Joshua Barron on 4/5/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCNewsViewController.h"
#import "FTCGridBox.h"
#import "MGLine.h"
#import "FTCLayoutHelper.h"
#import "FTCDetailSectionBox.h"
#import "SIAlertView.h"
#import "JASidePanelController.h"
#import "FTCNewsHelper.h"
#import "MBProgressHUD.h"
#import "FTCNewsStory.h"

@interface FTCNewsViewController ()
{
    FTCGridBox *_masterBox;
    NSArray *stories;
    BOOL isLoaded;
}
@end

@implementation FTCNewsViewController
@synthesize scroller = _scroller;

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if ([FTCLayoutHelper deviceIsIPad])
    {
        [_masterBox setSize:(CGSize){[FTCLayoutHelper getGridWidthForOrientation:toInterfaceOrientation], 0}];
        
        CGSize boxSize = (CGSize){[FTCLayoutHelper getRowWidthForOrientation:toInterfaceOrientation doubleWide:YES], 0};
        CGSize lineSize = (CGSize){[FTCLayoutHelper getInputWidthForOrientation:toInterfaceOrientation doubleWide:YES], 30};
        
        for (FTCDetailSectionBox *storyBox in _masterBox.boxes) {
            [storyBox setSize:boxSize];
            MGLine *bodyLine = (MGLine *)[storyBox.middleLines firstObject];
            [bodyLine setSize:lineSize];
            MGLine *footerLine = (MGLine *)[storyBox.bottomLines firstObject];
            [footerLine setSize:lineSize];
        }
        
        [self.scroller layoutWithSpeed:duration completion:nil];
    }
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if ([FTCLayoutHelper deviceIsIPad])
    {
        [self.scroller layout];
    }
}

#pragma mark UISplitViewControllerDelegate
-(void)splitViewController:(UISplitViewController *)svc
    willHideViewController:(UIViewController *)aViewController
         withBarButtonItem:(UIBarButtonItem *)barButtonItem
      forPopoverController:(UIPopoverController *)pc
{
    NSLog(@"Will hide left side");
    FTCAppDelegate *appDelegate = [FTCLayoutHelper appDelegate];
    barButtonItem.image = [JASidePanelController defaultImage];
    
    appDelegate.menuPopover = pc;
    appDelegate.menuNavItem = barButtonItem;
    
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
}

-(void)splitViewController:(UISplitViewController *)svc
    willShowViewController:(UIViewController *)aViewController
 invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    NSLog(@"Will show left side");
    FTCAppDelegate *appDelegate = [FTCLayoutHelper appDelegate];
    
    appDelegate.menuPopover = nil;
    appDelegate.menuNavItem = nil;
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
}

- (void) refreshNews
{
    [_masterBox.boxes removeAllObjects];
    [self.scroller layout];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Loading News";
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [[FTCNewsHelper sharedNewsHelper] getNewsEntriesWithSuccess:^(NSArray *newsStories)
        {
            stories = newsStories;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self displayNews];
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            });
        }
        andFailure:^(NSError *error) {
            SIAlertView *alert = [[SIAlertView alloc] initWithTitle:@"Error retrieving news" andMessage:@"We couldn't retrieve the news. Please check your internet connection."];
            [alert addButtonWithTitle:@"OK" type:SIAlertViewButtonTypeCancel handler:nil];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [alert show];
            });
        }];
    });
}

- (void)displayNews
{
    CGSize boxSize = (CGSize){[FTCLayoutHelper getRowWidth:YES], 0};
    for (FTCNewsStory *story in stories) {
        FTCDetailSectionBox *storyBox = [FTCDetailSectionBox boxWithSize:boxSize andTitle:story.title andActionImage:nil andActionCallback:nil];
        
        MGLine *bodyLine = [MGLine lineWithMultilineLeft:[story mushedBody] right:nil width:[FTCLayoutHelper getInputWidth:YES] minHeight:30];
        
        [storyBox.middleLines addObject:bodyLine];
        
        NSMutableString *footer = [NSMutableString stringWithString:@"Posted by Joshua Barron on "];
        [footer appendString:[story postedDateString]];
        MGLine *footerLine = [MGLine lineWithLeft:nil right:footer size:(CGSize){[FTCLayoutHelper getInputWidth:YES], 30}];
        
        [storyBox.bottomLines addObject:footerLine];
        
        [_masterBox.boxes addObject:storyBox];
    }
    isLoaded = YES;
    [self.scroller layout];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [FTCLayoutHelper setupMenuNav:self];
    isLoaded = NO;
    _masterBox = [FTCGridBox boxWithSize:(CGSize){[FTCLayoutHelper getGridWidth], 0}];
    _masterBox.contentLayoutMode = MGLayoutGridStyle;
    _masterBox.leftMargin = 8;
    _masterBox.topMargin = 8;
    
    self.scroller.contentLayoutMode = MGLayoutGridStyle;
    self.scroller.keyboardMargin = 10;
    //self.scroller.delegate = self;
    self.scroller.canCancelContentTouches = NO;
    
    [self.scroller.boxes addObject:_masterBox];
    
    [self refreshNews];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
