//
//  FTCGridBox.h
//  Fate Companion
//
//  Created by Joshua Barron on 3/15/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "MGBox.h"

@interface FTCGridBox : MGBox

@property double itemPadding;

@end
