//
//  FTCEntryTableCell.m
//  Fate Companion
//
//  Created by Joshua Barron on 1/25/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCEntryBox.h"
#import "FTCCharacterMetadata.h"
#import "MGTableBox.h"
#import "MGLine.h"
#import "FTCPhotoBox.h"
#import "FTCCharacterTableBoxStyled.h"
#import "FontAwesomeKit/FAKIonIcons.h"
#import "UIColor+LightAndDark.h"
#import "FTCLayoutHelper.h"

#define HEADER_WIDTH 200
#define HEADER_HEIGHT 35
#define ARROW_WIDTH 25
#define ARROW_HEIGHT 25
#define ARROW_SIZE (CGSize){ARROW_WIDTH, ARROW_HEIGHT}
#define ARROW_BOX_SIZE (CGSize){25, 50}
#define CHARACTER_ROW_SIZE (CGSize){200, 50}
#define HEADER_ROW_SIZE (CGSize){HEADER_WIDTH, HEADER_HEIGHT}
#define THUMBNAIL_SIZE (CGSize){75, 75}




#define WIDTH 320
#define TOP_MARGIN    8.0
#define BOTTOM_MARGIN 0
#define ARROW_TAG 123
#define LEFT_MARGIN   0
//#define CORNER_RADIUS 4.0

@implementation FTCEntryBox
{
    UIColor *tintColor;
}
@synthesize delegate = _delegate;
@synthesize overBox = _overBox;
@synthesize actionsBox = _actionsBox;
@synthesize characterName = _characterName;
@synthesize characterDescription = _characterDescription;
@synthesize characterThumbnail = _characterThumbnail;


+ (FTCEntryBox *) characterBox:(FTCEntry *)entry withWidth:(float)width forView:(id<FTCEntryBoxDelegate>)viewController
{
    CGSize size = CGSizeMake(width, 0);
    
    FTCEntryBox *box = [FTCEntryBox boxWithSize:size andName:entry.metadata.name andDescription:entry.metadata.description andThumbnail:entry.metadata.thumbnail andDelegate:viewController];
    
    return box;
}

+ (id)boxWithSize:(CGSize)size andName:(NSString *)name andDescription:(NSString *)description andThumbnail:(UIImage *)thumbnail andDelegate: (id<FTCEntryBoxDelegate>)delegate {
    CGRect frame = CGRectMake(0, 0, size.width, size.height);
    MGBox *box = [[self alloc] initWithFrame:frame andName:name andDescription:description andThumbnail:thumbnail andDelegate:delegate];
    
    return box;
}

- (id) initWithFrame:(CGRect)frame andName:(NSString *)name andDescription:(NSString *)description andThumbnail:(UIImage *)thumbnail andDelegate: (id<FTCEntryBoxDelegate>)delegate {
    self.delegate = delegate;
    self.characterName = name;
    self.characterDescription = description;
    self.characterThumbnail = thumbnail;
    tintColor = [FTCLayoutHelper getTintColor];
    self = [super initWithFrame:frame];
    return self;
}

- (void)setup {
    [super setup];
    tintColor = [FTCLayoutHelper getTintColor];
    
    self.contentLayoutMode = MGLayoutGridStyle;
    
    if (self.characterThumbnail == nil)
    {
        FAKIcon *personIcon = [FAKIonIcons ios7PersonOutlineIconWithSize:48];
        [personIcon addAttribute:NSForegroundColorAttributeName value:tintColor];
        UIImage *defaultPic = [personIcon imageWithSize:THUMBNAIL_SIZE];
        self.characterThumbnail = defaultPic;
    }
    [self setupOverbox];
    [self setupOverboxItems];
    //[self setupActionsBox];
    
    [self setupActions];
    //[self layout];
}

- (void) setupOverbox
{
    // shape, size, and position
    self.overBox = [MGBox boxWithSize:self.size];
    self.overBox.contentLayoutMode = MGLayoutGridStyle;
    [self.boxes addObject:self.overBox];
    
    self.width = self.overBox.width = self.width ? self.width : WIDTH;
    self.overBox.topMargin = TOP_MARGIN;
    self.overBox.bottomMargin = BOTTOM_MARGIN;
    self.overBox.leftMargin = LEFT_MARGIN;
    //self.overBox.zIndex = 1;
    // shape and colour
    self.overBox.backgroundColor = [UIColor colorWithWhite:.97f alpha:1];//[UIColor colorWithRed:218/255.0f green:90/255.0f blue:37/255.0f alpha:.1f];//[UIColor clearColor];//[UIColor colorWithRed:0.94 green:0.94 blue:0.95 alpha:1];
    self.overBox.layer.cornerRadius = 0;//CORNER_RADIUS;
    self.overBox.swiper.direction = UISwipeGestureRecognizerDirectionLeft;
    // shadow
    self.overBox.layer.shadowColor = [UIColor colorWithWhite:0.12 alpha:1].CGColor;
    self.overBox.layer.shadowOffset = CGSizeMake(0, 0.5);
    self.overBox.layer.shadowRadius = 1;
    self.overBox.layer.shadowOpacity = 1;
    self.rasterize = YES;
}

- (void) setupOverboxItems
{
    //OverBox
    FTCPhotoBox *photoBox = [FTCPhotoBox photoBoxFor:self.characterThumbnail withSize:THUMBNAIL_SIZE];
    [self.overBox.boxes addObject:photoBox];
    
    MGTableBox *characterSection = [MGTableBox boxWithSize:(CGSize){HEADER_WIDTH, 0}];
    characterSection.topMargin = 0;
    characterSection.leftMargin = 8;
    [self.overBox.boxes addObject:characterSection];
    
    FAKIcon *arrowIcon = [FAKIonIcons ios7ArrowForwardIconWithSize:36];
    [arrowIcon addAttribute:NSForegroundColorAttributeName value:tintColor];
    FAKIcon *arrowHighlightedIcon = [FAKIonIcons ios7ArrowForwardIconWithSize:36];
    [arrowHighlightedIcon addAttribute:NSForegroundColorAttributeName value:[tintColor darkerColor]];
    UIImage *arrow = [arrowIcon imageWithSize:ARROW_SIZE];
    UIImage *arrowHighlighted = [arrowHighlightedIcon imageWithSize:ARROW_SIZE];
    
    UIImageView *arrowView = [[UIImageView alloc] initWithImage:arrow highlightedImage:arrowHighlighted]; //todo look at highlighted
    [arrowView setTag: ARROW_TAG];
    [arrowView setSize:ARROW_SIZE];
    MGBox *arrowBox = [MGBox boxWithSize:ARROW_BOX_SIZE];
    arrowBox.topMargin = 30;
    arrowBox.leftMargin = 4;
    [arrowBox addSubview:arrowView];
    [self.overBox.boxes addObject:arrowBox];
    
    MGLine *header = [MGLine lineWithLeft:self.characterName right:nil size:HEADER_ROW_SIZE];
    header.borderStyle = MGBorderEtchedTop | MGBorderEtchedBottom;
    header.bottomBorderColor = [UIColor colorWithWhite:.6f alpha:1];
    header.font = [UIFont fontWithName:@"HelveticaNeue" size:22];
    [characterSection.topLines addObject:header];
    
    NSString *des = self.characterDescription.length > 0 ? self.characterDescription : @"No Description";
    
    NSRange stringRange = {0, MIN([des length], 70)};
    stringRange = [des rangeOfComposedCharacterSequencesForRange:stringRange];
    NSString *shortString = [des substringWithRange:stringRange];
    if (stringRange.length > 20)
    {
        NSMutableString *tmpStr = [NSMutableString stringWithString:shortString];
        [tmpStr appendString:@"..."];
        shortString = tmpStr;
    }
    MGLine *body = [MGLine lineWithMultilineLeft:shortString right:nil width:200 minHeight:50];
    body.font = [UIFont systemFontOfSize:14];
    [characterSection.topLines addObject:body];
}

- (void) setupActionsBox
{
    return; //don't bother with actions box for now
    NSLog(@"Setting up actions box");
    self.actionsBox = [MGBox boxWithSize:self.size];
    self.actionsBox.contentLayoutMode = MGLayoutGridStyle;
    self.actionsBox.zIndex = -1;
    self.actionsBox.topMargin = -2;
    self.actionsBox.attachedTo = self;
    [self.boxes addObject:self.actionsBox];
    
    //Underbox (actions)
    
    FAKIcon *trashIcon = [FAKIonIcons ios7TrashIconWithSize:60];
    [trashIcon addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
    UIImage *trash = [trashIcon imageWithSize:(CGSize){40, 40}];
    UIImageView *trashView = [[UIImageView alloc] initWithImage:trash];
    [trashView setSize:(CGSize){30, 30}];
    MGBox *deleteButton = [MGBox boxWithSize:(CGSize){40, 55}];
    deleteButton.topMargin = 30;
    deleteButton.leftMargin = 15;
    [deleteButton addSubview:trashView];
    MGBox *deleteBox = [MGBox boxWithSize:(CGSize){60, 95}];
    deleteBox.contentLayoutMode = MGLayoutGridStyle;
    deleteBox.backgroundColor = [UIColor redColor];
    [deleteBox.boxes addObject:deleteButton];
    deleteBox.topMargin = 5;
    MGLine *actionsBar = [MGLine lineWithLeft:nil right:deleteBox size:(CGSize){self.size.width, 95}];
    
    [self.actionsBox.boxes addObject:actionsBar];

}

- (void)callBoxWasTapped
{
    __weak FTCEntryBox *wbox = self;
    [wbox.delegate boxWasTapped:wbox];
}

- (void)setupActions {
    __weak FTCEntryBox *wbox = self;
    __weak UIColor *wtintColor = tintColor;
    self.overBox.onTap = ^{
        UIImageView *_arrow = (UIImageView *)[wbox viewWithTag:ARROW_TAG];
        _arrow.highlighted = YES;
        wbox.topBorderColor = wtintColor;
        wbox.bottomBorderColor = wtintColor;
        //wbox.backgroundColor = wtintColor;
        wbox.overBox.topBorderColor = wtintColor;
        wbox.overBox.bottomBorderColor = wtintColor;
        double delayInSeconds = 0.2;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [wbox.delegate boxWasTapped:wbox];
        });
    };
}

@end
