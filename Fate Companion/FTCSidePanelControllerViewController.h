//
//  FTCSidePanelControllerViewController.h
//  Fate Companion
//
//  Created by Joshua Barron on 1/28/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "JASidePanelController.h"
#import "FTCMainNavigationViewController.h"

@interface FTCSidePanelControllerViewController : JASidePanelController

- (FTCMainNavigationViewController *) getMainNav;

@end
