//
//  FTCCharacter.h
//  Fate Companion
//
//  Created by Joshua Barron on 1/25/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FTCAspect.h"

typedef NS_ENUM(NSInteger, CharacterType) {
    FateCoreCharacter = 0,
    FateAcceleratedCharacter = 1
};

@interface FTCCharacter : NSObject <NSCoding>

@property (strong) UIImage * photo;
@property (strong) NSString * name;

@property CharacterType characterType;

@property (strong) NSString * description;
@property (strong) NSNumber * fatePoints;
@property (strong) NSNumber * refresh;

//Aspects
@property (strong) FTCAspect * highConcept;
@property (strong) FTCAspect * trouble;
@property (strong) NSMutableArray * aspects;

//Approaches (FAE)
@property (strong) NSMutableArray * approaches;

//Skills (FC)
@property (strong) NSMutableArray * skills;

//Stunts
@property (strong) NSMutableArray * stunts;

//Extras (FC)
@property (strong) NSMutableArray * extras;

//Stress Tracks
@property (strong) NSMutableArray * stressTracks;

//Consequences
@property (strong) NSMutableArray * consequences;

@end
