//
//  FTCStressTrack.h
//  Fate Companion
//
//  Created by Joshua Barron on 2/28/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_OPTIONS(NSInteger, StressBoxes) {
    FirstBox = 1 << 0,
    SecondBox = 1 << 1,
    ThirdBox = 1 << 2,
    FourthBox = 1 << 3,
    FifthBox = 1 << 4
};

@interface FTCStressTrack : NSObject<NSCoding>

@property (strong) NSString *name;
@property (strong) NSNumber *slots;
@property StressBoxes stressBoxes;

- (id) initWithName:(NSString *)name andSlotCount:(NSNumber *)slots andStressBoxes:(StressBoxes)stressBoxes;

- (BOOL) isStressBoxUsed:(int)index;

- (StressBoxes) toggleStateOfStressBox:(int)index;

@end
