//
//  FTCLayoutHelper.h
//  Fate Companion
//
//  Created by Joshua Barron on 3/15/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FTCSplitViewController.h"
#import "FTCAppDelegate.h"

@interface FTCLayoutHelper : NSObject

+ (FTCAppDelegate *) appDelegate;

+ (UIColor *)getTintColor;

+ (void) setupMenuNav:(UIViewController *)vc;

+ (BOOL) deviceIsIPhone;

+ (BOOL) deviceIsIPad;

+ (FTCSplitViewController *) splitViewController:(UIViewController *)vc;

+ (UIInterfaceOrientation) getDeviceOrientation;

+ (double) getGridWidth;

+ (double) getGridWidthForOrientation:(UIInterfaceOrientation) orientation;

+ (double) getGridItemPadding;

+ (double) getGridItemPaddingForOrientation:(UIInterfaceOrientation) orientation;

+ (double) getRowWidth:(BOOL)doubleWide;

+ (double) getRowWidthForOrientation:(UIInterfaceOrientation) orientation doubleWide:(BOOL)doubleWide;

+ (double) getInputWidth:(BOOL)doubleWide;

+ (double) getInputWidthForOrientation:(UIInterfaceOrientation) orientation doubleWide:(BOOL)doubleWide;

@end
