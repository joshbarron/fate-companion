//
//  FTCNoteBox.m
//  Fate Companion
//
//  Created by Joshua Barron on 3/15/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCNoteLine.h"
#import "MGBox.h"
#import "FontAwesomeKit/FAKIonIcons.h"
#import "UIControl+BlocksKit.h"
#import "FTCLayoutHelper.h"

#define HEIGHT 30
#define LINE_SIZE (CGSize){INPUT_WIDTH, 30}
#define GLYPH_FONT @"FateCoreGlyphs"

@interface FTCNoteLine()
{

}

@end

@implementation FTCNoteLine
@synthesize note = _note;
@synthesize noteField = _noteField;
@synthesize delegate = _delegate;
@synthesize lineWidth = _lineWidth;

+ (double) getInputWidth:(double)totalWidth
{
    return totalWidth - 35;
}

+ (FTCNoteLine *)noteLineWithNote:(FTCNote *)note delegate:(id<FTCNoteLineDelegate>)delegate
{
    double totalWidth = [FTCLayoutHelper getRowWidth:YES] - 8;
    
    double inputWidth = [FTCNoteLine getInputWidth:totalWidth];
    
    JVFloatLabeledTextView *noteField = [[JVFloatLabeledTextView alloc] initWithFrame:CGRectMake(0, 0, inputWidth, HEIGHT)];
    //highConceptField = self.characterName;
    //aspectField.borderStyle = UITextBorderStyleRoundedRect;
    noteField.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
    [noteField setPlaceholder: [note title]];
    [noteField setText: note.text];
    noteField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    noteField.keyboardType = UIKeyboardTypeAlphabet;
    noteField.returnKeyType = UIReturnKeyDefault;
    noteField.scrollEnabled = NO;
    
    MGBox *rightBox = nil;
    

    FAKIcon *icon = [FAKIonIcons ios7CloseOutlineIconWithSize:24];
    [icon addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]];
    UIImage *iconImage = [icon imageWithSize:(CGSize){35, 30}];
    
    UIButton *actionButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, iconImage.size.width, iconImage.size.height)];
    [actionButton setBackgroundImage:iconImage forState:UIControlStateNormal];
    actionButton.showsTouchWhenHighlighted = YES;
    rightBox = [MGBox boxWithSize:actionButton.size];
    [rightBox addSubview: actionButton]; //imgView];
    rightBox.rightMargin = 0;
    
    
    FTCNoteLine *noteLine = [FTCNoteLine lineWithLeft:noteField right:rightBox size:(CGSize){totalWidth, HEIGHT}];
    noteLine.delegate = delegate;
    noteLine.noteField = noteField;
    noteLine.note = note;
    noteLine.lineWidth = totalWidth;
    noteLine.sizingMode = MGResizingShrinkWrap;
    noteField.delegate = noteLine;
    [noteField layoutSubviews];
    
    CGSize textViewSize = [noteField sizeThatFits:CGSizeMake(noteField.frame.size.width, FLT_MAX)];
    noteField.height = textViewSize.height;
    [noteLine setHeight:noteField.height];
    [noteLine sizeToFit];
    
    UIToolbar *valueToolbar = [[UIToolbar alloc] initWithFrame:
                               CGRectMake(0,0, 320, 44)]; //should code with variables to support view resizing
    UIBarButtonItem *doneButton =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                  target:noteLine action:@selector(inputAccessoryViewDidFinish:)];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [valueToolbar setItems:[NSArray arrayWithObjects: flexibleSpace, doneButton, nil] animated:NO];
    noteField.inputAccessoryView = valueToolbar;
    
    
    //UIButton *actionButton = (UIButton *)[rightBox.subviews objectAtIndex:0];
    [actionButton bk_addEventHandler:^(id sender) {
        [noteLine.delegate noteLineWasRemoved:noteLine];
    } forControlEvents:UIControlEventTouchUpInside];
    
    return noteLine;
}

- (void)setup {
    [super setup];
    self.bottomMargin = 0;
    self.rasterize = YES;
    
}

- (void) layout {
    [super layout];
    
    [self setSize:(CGSize){self.lineWidth, HEIGHT}];
    [self.noteField setSize:(CGSize){[FTCNoteLine getInputWidth:self.lineWidth], HEIGHT}];
    CGSize textViewSize = [self.noteField sizeThatFits:CGSizeMake(self.noteField.frame.size.width, FLT_MAX)];
    if (self.noteField.height < textViewSize.height)
    {
        self.noteField.height = textViewSize.height;
        [self setHeight:self.noteField.height];
        [self sizeToFit];
    }
    for (MGBox *box in self.leftItems) {
        NSLog(@"LI %f", box.width);
    }
    for (MGBox *box in self.rightItems) {
        NSLog(@"RI %f", box.width);
    }
    NSLog(@"%f %f", self.width, self.noteField.width);
}

#pragma mark UITextViewDelegate
- (void)inputAccessoryViewDidFinish:(id) sender
{
    [self.noteField resignFirstResponder];
}

- (void) textViewDidChange:(UITextView *)textView
{
    CGSize textViewSize = [textView sizeThatFits:CGSizeMake(textView.frame.size.width, FLT_MAX)];
    
    if (textView.height < textViewSize.height) {
        NSLog(@"Height changed");
        textView.height = textViewSize.height;
        [self setHeight:textView.height];
        [self sizeToFit];
        [self.delegate noteLineHeightChanged:self];
    }
}

- (void) textViewDidEndEditing:(UITextView *)textView
{
    self.note.text = textView.text;
    [self.delegate noteLineChanged:self];
    [self.noteField layoutSubviews];
}


@end
