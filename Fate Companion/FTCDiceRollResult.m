//
//  FTCDiceRollResult.m
//  Fate Companion
//
//  Created by Joshua Barron on 3/7/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCDiceRollResult.h"

#define GLYPH_NEGATIVE @"-"
#define GLYPH_NEUTRAL @"0"
#define GLYPH_POSITIVE @"+"

#define GLYPH_FONT @"FateCoreGlyphs"

@implementation FTCDiceRollResult
@synthesize originalRoll = _originalRoll;
@synthesize modifier = _modifier;
@synthesize finalResult = _finalResult;

- (id) initWithRoll:(DieRollResult)first andSecond:(DieRollResult)second andThird:(DieRollResult)third andFourth:(DieRollResult)fourth andModifier:(int)modifier
{
    if (self = [super init])
    {
        self.firstDie = first;
        self.secondDie = second;
        self.thirdDie = third;
        self.fourthDie = fourth;
        self.originalRoll = first + second + third + fourth;
        self.modifier = modifier;
        self.finalResult = self.originalRoll + modifier;
    }
    return self;
}

- (NSAttributedString *)rollDescription
{
    return [self rollDescription:YES];
}

- (NSAttributedString *)rollDescription:(BOOL)includeResult
{
    UIFont *glyphFont = [UIFont fontWithName:GLYPH_FONT size:24];
    UIFont *sysFont = [UIFont systemFontOfSize:24];
    UIColor *fgColor = [UIColor blackColor];//[[[[UIApplication sharedApplication] delegate] window] tintColor];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSMutableString *str = [[NSMutableString alloc] initWithString:[self originalRollString]];
    
    [str appendString:@" "];
    [str appendString:[self modifierString]];
    if (includeResult) {
        [str appendString:@" = "];
        [str appendString:[NSString stringWithFormat:@"%d", self.finalResult]];
    }
    
    NSMutableAttributedString *description = [[NSMutableAttributedString alloc] initWithString:str attributes:@{NSForegroundColorAttributeName : fgColor, NSParagraphStyleAttributeName: paragraphStyle}];
    
    [description addAttribute:NSFontAttributeName value:glyphFont range:NSMakeRange(0, 4)];
    [description addAttribute:NSFontAttributeName value:sysFont range:NSMakeRange(4, description.length - 4)];
    return description;
}

- (NSString *)originalRollString
{
    NSMutableString *str = [[NSMutableString alloc] init];
    
    [str appendString:[self glyphForDieRollResult:self.firstDie]];
    [str appendString:[self glyphForDieRollResult:self.secondDie]];
    [str appendString:[self glyphForDieRollResult:self.thirdDie]];
    [str appendString:[self glyphForDieRollResult:self.fourthDie]];
    
    return str;
}

- (NSString *)modifierString
{
    if (self.modifier < 0)
        return [NSString stringWithFormat:@"- %d", abs(self.modifier)];
    
    return [NSString stringWithFormat:@"+ %d", self.modifier];
}

- (NSString *)glyphForDieRollResult:(DieRollResult)result
{
    if (result == RollNegative)
        return GLYPH_NEGATIVE;
    
    if (result == RollNeutral)
        return GLYPH_NEUTRAL;
    
    if (result == RollPositive)
        return GLYPH_POSITIVE;
    
    return @"UNK";
}

#pragma mark NSCoding

#define kDiceRollResultVersionKey @"DiceRollResultVersion"
#define kModifier @"Modifier"
#define kFirstDie @"FirstDie"
#define kSecondDie @"SecondDie"
#define kThirdDie @"ThirdDie"
#define kFourthDie @"FourthDie"

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeInt:1 forKey:kDiceRollResultVersionKey];
    [encoder encodeInt:self.modifier forKey:kModifier];
    [encoder encodeInt:self.firstDie forKey:kFirstDie];
    [encoder encodeInt:self.secondDie forKey:kSecondDie];
    [encoder encodeInt:self.thirdDie forKey:kThirdDie];
    [encoder encodeInt:self.fourthDie forKey:kFourthDie];
}

- (id)initWithCoder:(NSCoder *)decoder {
    [decoder decodeIntForKey:kDiceRollResultVersionKey];
    DieRollResult firstDie, secondDie, thirdDie, fourthDie;
    int modifier = [decoder decodeIntForKey:kModifier];
    firstDie = [decoder decodeIntForKey:kFirstDie];
    secondDie = [decoder decodeIntForKey:kSecondDie];
    thirdDie = [decoder decodeIntForKey:kThirdDie];
    fourthDie = [decoder decodeIntForKey:kFourthDie];
    
    return [self initWithRoll:firstDie andSecond:secondDie andThird:thirdDie andFourth:fourthDie andModifier:modifier];
}

@end
