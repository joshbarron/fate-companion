//
//  FTCSplitViewController.h
//  Fate Companion
//
//  Created by Joshua Barron on 3/15/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FTCSplitViewController : UISplitViewController

@property(nonatomic, weak) UIViewController *masterViewController;
@property(nonatomic, weak) UIViewController *detailViewController;

- (void)changeDetailViewController:(UINavigationController *)target;

@end
