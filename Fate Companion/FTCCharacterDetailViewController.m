//
//  FTCCharacterDetailViewController.m
//  Fate Companion
//
//  Created by Joshua Barron on 1/25/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCCharacterDetailViewController.h"
#import "FTCSidePanelControllerViewController.h"
#import "UIViewController+JASidePanel.h"
#import "UIView+FindFirstResponder.h"
#import "FTCDocument.h"
#import "FTCCharacter.h"
#import "UIImageExtras.h"
#import "MGBox.h"
#import "MGTableBox.h"
#import "MGLine.h"
#import "FTCPhotoBox.h"
#import "FTCDetailSectionBox.h"
#import "FontAwesomeKit/FAKIonIcons.h"
#import "FTCStandardSkillsHelper.h"
#import "FTCSkillValueTranslator.h"
#import "UIView+MGEasyFrame.h"
#import "CRToast.h"
#import "FTCNotificationHelper.h"
#import "FTCDiceRoller.h"
#import "FTCStressTrackBox.h"
#import "SIAlertView.h"
#import "FTCLayoutHelper.h"
#import "FTCGridBox.h"
#import "FTCDocumentHTMLExporter.h"

#define KEYBOARD_OFFSET 50

#define THUMBNAIL_TAG 244
#define THUMBNAIL_LENGTH 100
#define THUMBNAIL_SIZE (CGSize){THUMBNAIL_LENGTH, THUMBNAIL_LENGTH}

#define AS_CANCEL @"Cancel"

#define AS_CONVERTTOACCELERATED @"Convert to FAE Character"
#define AS_CONVERTTOCORE @"Convert to Core Character"
#define AS_DELETE @"Delete Character"
#define AS_GENPDF @"Export to PDF"

#define AS_STANDARD @"Add Standard Skill"
#define AS_CUSTOM @"Add Custom Skill"

#define AS_MILD @"Mild Consquence"
#define AS_MODERATE @"Moderate Consequence"
#define AS_SEVERE @"Severe Consequence"

#define TAG_SKILLACTIONSHEET 1100
#define TAG_CHARACTERMANAGEACTIONSHEET 1101
#define TAG_CONSEQUENCESHEET 1102

#define TAG_HIGHCONCEPT 1001
#define TAG_TROUBLE 1002
#define TAG_ASPECT 1003

#define TAG_DESCRIPTIONVIEW 1004
#define TAG_FPREFRESHLINE 1005
#define TAG_FPSTEPPER 1006
#define TAG_REFRESHSTEPPER 1007
#define TAG_CHARACTERNAME 1008

@interface FTCCharacterDetailViewController ()
- (void)configureView;
@end

@implementation FTCCharacterDetailViewController {
    UIImagePickerController * _picker;
    FTCGridBox * _masterBox;
    FTCStandardSkillsHelper *standardSkillsHelper;
    FTCSkillValueTranslator *translator;
    MGTableBox *headerBox;
    FTCPhotoBox *photoBox;
    FTCDetailSectionBox *fatePoints, *aspects, *approaches, *skills, *extras, *stunts, *stressTracks, *consequences;
    
    JVFloatLabeledTextField *characterNameField;
    JVFloatLabeledTextView *descriptionView;
    
    FTCSkill *editSkill;
    FTCStressTrack *editStressTrack;
    FTCApproach *editApproach;
    
    RMPickerViewController *standardSkillsPicker;
    
    CGSize currentKeyboardSize;
    NSArray *unusedSkillDefinitions;
    NSArray *skillValues;
    
    double skillPadding;
    
    UIPopoverController *imagePickerPopover;
    UIDocumentInteractionController *documentInteractionController;
    
    UIEdgeInsets defaultInsets;
    
    BNHtmlPdfKit *pdfKit;
    
    //__weak UIPopoverController *openPopover;
}

@synthesize doc = _doc;

@synthesize scroller = _scroller;
@synthesize delegate = _delegate;

#pragma mark - Managing the detail item

- (void)setDoc:(id)newDoc
{
    if (_doc != newDoc) {
        _doc = newDoc;
        
        // Update the view.
        [self configureView];
    }
}

#pragma mark Header

- (void)setupHeader
{
    UIColor *tintColor = [FTCLayoutHelper getTintColor];
    double rowWidth = [FTCLayoutHelper getRowWidth:NO];
    headerBox = [MGTableBox boxWithSize:(CGSize){rowWidth, 0}];
    [_masterBox.boxes addObject:headerBox];
    
    NSMutableString *nameStr = [NSMutableString stringWithString:@"**"];
    [nameStr appendString:_doc.name];
    [nameStr appendString:@"**|mush"];
    
    characterNameField = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(0, 0, rowWidth, 40)];
    characterNameField.delegate = self;
    [characterNameField setPlaceholder:@"Name"];
    characterNameField.text = self.doc.name;
    characterNameField.font = [UIFont fontWithName:@"HelveticaNeue" size:22];
    characterNameField.tag = TAG_CHARACTERNAME;
    
    MGLine *nameRow = [MGLine lineWithLeft:characterNameField right:nil size:(CGSize){rowWidth, 40}];
    //MGLine *nameRow = [MGLine lineWithLeft:[NSString stringWithString:nameStr] right:nil size:(CGSize){rowWidth, 40}];
    //nameRow.topMargin = 4;
    //nameRow.font = [UIFont fontWithName:@"HelveticaNeue" size:22];
    nameRow.borderStyle = MGBorderEtchedTop | MGBorderEtchedBottom;
    nameRow.bottomBorderColor = [UIColor colorWithWhite:.6f alpha:1];
    nameRow.bottomMargin = 8;
    [headerBox.topLines addObject:nameRow];
    
    UIImage *photo = _doc.photo;
    if (photo == nil)
    {
        FAKIcon *personIcon = [FAKIonIcons ios7PersonOutlineIconWithSize:48];
        [personIcon addAttribute:NSForegroundColorAttributeName value:tintColor];
        UIImage *defaultPic = [personIcon imageWithSize:THUMBNAIL_SIZE];
        photo = defaultPic;
    }
    photoBox = [FTCPhotoBox photoBoxFor:photo withSize:THUMBNAIL_SIZE];
    photoBox.tag = THUMBNAIL_TAG;
    __weak FTCCharacterDetailViewController *wself = self;
    photoBox.onTap = ^{
        [wself imageTapped];
    };
    
    float width = rowWidth - photoBox.width - photoBox.leftMargin - photoBox.rightMargin;
    MGBox *statusBox = [MGBox boxWithSize:(CGSize){width - 8, THUMBNAIL_LENGTH}];
    statusBox.contentLayoutMode = MGLayoutGridStyle;
    statusBox.leftMargin = 8;
    descriptionView = [[JVFloatLabeledTextView alloc] initWithFrame:CGRectMake(0, 0, width - 8, THUMBNAIL_LENGTH)];
    descriptionView.returnKeyType = UIReturnKeyDone;
    [descriptionView setFont:[UIFont systemFontOfSize:14]];
    [descriptionView setPlaceholder:@"Description"];
    [descriptionView setText:self.doc.characterDescription];
    [descriptionView setTag:TAG_DESCRIPTIONVIEW];
    [descriptionView setDelegate:self];
    [descriptionView setScrollEnabled:YES];
    
    //descriptionView.delaysContentTouches = NO;
    
    MGBox *descriptionBox = [MGBox boxWithSize:(CGSize){width - 8, THUMBNAIL_LENGTH}];
    [descriptionBox addSubview:descriptionView];
    
    [statusBox.boxes addObject:descriptionBox];
    
    MGLine *headerRow = [MGLine lineWithLeft:photoBox right:statusBox size:(CGSize){rowWidth, THUMBNAIL_LENGTH}];
    //photoRow.rightItemsAlignment = NSTextAlignmentJustified;
    [headerBox.topLines addObject:headerRow];
    
    [descriptionView layoutSubviews];
    
}

- (void) resizeHeaderSection:(UIInterfaceOrientation)orientation
{
    double width = [FTCLayoutHelper getRowWidthForOrientation:orientation doubleWide:NO];
    
    
    MGLine *nameRow = (MGLine *)[headerBox.topLines firstObject];
    [nameRow setSize:(CGSize){width, nameRow.size.height}];
    [characterNameField setSize:(CGSize){width, characterNameField.size.height}];
    MGLine *headerRow = (MGLine *)[headerBox.topLines objectAtIndex:1];
    [headerRow setSize:(CGSize){width, headerRow.size.height}];
    
    //resize the des and and its box
    MGBox *descriptionBox = (MGBox *)[headerRow.rightItems objectAtIndex:0];
    float desWidth = width - photoBox.width - photoBox.leftMargin - photoBox.rightMargin - 8;
    CGSize desSize = (CGSize){desWidth, descriptionBox.size.height};
    [descriptionBox setSize:desSize];
    [descriptionView setSize:desSize];
    
    [headerBox setSize:(CGSize){width, headerBox.size.height}];
    //[headerBox layout];
    //[consequences setSize:(CGSize){width, consequences.size.height}];
}

#pragma mark Fate points and refresh
- (void)setupFatePointsAndRefreshSection
{
    fatePoints = [FTCDetailSectionBox boxWithSize:(CGSize){[FTCLayoutHelper getRowWidth:NO], 0} andTitle:@"Fate Points & Refresh" andActionImage:nil andActionCallback:nil];
    
    [_masterBox.boxes addObject:fatePoints];
    
    double inputWidth = [FTCLayoutHelper getInputWidth:NO];
    
    MGLine *fpHeaderLine = [MGLine lineWithLeft:@"Fate Points" right:@"Refresh" size:(CGSize){inputWidth, 30}];
    fpHeaderLine.font = [UIFont boldSystemFontOfSize:14];
    fpHeaderLine.leftItemsAlignment = NSTextAlignmentCenter;
    fpHeaderLine.rightItemsAlignment = NSTextAlignmentCenter;
    fpHeaderLine.leftWidth = inputWidth / 2;
    fpHeaderLine.rightWidth = inputWidth / 2;
    
    NSString *fp = [self.doc.fatePoints stringValue];
    NSString *refresh = [self.doc.refresh stringValue];
    
    MGLine *valueLine = [MGLine lineWithLeft:fp right:refresh size:(CGSize){inputWidth, 30}];
    valueLine.font = [UIFont systemFontOfSize:30];
    valueLine.leftItemsAlignment = NSTextAlignmentCenter;
    valueLine.rightItemsAlignment = NSTextAlignmentCenter;
    valueLine.leftWidth = inputWidth / 2;
    valueLine.rightWidth = inputWidth / 2;
    valueLine.tag = TAG_FPREFRESHLINE;
    
    UIStepper *fpStepper = [[UIStepper alloc] initWithFrame:CGRectMake(0, 10, 0, 0)];
    [fpStepper addTarget:self action:@selector(stepperValueChanged:) forControlEvents:UIControlEventValueChanged];
    fpStepper.value = [self.doc.fatePoints doubleValue];
    fpStepper.minimumValue = 0;
    fpStepper.stepValue = 1;
    fpStepper.tag = TAG_FPSTEPPER;
    double fpX = (inputWidth / 4) - (fpStepper.width / 2);
    [fpStepper setX:fpX];
    
    UIStepper *refreshStepper = [[UIStepper alloc] initWithFrame:CGRectMake(0, 10, 0, 0)];
    [refreshStepper addTarget:self action:@selector(stepperValueChanged:) forControlEvents:UIControlEventValueChanged];
    refreshStepper.value = [self.doc.refresh doubleValue];
    refreshStepper.minimumValue = 0;
    refreshStepper.stepValue = 1;
    refreshStepper.tag = TAG_REFRESHSTEPPER;
    double refreshX = (3 * inputWidth / 4) - (refreshStepper.width / 2);
    [refreshStepper setX:refreshX];
    
    MGBox *stepperLine = [MGBox boxWithSize:(CGSize){inputWidth, 35}];
    [stepperLine addSubview:fpStepper];
    [stepperLine addSubview:refreshStepper];
    
    [fatePoints.middleLines addObject:fpHeaderLine];
    [fatePoints.middleLines addObject:valueLine];
    [fatePoints.middleLines addObject:stepperLine];
}

- (void) resizeFatePointsAndRefreshSection:(UIInterfaceOrientation)orientation
{
    double width = [FTCLayoutHelper getRowWidthForOrientation:orientation doubleWide:NO];
    double inputWidth = [FTCLayoutHelper getInputWidthForOrientation:orientation doubleWide:NO];
    
    
    MGLine *fpHeaderLine = (MGLine *)[fatePoints.middleLines objectAtIndex:0];
    [fpHeaderLine setSize:(CGSize){width, 30}];
    fpHeaderLine.leftWidth = inputWidth / 2;
    fpHeaderLine.rightWidth = inputWidth / 2;
    
    MGLine *valueLine = (MGLine *)[fatePoints.middleLines objectAtIndex:1];
    [valueLine setSize:(CGSize){width, 30}];
    valueLine.leftWidth = inputWidth / 2;
    valueLine.rightWidth = inputWidth / 2;
    
    MGLine *stepperLine = (MGLine *)[fatePoints.middleLines objectAtIndex:2];
    [stepperLine setSize:(CGSize){width, 35}];
    UIStepper *fpStepper = (UIStepper *)[stepperLine viewWithTag:TAG_FPSTEPPER];
    UIStepper *refreshStepper = (UIStepper *)[stepperLine viewWithTag:TAG_REFRESHSTEPPER];
    double fpX = (inputWidth / 4) - (fpStepper.width / 2);
    [fpStepper setX:fpX];
    double refreshX = (3 * inputWidth / 4) - (refreshStepper.width / 2);
    [refreshStepper setX:refreshX];
    
    [fatePoints setSize:(CGSize){width, fatePoints.size.height}];
    //[fatePoints layout];
    
}

- (void)stepperValueChanged:(UIStepper *) stepper
{
    MGLine *valueLine = (MGLine *)[fatePoints viewWithTag:TAG_FPREFRESHLINE];
    int newVal = (int)stepper.value;
    NSString *newValString = [NSString stringWithFormat:@"%d", newVal];
    if (stepper.tag == TAG_FPSTEPPER)
    {
        [self.doc setFatePoints:[NSNumber numberWithInt:newVal]];
        [valueLine setMultilineLeft:newValString];
    }
    else if (stepper.tag == TAG_REFRESHSTEPPER)
    {
        [self.doc setRefresh:[NSNumber numberWithInt:newVal]];
        [valueLine setMultilineRight:newValString];
    }
    [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) { NSLog(@"Document saved."); }];
    [valueLine layout];
}



#pragma mark Aspects

- (void)addAspect
{
    FTCAspect *aspect = [self.doc addAspectWithText:nil];
    [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) { NSLog(@"Document saved."); }];
    FTCAspectLine *newAspect = [FTCAspectLine aspectLineWithAspect:aspect placeholder:@"Aspect" tag:TAG_ASPECT mandatory:NO delegate:self];
    
    [aspects.middleLines addObject:newAspect];
    [self relayout];
}

- (void) aspectLineWasRemoved:(FTCAspectLine *)aspectLine
{
    [self.doc removeAspect:aspectLine.aspect];
    [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) { NSLog(@"Document saved."); }];
    
    [UIView animateWithDuration:.3 animations:^{
        aspectLine.alpha = 0.0;
        aspectLine.transform = CGAffineTransformMakeScale(.1, .1);
    } completion:^(BOOL finished) {
        [aspects.middleLines removeObject:aspectLine];
        [self relayout];
    }];
}

- (void) aspectLineChanged:(FTCAspectLine *)aspectLine withText:(NSString *)text
{
    if (aspectLine.tag == TAG_HIGHCONCEPT)
    {
        [self.doc setHighConcept:text];
    }
    else if (aspectLine.tag == TAG_TROUBLE)
    {
        [self.doc setTrouble:text];
    }
    else
    {
        [self.doc updateAspect:aspectLine.aspect withText:text];
    }
    [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) { NSLog(@"Document saved."); }];
    [self relayout];
}

- (void) aspectLineHeightChanged:(FTCAspectLine *)aspectLine
{
    [self relayout];
    // if there is a selection cursor…
    UITextView *textView = aspectLine.aspectField;
    
    if(textView.selectedRange.location != NSNotFound) {
        NSRange range;
        range.location = textView.selectedRange.location;
        range.length = textView.text.length - range.location;
        NSString *string = [textView.text stringByReplacingCharactersInRange:range withString:@""];
        //CGSize size = [string boundingRectWithSize:textView.bounds.size options:NSStringDrawingUsesDeviceMetrics attributes:nil context:nil].size;
        CGRect target = [aspectLine convertRect:textView.frame toView:nil];
        CGSize size = [string sizeWithFont:textView.font constrainedToSize:textView.bounds.size];
        
        // work out where that position would be relative to the textView's frame
        CGRect viewRect = textView.frame;
        int scrollHeight = viewRect.origin.y + size.height + target.origin.y + KEYBOARD_OFFSET;
        CGRect finalRect = CGRectMake(0, scrollHeight, 1, 1);//CGRectMake(0, scrollHeight, 1, 30);
        
        // scroll to it
        [self.scroller scrollRectToVisible:finalRect animated:YES];
    }
    //    if(textView.selectedRange.location != NSNotFound) {
    //        UIView *viewToScrollTo;
    //        if (aspectLine == [aspects.middleLines lastObject]) {
    //            viewToScrollTo = skills;
    //        }
    //        else
    //        {
    //            viewToScrollTo = [aspects.middleLines objectAtIndex:[aspects.middleLines indexOfObject:aspectLine] + 1];
    //        }
    //
    //        [self.scroller scrollToView:viewToScrollTo withMargin:currentKeyboardSize.height + self.scroller.keyboardMargin]; //44 is the accessory view height
    //
    //    }
}

- (void)setupAspectsSection
{
    FAKIcon *aspectsIcon = [FAKIonIcons ios7PlusOutlineIconWithSize:24];
    [aspectsIcon addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
    UIImage *aspectsIconImage = [aspectsIcon imageWithSize:(CGSize){30, 30}];
    aspects = [FTCDetailSectionBox boxWithSize:(CGSize){[FTCLayoutHelper getRowWidth:YES], 0} andTitle:@"Aspects" andActionImage:aspectsIconImage andActionCallback:^(id sender){
        [self addAspect];
    }];
    
    if ([FTCLayoutHelper deviceIsIPad]) {
        aspects.topMargin = 4;
    }
    
    [_masterBox.boxes addObject:aspects];
    
    FTCAspectLine *highConceptLine = [FTCAspectLine aspectLineWithAspect: self.doc.highConcept placeholder:@"High Concept" tag:TAG_HIGHCONCEPT mandatory:YES delegate:self];
    
    [aspects.middleLines addObject:highConceptLine];
    
    FTCAspectLine *troubleLine = [FTCAspectLine aspectLineWithAspect:self.doc.trouble placeholder:@"Trouble" tag:TAG_TROUBLE mandatory:YES delegate:self];
    [aspects.middleLines addObject:troubleLine];
    
    for (FTCAspect *aspect in self.doc.aspects) {
        FTCAspectLine *aspectLine = [FTCAspectLine aspectLineWithAspect:aspect placeholder:@"Aspect" tag:TAG_ASPECT mandatory:NO delegate:self];
        [aspects.middleLines addObject:aspectLine];
    }
}

- (void) resizeAspectsSection:(UIInterfaceOrientation)orientation
{
    double width = [FTCLayoutHelper getRowWidthForOrientation:orientation doubleWide:YES];
    double inputWidth = [FTCLayoutHelper getRowWidthForOrientation:orientation doubleWide:YES] - 8;//[FTCLayoutHelper getInputWidth];
    
    for (FTCAspectLine *line in aspects.middleLines) {
        line.lineWidth = inputWidth;
    }
    
    [aspects setSize:(CGSize){width, aspects.size.height}];
}

#pragma mark Approaches

- (void)addApproach
{
    editApproach = nil;
    [self performSegueWithIdentifier:@"openApproachEntry" sender:self];
}

- (BOOL) validateApproachEntry:(FTCApproachEntryViewController *)approachEntryController withMessage:(NSString *__autoreleasing *)validationMessage
{
    FTCApproach *approach = approachEntryController.approach;
    
    NSString *proposedName = approachEntryController.nameTextField.text;
    
    NSArray *existingApproachNames = [self.doc.approaches valueForKeyPath:@"name"];
    if ([existingApproachNames containsObject:proposedName])
    {
        //it's ok if it's an existing approach and they're not changing the name
        if (approach == nil || ![approach.name isEqualToString:proposedName])
        {
            NSString *error = @"You already have an approach with that name.";
            *validationMessage = error;
            return NO;
        }
    }
    return YES;
}

- (void) approachWasSaved:(FTCApproachEntryViewController *)approachEntryController
{
    [approaches removeEmptyMessage];
    if (approachEntryController.approach == nil) //New
    {
        [self.doc addApproachWithName:approachEntryController.nameTextField.text andValue:[NSNumber numberWithInt:approachEntryController.selectedApproachValue] isCustom:YES];
    }
    else //Edit
    {
        FTCApproach *approach = approachEntryController.approach;
        [self.doc changeApproachName:approach toName:approachEntryController.nameTextField.text];
        [self.doc changeApproachValue:approach toValue:[NSNumber numberWithInt:approachEntryController.selectedApproachValue]];
    }
    
    [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self displayOrderedApproachLines];
            [self relayout];
            [CRToastManager showNotificationWithOptions:[FTCNotificationHelper successNotification:@"Approach saved!"] completionBlock:nil];
            [self.scroller scrollToView:approaches withMargin:0];
        });
    }];

}

- (void) approachLineWasRemoved:(FTCApproachLine *)approachLine
{
    [self.doc removeApproach:approachLine.approach];
    [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) { NSLog(@"Document saved."); }];
    
    [UIView animateWithDuration:.3 animations:^{
        approachLine.alpha = 0.0;
        approachLine.transform = CGAffineTransformMakeScale(.1, .1);
    } completion:^(BOOL finished) {
        [approaches.middleLines removeObject:approachLine];
        [approaches showEmptyMessage];
        [self relayout];
    }];

}

- (void) approachLineWasTapped:(FTCApproachLine *)approachLine
{
    editApproach = approachLine.approach;
    [self performSegueWithIdentifier:@"openApproachEntry" sender:self];
}

- (void) rollDiceForApproachLine:(FTCApproachLine *)approachLine
{
    FTCDiceRollResult *roll = [approachLine.approach rollDice];
    
    NSMutableString *title = [[NSMutableString alloc] initWithString:@"Dice Roll ("];
    [title appendString:approachLine.approach.name];
    [title appendString:@")"];
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:title andMessage:@""];
    [alertView setAttributedMessage:[roll rollDescription]];
    alertView.messageFont = [UIFont systemFontOfSize:24];
    
    [alertView addButtonWithTitle:@"Roll Again"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alert) {
                              [self rollDiceForApproachLine:approachLine];
                          }];
    [alertView addButtonWithTitle:@"OK"
                             type:SIAlertViewButtonTypeDefault
                          handler:nil];
    alertView.transitionStyle = SIAlertViewTransitionStyleFade;
    
    [alertView show];
}

- (void) setupApproachesSection
{
    FAKIcon *approachesIcon = [FAKIonIcons ios7PlusOutlineIconWithSize:24];
    [approachesIcon addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
    UIImage *approachesIconImage = [approachesIcon imageWithSize:(CGSize){30, 30}];
    approaches = [FTCDetailSectionBox boxWithSize:(CGSize){[FTCLayoutHelper getRowWidth:NO], 0} andTitle:@"Approaches" andActionImage:approachesIconImage andActionCallback:^(id sender){
        [self addApproach];
    }];
    
    [_masterBox.boxes addObject:approaches];
    [self displayOrderedApproachLines];
}

- (void) displayOrderedApproachLines
{
    [approaches.middleLines removeAllObjects];
    
    SkillSortKey sortKey = [FTCUserSettings skillSortOrder];
    NSArray *sortedApproaches = (sortKey == ByValue) ? self.doc.approachesSortedByValue : self.doc.approachesSortedAlphabetically;
    
    if (sortedApproaches.count == 0)
    {
        [approaches showEmptyMessage];
    }
    else
    {
        for (FTCApproach *approach in sortedApproaches) {
            FTCApproachLine *approachLine = [FTCApproachLine approachLineWithApproach:approach delegate:self];
            [approaches.middleLines addObject:approachLine];
        }
    }
}

- (void) resizeApproachesSection:(UIInterfaceOrientation)orientation
{
    double inputWidth = [FTCLayoutHelper getRowWidthForOrientation:orientation doubleWide:NO];
    
    BOOL redoEmptyMessage = self.doc.approaches.count == 0;
    
    if (!redoEmptyMessage) {
        for (FTCApproachLine *line in approaches.middleLines) {
            line.lineWidth = inputWidth;
        }
    }
    else {
        [approaches.middleLines removeAllObjects];
    }
    [approaches setSize:(CGSize){inputWidth, approaches.size.height}];
    if (redoEmptyMessage)
        [approaches showEmptyMessage];
}


#pragma mark Skills

- (void)addSkill
{
    editSkill = nil;
    //@"Would you like to add a standard FATE skill or a custom skill?"
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:AS_CANCEL destructiveButtonTitle:nil otherButtonTitles:AS_STANDARD, AS_CUSTOM, nil];
    actionSheet.tag = TAG_SKILLACTIONSHEET;
    [actionSheet showInView:self.view];
}

- (BOOL) validateSkillEntry:(FTCSkillEntryViewController *)skillEntryController withMessage:(NSString *__autoreleasing *)validationMessage
{
    FTCSkill *skill = skillEntryController.skill;
    
    NSString *proposedName = skillEntryController.nameTextField.text;
    
    NSArray *existingSkillNames = [self.doc.skills valueForKeyPath:@"name"];
    if ([existingSkillNames containsObject:proposedName])
    {
        //it's ok if it's an existing skill and they're not changing the name
        if (skill == nil || ![skill.name isEqualToString:proposedName])
        {
            NSString *error = @"You already have a skill with that name.";
            *validationMessage = error;
            return NO;
        }
    }
    return YES;
}

- (void) skillWasSaved:(FTCSkillEntryViewController *)skillEntryController
{
    [skills removeEmptyMessage];
    if (skillEntryController.skill == nil) //New
    {
        [self.doc addSkillWithName:skillEntryController.nameTextField.text andValue:[NSNumber numberWithInt:skillEntryController.selectedSkillValue] isCustom:YES andActions:skillEntryController.skillActions];
    }
    else //Edit
    {
        NSString *skillName = skillEntryController.skill.name;
        
        [self.doc changeSkillWithName:skillName toValue:[NSNumber numberWithInt:skillEntryController.selectedSkillValue]];
        [self.doc changeSkillWithName:skillName toActions:[NSNumber numberWithInt:skillEntryController.skillActions]];
        [self.doc changeSkillWithName:skillName toNewName:skillEntryController.nameTextField.text];
    }
    
    [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self displayOrderedSkillLines];
            [self relayout];
            [CRToastManager showNotificationWithOptions:[FTCNotificationHelper successNotification:@"Skill saved!"] completionBlock:nil];
            [self.scroller scrollToView:skills withMargin:0];
        });
    }];
    
    
}

- (void) skillLineWasRemoved:(FTCSkillLine *)skillLine
{
    [self.doc removeSkill:skillLine.skill];
    [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) { NSLog(@"Document saved."); }];
    
    [UIView animateWithDuration:.3 animations:^{
        skillLine.alpha = 0.0;
        skillLine.transform = CGAffineTransformMakeScale(.1, .1);
    } completion:^(BOOL finished) {
        [skills.middleLines removeObject:skillLine];
        [skills showEmptyMessage];
        [self relayout];
    }];
    
}

- (void) rollDiceForSkillLine:(FTCSkillLine *)skillLine
{
    FTCDiceRollResult *roll = [skillLine.skill rollDice];
    
    NSMutableString *title = [[NSMutableString alloc] initWithString:@"Dice Roll ("];
    [title appendString:skillLine.skill.name];
    [title appendString:@")"];
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:title andMessage:@""];
    [alertView setAttributedMessage:[roll rollDescription]];
    alertView.messageFont = [UIFont systemFontOfSize:24];
    
    [alertView addButtonWithTitle:@"Roll Again"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alert) {
                              [self rollDiceForSkillLine:skillLine];
                          }];
    [alertView addButtonWithTitle:@"OK"
                             type:SIAlertViewButtonTypeDefault
                          handler:nil];
    alertView.transitionStyle = SIAlertViewTransitionStyleFade;
    
    [alertView show];
    
}

- (void) skillLineWasTapped:(FTCSkillLine *)skillLine
{
    editSkill = skillLine.skill;
    [self performSegueWithIdentifier:@"openSkillEntry" sender:self];
}

- (void) setSkillPaddingForOrientation:(UIInterfaceOrientation)orientation
{
    skillPadding = UIDeviceOrientationIsPortrait(orientation) ? 20 : 28;
}

- (double) getSkillPadding
{
    return skillPadding;
}

- (void) setupSkillsSection
{
    FAKIcon *skillsIcon = [FAKIonIcons ios7PlusOutlineIconWithSize:24];
    [skillsIcon addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
    UIImage *skillsIconImage = [skillsIcon imageWithSize:(CGSize){30, 30}];
    skills = [FTCDetailSectionBox boxWithSize:(CGSize){[FTCLayoutHelper getRowWidth:YES], 0} andTitle:@"Skills" andActionImage:skillsIconImage andActionCallback:^(id sender){
        [self addSkill];
    }];
    [self setSkillPaddingForOrientation:[FTCLayoutHelper getDeviceOrientation]];

    if ([FTCLayoutHelper deviceIsIPad])
    {
        skills.contentLayoutMode = MGLayoutGridStyle;
        __weak FTCCharacterDetailViewController *wself = self;
        skills.layoutBlock = ^(FTCDetailSectionBox *section) {
            for (int i = 0; i < section.middleLines.count; i++) {
                MGLine *line = (MGLine *)[section.middleLines objectAtIndex:i];
                double margin = [wself getSkillPadding];
                if (i % 2 == 0) {
                    //it's on the left, so change the right margin
                    line.rightMargin = margin;
                    line.leftMargin = 0; //explicity set this in case of removal
                }
                else
                {
                    line.leftMargin = margin;
                    line.rightMargin = 0;
                }
            }
        };
        
        
        skills.lastLinesBorderBlock = ^(FTCDetailSectionBox *section){
            BOOL isLastLineFull = (section.middleLines.count > 0) && (section.middleLines.count % 2 == 0);
            return isLastLineFull ? 2 : 1;
        };
    }
    [_masterBox.boxes addObject:skills];
    [self displayOrderedSkillLines];
}

- (void) displayOrderedSkillLines
{
    [skills.middleLines removeAllObjects];
    
    SkillSortKey sortKey = [FTCUserSettings skillSortOrder];
    NSArray *sortedSkills = (sortKey == ByValue) ? self.doc.skillsSortedByValue : self.doc.skillsSortedAlphabetically;
    
    if (sortedSkills.count == 0)
    {
        [skills showEmptyMessage];
    }
    else
    {
        for (FTCSkill *skill in sortedSkills) {
            FTCSkillLine *skillLine = [FTCSkillLine skillLineWithSkill:skill delegate:self];
            [skills.middleLines addObject:skillLine];
        }
    }
}

- (void) resizeSkillsSection:(UIInterfaceOrientation)orientation
{
    double width = [FTCLayoutHelper getRowWidthForOrientation:orientation doubleWide:YES];
    double inputWidth = [FTCLayoutHelper getRowWidthForOrientation:orientation doubleWide:NO] - 8;
    
    //set the padding to the target interface
    [self setSkillPaddingForOrientation:orientation];
    
    BOOL redoEmptyMessage = self.doc.skills.count == 0;
    
    if (!redoEmptyMessage) {
        for (FTCSkillLine *line in skills.middleLines) {
            line.lineWidth = inputWidth;
        }
    }
    else {
        [skills.middleLines removeAllObjects];
    }
    [skills setSize:(CGSize){width, skills.size.height}];
    if (redoEmptyMessage)
        [skills showEmptyMessage];
}

#pragma mark Stunts

- (void) displayStuntLines
{
    [stunts.middleLines removeAllObjects];
    
    NSArray *stuntsList = self.doc.stunts;
    
    if (stuntsList.count == 0)
    {
        [stunts showEmptyMessage];
    }
    else
    {
        for (FTCStunt *stunt in stuntsList) {
            FTCStuntLine *stuntLine = [FTCStuntLine stuntLineWithStunt:stunt delegate:self];
            [stunts.middleLines addObject:stuntLine];
        }
    }
}

- (void)addStunt
{
    [stunts removeEmptyMessage];
    [self.doc addStuntWithName:nil andStuntText:nil];
    [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self displayStuntLines];
            [self relayout];
            [self.scroller scrollToView:stunts withMargin:0];
        });
    }];
}

- (void)stuntLineWasRemoved:(FTCStuntLine *)stuntLine
{
    [self.doc removeStunt:stuntLine.stunt];
    [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) { NSLog(@"Document saved."); }];
    
    [UIView animateWithDuration:.3 animations:^{
        stuntLine.alpha = 0.0;
        stuntLine.transform = CGAffineTransformMakeScale(.1, .1);
    } completion:^(BOOL finished) {
        [stunts.middleLines removeObject:stuntLine];
        [stunts showEmptyMessage];
        [self relayout];
    }];
}

- (void)stuntLineHeightChanged:(FTCStuntLine *)stuntLine
{
    [self relayout];
    // if there is a selection cursor…
    UITextView *textView = stuntLine.stuntTextField;
    if(textView.selectedRange.location != NSNotFound) {
        NSRange range;
        range.location = textView.selectedRange.location;
        range.length = textView.text.length - range.location;
        NSString *string = [textView.text stringByReplacingCharactersInRange:range withString:@""];
        //CGSize size = [string boundingRectWithSize:textView.bounds.size options:NSStringDrawingUsesDeviceMetrics attributes:nil context:nil].size;
        CGRect target = [stuntLine convertRect:textView.frame toView:nil];
        CGSize size = [string sizeWithFont:textView.font constrainedToSize:textView.bounds.size];
        
        // work out where that position would be relative to the textView's frame
        CGRect viewRect = textView.frame;
        int scrollHeight = viewRect.origin.y + size.height + target.origin.y + KEYBOARD_OFFSET;
        CGRect finalRect = CGRectMake(0, scrollHeight, 1, 1);//CGRectMake(0, scrollHeight, 1, 30);
        
        // scroll to it
        [self.scroller scrollRectToVisible:finalRect animated:YES];
    }
}

- (void)stuntLineNameChanged:(FTCStuntLine *)stuntLine withText:(NSString *)text
{
    [self.doc changeStuntName:stuntLine.stunt toName:text];
    [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) { NSLog(@"Document saved."); }];
    [self relayout];
}

- (void)stuntLineTextChanged:(FTCStuntLine *)stuntLine withText:(NSString *)text
{
    [self.doc changeStuntText:stuntLine.stunt toText:text];
    [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) { NSLog(@"Document saved."); }];
    [self relayout];
}

- (void) setupStuntsSection
{
    FAKIcon *stuntsIcon = [FAKIonIcons ios7PlusOutlineIconWithSize:24];
    [stuntsIcon addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
    UIImage *stuntsIconImage = [stuntsIcon imageWithSize:(CGSize){30, 30}];
    stunts = [FTCDetailSectionBox boxWithSize:(CGSize){[FTCLayoutHelper getRowWidth:NO], 0} andTitle:@"Stunts" andActionImage:stuntsIconImage andActionCallback:^(id sender){
        [self addStunt];
    }];
    
    [stunts showEmptyMessage];
    
    [_masterBox.boxes addObject:stunts];
    
    [self displayStuntLines];
    
}

- (void) resizeStuntsSection:(UIInterfaceOrientation)orientation
{
    double inputWidth = [FTCLayoutHelper getRowWidthForOrientation:orientation doubleWide:NO];
    
    BOOL redoEmptyMessage = self.doc.stunts.count == 0;
    
    if (!redoEmptyMessage) {
        for (FTCStuntLine *line in stunts.middleLines) {
            line.lineWidth = inputWidth - 8;
        }
    }
    else {
        [stunts.middleLines removeAllObjects];
    }
    [stunts setSize:(CGSize){inputWidth, stunts.size.height}];
    if (redoEmptyMessage)
        [stunts showEmptyMessage];
    
    //    double width = [FTCLayoutHelper getRowWidthForOrientation:orientation doubleWide:NO];
    //
    //    [stunts setSize:(CGSize){width, stunts.size.height}];
}

#pragma mark Extras

- (void) displayExtraLines
{
    [extras.middleLines removeAllObjects];
    
    NSArray *extrasList = self.doc.extras;
    
    if (extrasList.count == 0)
    {
        [extras showEmptyMessage];
    }
    else
    {
        for (FTCExtra *extra in extrasList) {
            FTCExtraLine *extraLine = [FTCExtraLine extraLineWithExtra:extra delegate:self];
            [extras.middleLines addObject:extraLine];
        }
    }
}

- (void) addExtra
{
    [extras removeEmptyMessage];
    [self.doc addExtraWithName:nil];
    [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self displayExtraLines];
            [self relayout];
            [self.scroller scrollToView:extras withMargin:0];
        });
    }];
}

- (void) extraLineNameChanged:(FTCExtraLine *)extraLine withText:(NSString *)text
{
    [self.doc changeExtraName:extraLine.extra toName:text];
    [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) { NSLog(@"Document saved."); }];
    [self relayout];
}

- (void) extraLineWasRemoved:(FTCExtraLine *)extraLine
{
    [self.doc removeExtra:extraLine.extra];
    [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) { NSLog(@"Document saved."); }];
    
    [UIView animateWithDuration:.3 animations:^{
        extraLine.alpha = 0.0;
        extraLine.transform = CGAffineTransformMakeScale(.1, .1);
    } completion:^(BOOL finished) {
        [extras.middleLines removeObject:extraLine];
        [extras showEmptyMessage];
        [self relayout];
    }];
    
}

- (void) setupExtrasSection
{
    FAKIcon *extrasIcon = [FAKIonIcons ios7PlusOutlineIconWithSize:24];
    [extrasIcon addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
    UIImage *extrasIconImage = [extrasIcon imageWithSize:(CGSize){30, 30}];
    extras = [FTCDetailSectionBox boxWithSize:(CGSize){[FTCLayoutHelper getRowWidth:NO], 0} andTitle:@"Extras" andActionImage:extrasIconImage andActionCallback:^(id sender){
        [self addExtra];
    }];
    
    [extras showEmptyMessage];
    
    [_masterBox.boxes addObject:extras];
    
    [self displayExtraLines];
}

- (void) resizeExtrasSection:(UIInterfaceOrientation)orientation
{
    double inputWidth = [FTCLayoutHelper getRowWidthForOrientation:orientation doubleWide:NO];
    
    BOOL redoEmptyMessage = self.doc.extras.count == 0;
    
    if (!redoEmptyMessage) {
        for (FTCExtraLine *line in extras.middleLines) {
            line.lineWidth = inputWidth - 8;
        }
    }
    else {
        [extras.middleLines removeAllObjects];
    }
    [extras setSize:(CGSize){inputWidth, extras.size.height}];
    if (redoEmptyMessage)
        [extras showEmptyMessage];
    
    //    double width = [FTCLayoutHelper getRowWidthForOrientation:orientation doubleWide:NO];
    //
    //    [extras setSize:(CGSize){width, extras.size.height}];
}

#pragma mark Stress Tracks

- (void) addStressTrack
{
    editStressTrack = nil;
    [self performSegueWithIdentifier:@"openStressTrackEntry" sender:self];
}

- (void) stressTrackWasTapped:(FTCStressTrackLine *)stressTrackLine
{
    editStressTrack = stressTrackLine.stressTrack;
    [self performSegueWithIdentifier:@"openStressTrackEntry" sender:self];
}

- (void) stressTrackWasRemoved:(FTCStressTrackLine *)stressTrackLine
{
    [self.doc removeStressTrack:stressTrackLine.stressTrack];
    [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) { NSLog(@"Document saved."); }];
    
    [UIView animateWithDuration:.3 animations:^{
        stressTrackLine.alpha = 0.0;
        stressTrackLine.transform = CGAffineTransformMakeScale(.1, .1);
    } completion:^(BOOL finished) {
        [stressTracks.middleLines removeObject:stressTrackLine];
        [stressTracks showEmptyMessage];
        [self relayout];
    }];
}

- (void) stressTrackBoxStateChanged:(FTCStressTrackLine *)stressTrackLine withIndex:(int)boxIndex
{
    StressBoxes newState = [stressTrackLine.stressTrack toggleStateOfStressBox:boxIndex];
    [self.doc changeStressTrackBoxes:stressTrackLine.stressTrack toStressBoxes:[NSNumber numberWithInt:newState]];
    [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) { NSLog(@"Document saved. (stress updated)"); }];
    
}

- (BOOL) validateStressTrackEntry:(FTCStressTrackEntryViewController *)stressTrackEntryController withMessage:(NSString *__autoreleasing *)validationMessage
{
    FTCStressTrack *stressTrack = stressTrackEntryController.stressTrack;
    
    NSString *proposedName = stressTrackEntryController.nameTextField.text;
    
    NSArray *existingNames = [self.doc.stressTracks valueForKeyPath:@"name"];
    if ([existingNames containsObject:proposedName])
    {
        //it's ok if it's an existing skill and they're not changing the name
        if (stressTrack == nil || ![stressTrack.name isEqualToString:proposedName])
        {
            NSString *error = @"You already have a stress track with that name.";
            *validationMessage = error;
            return NO;
        }
    }
    return YES;
}
- (void) stressTrackWasSaved:(FTCStressTrackEntryViewController *)stressTrackEntryController
{
    [stressTracks removeEmptyMessage];
    if (stressTrackEntryController.stressTrack == nil) //New
    {
        [self.doc addStressTrackWithName:stressTrackEntryController.nameTextField.text andSlots:[NSNumber numberWithInt:stressTrackEntryController.selectedNumberOfBoxes]];
    }
    else //Edit
    {
        [self.doc updateStressTrack:stressTrackEntryController.stressTrack withName:stressTrackEntryController.nameTextField.text andSlots:[NSNumber numberWithInt:stressTrackEntryController.selectedNumberOfBoxes]];
    }
    
    [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) {
        NSLog(@"Document saved. Redisplaying stuff...");
        dispatch_async(dispatch_get_main_queue(), ^{
            [self displayOrderedStressTracks];
            [self relayout];
            [CRToastManager showNotificationWithOptions:[FTCNotificationHelper successNotification:@"Stress track saved!"] completionBlock:nil];
            [self.scroller scrollToView:stressTracks withMargin:0];
        });
    }];
}

- (void) setupStressSection
{
    FAKIcon *psIcon = [FAKIonIcons ios7PlusOutlineIconWithSize:24];
    [psIcon addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
    UIImage *psIconImage = [psIcon imageWithSize:(CGSize){30, 30}];
    stressTracks = [FTCDetailSectionBox boxWithSize:(CGSize){[FTCLayoutHelper getRowWidth:NO], 0} andTitle:@"Stress" andActionImage:psIconImage andActionCallback:^(id sender){
        [self addStressTrack];
    }];
    
    [stressTracks showEmptyMessage];
    
    [_masterBox.boxes addObject:stressTracks];
    
    [self displayOrderedStressTracks];
}

- (void) displayOrderedStressTracks
{
    [stressTracks.middleLines removeAllObjects];
    
    NSArray *sortedStressTracks = self.doc.stressTracksSortedByName;
    
    if (sortedStressTracks.count == 0)
    {
        [stressTracks showEmptyMessage];
    }
    else
    {
        for (FTCStressTrack *stressTrack in sortedStressTracks) {
            FTCStressTrackLine *stressTrackLine = [FTCStressTrackLine stressTrackLineWithStressTrack:stressTrack delegate:self];
            [stressTracks.middleLines addObject:stressTrackLine];
        }
    }
}

- (void) resizeStressSection:(UIInterfaceOrientation)orientation
{
    double inputWidth = [FTCLayoutHelper getRowWidthForOrientation:orientation doubleWide:NO];
    
    BOOL redoEmptyMessage = self.doc.stressTracks.count == 0;
    
    if (!redoEmptyMessage) {
        for (FTCStressTrackLine *line in stressTracks.middleLines) {
            line.lineWidth = inputWidth - 8;
        }
    }
    else {
        [stressTracks.middleLines removeAllObjects];
    }
    [stressTracks setSize:(CGSize){inputWidth, stressTracks.size.height}];
    if (redoEmptyMessage)
        [stressTracks showEmptyMessage];
    
    //    double width = [FTCLayoutHelper getRowWidthForOrientation:orientation doubleWide:NO];
    //
    //    [stressTracks setSize:(CGSize){width, stressTracks.size.height}];
}

#pragma mark Consequences

- (void) consequenceLineWasRemoved:(FTCConsequenceLine *)consequenceLine
{
    [self.doc removeConsequence:consequenceLine.consequence];
    [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) { NSLog(@"Document saved."); }];
    
    [UIView animateWithDuration:.3 animations:^{
        consequenceLine.alpha = 0.0;
        consequenceLine.transform = CGAffineTransformMakeScale(.1, .1);
    } completion:^(BOOL finished) {
        [consequences.middleLines removeObject:consequenceLine];
        [consequences showEmptyMessage];
        [self relayout];
    }];
}

- (void) consequenceLineWasChanged:(FTCConsequenceLine *)consequenceLine withText:(NSString *)text
{
    [self.doc updateConsequence:consequenceLine.consequence withText:text];
    [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) { NSLog(@"Document saved."); }];
}

- (void) addConsequence
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"What type of consequence would you like to add?" delegate:self cancelButtonTitle:AS_CANCEL destructiveButtonTitle:nil otherButtonTitles:AS_MILD, AS_MODERATE, AS_SEVERE, nil];
    actionSheet.tag = TAG_CONSEQUENCESHEET;
    [actionSheet showInView:self.view];
    
}

- (void) displayOrderedConsequences
{
    [consequences.middleLines removeAllObjects];
    
    NSArray *sortedConsequences = self.doc.consequencesSortedByType;
    
    if (sortedConsequences.count == 0)
    {
        [consequences showEmptyMessage];
    }
    else
    {
        for (FTCConsequence *consequence in sortedConsequences) {
            FTCConsequenceLine *consequenceLine = [FTCConsequenceLine consequenceLineWithConsequence:consequence delegate:self];
            [consequences.middleLines addObject:consequenceLine];
        }
    }
}

- (void) setupConsequencesSection
{
    FAKIcon *psIcon = [FAKIonIcons ios7PlusOutlineIconWithSize:24];
    [psIcon addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
    UIImage *psIconImage = [psIcon imageWithSize:(CGSize){30, 30}];
    consequences = [FTCDetailSectionBox boxWithSize:(CGSize){[FTCLayoutHelper getRowWidth:NO], 0} andTitle:@"Consequences" andActionImage:psIconImage andActionCallback:^(id sender){
        [self addConsequence];
    }];
    
    [consequences showEmptyMessage];
    
    [_masterBox.boxes addObject:consequences];
    
    [self displayOrderedConsequences];
}

- (void) resizeConsequencesSection:(UIInterfaceOrientation)orientation
{
    double inputWidth = [FTCLayoutHelper getRowWidthForOrientation:orientation doubleWide:NO];
    
    BOOL redoEmptyMessage = self.doc.consequences.count == 0;
    
    if (!redoEmptyMessage) {
        for (FTCConsequenceLine *line in consequences.middleLines) {
            line.lineWidth = inputWidth - 8;
        }
    }
    else {
        [consequences.middleLines removeAllObjects];
    }
    [consequences setSize:(CGSize){inputWidth, consequences.size.height}];
    if (redoEmptyMessage)
        [consequences showEmptyMessage];
    
    //    double width = [FTCLayoutHelper getRowWidthForOrientation:orientation doubleWide:NO];
    //
    //    [consequences setSize:(CGSize){width, consequences.size.height}];
}

#pragma mark General UI Config

- (void)configureView
{
    [_masterBox.boxes removeAllObjects];
    
    CharacterType type = [self.doc characterType];
    
    [self setupHeader];
    [self setupFatePointsAndRefreshSection];
    [self setupAspectsSection];
    if (type == FateCoreCharacter) {
        //FC characters have skills, stunts, extras
        [self setupSkillsSection];
        [self setupStuntsSection];
        [self setupExtrasSection];
    } else {
        //FAE characters have approaches, stunts
        [self setupApproachesSection];
        [self setupStuntsSection];
    }
    [self setupStressSection];
    [self setupConsequencesSection];
    
    [self relayout];
}

- (void) relayout
{
    [self.scroller layoutWithSpeed:.3 completion:nil];
}

#pragma mark UIScrollViewDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (scrollView == self.scroller)
    {
        descriptionView.scrollEnabled = NO;
    }
    else if (scrollView == descriptionView)
    {
        self.scroller.scrollEnabled = NO;
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    descriptionView.scrollEnabled = YES;
    self.scroller.scrollEnabled = YES;
}

#pragma mark - RMPickerViewController Delegates
- (void)pickerViewController:(RMPickerViewController *)vc didSelectRows:(NSArray  *)selectedRows {
    if (standardSkillsPicker == vc)
    {
        NSNumber *skillDefIndex = (NSNumber *)[selectedRows objectAtIndex:0];
        NSNumber *skillValueIndex = (NSNumber *)[selectedRows objectAtIndex:1];
        FTCSkillDefinition *skillDef = [unusedSkillDefinitions objectAtIndex:[skillDefIndex integerValue]];
        NSString *valueString = [skillValues objectAtIndex:[skillValueIndex integerValue]];
        int value = [skillValues indexOfObject:valueString] - 2;
        
        [self.doc addSkillWithName:skillDef.name andValue:[NSNumber numberWithInt:value] isCustom:NO andActions:skillDef.actions];
        
        [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) {
            NSLog(@"Document saved. Redisplaying stuff...");
            dispatch_async(dispatch_get_main_queue(), ^{
                [self displayOrderedSkillLines];
                [self relayout];
                [self.scroller scrollToView:skills withMargin:0];
            });
        }];
        
    }
}

- (void)pickerViewControllerDidCancel:(RMPickerViewController *)vc {
    //Do something else
}


#pragma mark - UIPickerView DataSource
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (standardSkillsPicker.picker == pickerView)
        return 2;
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (standardSkillsPicker.picker == pickerView)
    {
        if (component == 0) //Skill names
            return unusedSkillDefinitions.count;
        else //skill values
            return skillValues.count;
    }
    return 0;
}

#pragma mark - UIPickerView Delegate
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    if (standardSkillsPicker.picker == pickerView)
    {
        return 30.0;
    }
    return 30.0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (standardSkillsPicker.picker == pickerView)
    {
        if (component == 0) //Skill names
            return ((FTCSkillDefinition *)[unusedSkillDefinitions objectAtIndex:row]).name;
        else //Skill values
            return [skillValues objectAtIndex:row];
    }
    return @"";
}

//If the user chooses from the pickerview, it calls this function;
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
}

-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    float width = pickerView.width;
    if (standardSkillsPicker.picker == pickerView)
    {
        if (component == 0)
        {
            return (width * 40 ) / 100  ;
        }
        else
        {
            return (width * 60 ) / 100  ;
        }
    }
    return width;
}

- (UIImage *) imageWithView:(UIView *)view
{
    //BOOL isOpaque = view.opaque;
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

#pragma mark UIActionSheet methods

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *tappedButtonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    if (actionSheet.tag == TAG_CHARACTERMANAGEACTIONSHEET)
    {
        CharacterType type = [self.doc characterType];
        if ([tappedButtonTitle isEqualToString:AS_CONVERTTOACCELERATED]) {
            if (type == FateAcceleratedCharacter)
                return;
            
            [self.doc setCharacterType:[NSNumber numberWithInt:FateAcceleratedCharacter]];
            NSArray *defaultApproaches = [NSArray arrayWithObjects:@"Careful", @"Clever", @"Flashy", @"Forceful", @"Quick", @"Sneaky", nil];
            NSArray *existingApproachNames = [self.doc.approaches valueForKey:@"name"];
            for (NSString *approachName in defaultApproaches) {
                if (![existingApproachNames containsObject:approachName]) {
                    [self.doc addApproachWithName:approachName andValue:[NSNumber numberWithInt:0] isCustom:NO];
                }
            }
            
            //Kill existing stress tracks and reset to default
            for (FTCStressTrack *stressTrack in [[self.doc stressTracks] copy]) {
                [self.doc removeStressTrack:stressTrack];
            }
            [self.doc addStressTrackWithName:@"Stress" andSlots:[NSNumber numberWithInt:3]];
            
            [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self configureView];
                    [CRToastManager showNotificationWithOptions:[FTCNotificationHelper successNotification:@"Conversion successful!" andSubtitle:@"Your character is now a Fate Accelerated Character."] completionBlock:nil];
                });
            }];
            
        }
        else if ([tappedButtonTitle isEqualToString:AS_CONVERTTOCORE]) {
            if (type == FateCoreCharacter)
                return;
            
            [self.doc setCharacterType:[NSNumber numberWithInt:FateCoreCharacter]];
            //Kill existing stress tracks and reset to default
            for (FTCStressTrack *stressTrack in [[self.doc stressTracks] copy]) {
                [self.doc removeStressTrack:stressTrack];
            }
            [self.doc addStressTrackWithName:@"Physical" andSlots:[NSNumber numberWithInt:2]];
            [self.doc addStressTrackWithName:@"Mental" andSlots:[NSNumber numberWithInt:2]];
            [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self configureView];
                    [CRToastManager showNotificationWithOptions:[FTCNotificationHelper successNotification:@"Conversion successful!" andSubtitle:@"Your character is now a Fate Core Character."] completionBlock:nil];
                });
            }];
        }
        else if ([tappedButtonTitle isEqualToString:AS_GENPDF])
        {
            NSString *html = [FTCDocumentHTMLExporter convertDocumentToHTML:self.doc];
            NSString *file = [NSString stringWithFormat:@"%@.pdf", self.doc.name];
            NSArray* documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
            
            NSString* documentDirectory = [documentDirectories objectAtIndex:0];
            NSString* documentDirectoryFilename = [documentDirectory stringByAppendingPathComponent:file];

            
            [pdfKit saveHtmlAsPdf:html toFile:documentDirectoryFilename];
        }
        else if ([tappedButtonTitle isEqualToString:AS_DELETE])
        {
            //Delete the character
            [[FTCDocumentManager sharedManager] deleteSelectedCharacter:^(BOOL success, NSString *validationMessage) {
                if (!success) {
                    [CRToastManager showNotificationWithOptions:[FTCNotificationHelper warningNotification:@"Couldn't delete character" andSubtitle:validationMessage] completionBlock:nil];
                }
                else {
                    [self.delegate detailViewControllerDidDelete:self];
                    if ([FTCLayoutHelper deviceIsIPhone]) {
                        [self performSegueWithIdentifier:@"deleteCharacter" sender:self];
                    }
                    else
                    {
                        [self.navigationController popToRootViewControllerAnimated:YES];
                    }
                }
            }];
        }
    }
    else if (actionSheet.tag == TAG_SKILLACTIONSHEET)
    {
        if ([tappedButtonTitle isEqualToString:AS_STANDARD])
        {
            unusedSkillDefinitions = [standardSkillsHelper undesignatedStandardSkillsForCharacter:self.doc];
            standardSkillsPicker = [RMPickerViewController pickerController];
            standardSkillsPicker.delegate = self;
            
            [standardSkillsPicker showFromViewController:self];
        }
        else if ([tappedButtonTitle isEqualToString:AS_CUSTOM])
        {
            [self performSegueWithIdentifier:@"openSkillEntry" sender:self];
        }
    }
    else if (actionSheet.tag == TAG_CONSEQUENCESHEET)
    {
        if ([tappedButtonTitle isEqualToString:AS_CANCEL])
            return;
        
        if ([tappedButtonTitle isEqualToString:AS_MILD])
        {
            [self.doc addConsequenceWithType:ConsequenceMild isOptional:YES];
        }
        else if ([tappedButtonTitle isEqualToString:AS_MODERATE])
        {
            [self.doc addConsequenceWithType:ConsequenceModerate isOptional:YES];
        }
        else if ([tappedButtonTitle isEqualToString:AS_SEVERE])
        {
            [self.doc addConsequenceWithType:ConsequenceSevere isOptional:YES];
        }
        [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self displayOrderedConsequences];
                [self relayout];
                [self.scroller scrollToView:consequences withMargin:0];
            });
        }];
    }
}

- (IBAction)actionsButtonWasTapped:(id)sender {
    NSString *conversionOption = [self.doc characterType] == FateCoreCharacter ? AS_CONVERTTOACCELERATED : AS_CONVERTTOCORE;
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:AS_CANCEL destructiveButtonTitle:AS_DELETE otherButtonTitles:conversionOption, AS_GENPDF, nil];
    actionSheet.tag = TAG_CHARACTERMANAGEACTIONSHEET;
    //actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [actionSheet showFromBarButtonItem:sender animated:YES];
}

#pragma mark BNHtmlPDFKit delegate

- (void)htmlPdfKit:(BNHtmlPdfKit *)htmlPdfKit didSavePdfData:(NSData *)data
{
    //never used
}

- (void)htmlPdfKit:(BNHtmlPdfKit *)htmlPdfKit didSavePdfFile:(NSString *)file
{
    // Retrieves the document directories from the iOS device
//    NSArray* documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
//    
//    NSString* documentDirectory = [documentDirectories objectAtIndex:0];
//    NSString* documentDirectoryFilename = [documentDirectory stringByAppendingPathComponent:file];
    NSURL *url = [NSURL fileURLWithPath:file];
    [CRToastManager showNotificationWithOptions:[FTCNotificationHelper successNotification:@"PDF Generated!" andSubtitle:@"Your character sheet will open momentarily." andDuration:2.0] completionBlock:nil];
    // Initialize Document Interaction Controller
    documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:url];
    
    // Configure Document Interaction Controller
    //[documentInteractionController setDelegate:self];
    
    // Present Open In Menu
    UIBarButtonItem *rightItem = self.navigationItem.rightBarButtonItem;
    BOOL canOpen = [documentInteractionController presentOpenInMenuFromBarButtonItem:rightItem animated:YES];
    
    if (!canOpen) {
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Can't open PDF" andMessage:@"Sorry, there are no apps on your device capable of opening a PDF."];
        [alertView addButtonWithTitle:@"OK"
                                 type:SIAlertViewButtonTypeDefault
                              handler:nil];
        alertView.transitionStyle = SIAlertViewTransitionStyleFade;
        [alertView show];
    }
}

- (void)htmlPdfKit:(BNHtmlPdfKit *)htmlPdfKit didFailWithError:(NSError *)error
{
    [CRToastManager showNotificationWithOptions:[FTCNotificationHelper warningNotification:@"Oops!" andSubtitle:@"Couldn't generate a PDF, sorry."] completionBlock:nil];
    //todo log the error
    
}

#pragma mark Standard view lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.doc = [FTCDocumentManager sharedManager].currentDocument;
    pdfKit = [[BNHtmlPdfKit alloc] initWithPageSize:BNPageSizeLetter isLandscape:NO];
    pdfKit.delegate = self;
    
    skillPadding = 0;
    currentKeyboardSize = CGSizeZero;
    
    standardSkillsHelper = [[FTCStandardSkillsHelper alloc] init];
    translator = [FTCSkillValueTranslator new];
    skillValues = [translator allSkillNamesAndValues];
    
    _masterBox = [FTCGridBox boxWithSize:(CGSize){[FTCLayoutHelper getGridWidth], 0}];
    _masterBox.contentLayoutMode = MGLayoutGridStyle;
    _masterBox.leftMargin = 8;
    _masterBox.topMargin = 8;
    
    self.scroller.contentLayoutMode = MGLayoutGridStyle;
    //self.scroller.keyboardMargin = 10;
    self.scroller.keepFirstResponderAboveKeyboard = NO;
    self.scroller.delegate = self;
    self.scroller.canCancelContentTouches = NO;
    
    [self.scroller.boxes addObject:_masterBox];
    [self configureView];
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if ([FTCLayoutHelper deviceIsIPad])
    {
        CharacterType type = [self.doc characterType];
        
        [_masterBox setSize:(CGSize){[FTCLayoutHelper getGridWidthForOrientation:toInterfaceOrientation], 0}];
        _masterBox.itemPadding = [FTCLayoutHelper getGridItemPaddingForOrientation:toInterfaceOrientation];
        [self resizeHeaderSection:toInterfaceOrientation];
        [self resizeFatePointsAndRefreshSection:toInterfaceOrientation];
        [self resizeAspectsSection:toInterfaceOrientation];
        
        if (type == FateCoreCharacter) {
            [self resizeSkillsSection:toInterfaceOrientation];
            [self resizeStuntsSection:toInterfaceOrientation];
            [self resizeExtrasSection:toInterfaceOrientation];
        } else {
            [self resizeApproachesSection:toInterfaceOrientation];
            [self resizeStuntsSection:toInterfaceOrientation];
        }
        [self resizeStressSection:toInterfaceOrientation];
        [self resizeConsequencesSection:toInterfaceOrientation];
        [self.scroller layoutWithSpeed:duration completion:nil];
    }
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if ([FTCLayoutHelper deviceIsIPad])
    {
        [self.scroller layout];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [NSNotificationCenter.defaultCenter addObserver:self
                                           selector:@selector(keyboardWillAppear:) name:UIKeyboardWillShowNotification
                                             object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(documentStateChanged:)
                                                 name:UIDocumentStateChangedNotification
                                               object:self.doc];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)documentStateChanged:(NSNotification *)notification {
    UIDocumentState state = self.doc.documentState;
    
    if (state & UIDocumentStateInConflict)
    {
        NSURL *fileURL = self.doc.fileURL;
        NSMutableArray * fileVersions = [NSMutableArray array];
        
        NSFileVersion * currentVersion = [NSFileVersion currentVersionOfItemAtURL:fileURL];
        [fileVersions addObject:currentVersion];
        
        NSArray * otherVersions = [NSFileVersion otherVersionsOfItemAtURL:fileURL];
        [fileVersions addObjectsFromArray:otherVersions];
        
        __block NSFileVersion *versionToKeep = nil;
        __block NSDate *lastUpdatedDate = nil;
        //Open up all the document versions
        for (NSFileVersion *fileVersion in fileVersions) {
            FTCDocument *doc = [[FTCDocument alloc] initWithFileURL:fileVersion.URL];
            [doc openWithCompletionHandler:^(BOOL success) {
                NSString *dateString = [NSDateFormatter localizedStringFromDate:doc.fileModificationDate dateStyle:NSDateFormatterFullStyle timeStyle:NSDateFormatterFullStyle];
                if (versionToKeep == nil) {
                    NSLog(@"No version to keep, keeping date %@", dateString);
                    versionToKeep = fileVersion;
                    lastUpdatedDate = doc.fileModificationDate;
                }
                else
                {
                    NSLog(@"Comparing to %@", dateString);
                    if ([doc.fileModificationDate compare:lastUpdatedDate] == NSOrderedDescending)
                    {
                        NSLog(@"Date was later, replacing current version.");
                        versionToKeep = fileVersion;
                        lastUpdatedDate = doc.fileModificationDate;
                    }
                }
                [doc closeWithCompletionHandler:nil];
            }];
        }
        
        if (![versionToKeep isEqual:[NSFileVersion currentVersionOfItemAtURL:fileURL]]) {
            NSLog(@"Replacing file!");
            [versionToKeep replaceItemAtURL:fileURL options:0 error:nil];
        }
        [NSFileVersion removeOtherVersionsOfItemAtURL:fileURL error:nil];
        NSArray* conflictVersions = [NSFileVersion unresolvedConflictVersionsOfItemAtURL:fileURL];
        for (NSFileVersion* fileVersion in conflictVersions) {
            fileVersion.resolved = YES;
        }
    }
    
    [self configureView];
    [CRToastManager showNotificationWithOptions:[FTCNotificationHelper successNotification:@"Character updated!" andSubtitle:@"Your changes have been synced."] completionBlock:nil];
}

- (void)dealloc {
    [NSNotificationCenter.defaultCenter removeObserver:self];
}

- (void)keyboardWillAppear:(NSNotification *)note
{
    CGRect rawKeyboardRect = [[[note userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    currentKeyboardSize = [self.view.window convertRect:rawKeyboardRect toView:self.view.window.rootViewController.view].size;
    defaultInsets = self.scroller.contentInset;
//    NSLog(@"INSETS: %f %f", self.scroller.contentInset.top, self.scroller.scrollIndicatorInsets.top);
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(defaultInsets.top, defaultInsets.left, currentKeyboardSize.height, defaultInsets.right);
    
    self.scroller.contentInset = contentInsets;
    self.scroller.scrollIndicatorInsets = contentInsets;
    [self.scroller layout];
}

- (void)keyboardWasShown:(NSNotification *)notification {
    // Get the size of the keyboard.
//    CGRect rawKeyboardRect = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
//    currentKeyboardSize = [self.view.window convertRect:rawKeyboardRect toView:self.view.window.rootViewController.view].size;
//    defaultInsets = self.scroller.contentInset;
//    NSLog(@"INSETS: %f %f", self.scroller.contentInset.top, self.scroller.scrollIndicatorInsets.top);
//    
//    UIEdgeInsets contentInsets = UIEdgeInsetsMake(defaultInsets.top, defaultInsets.left, currentKeyboardSize.height, defaultInsets.right);
//    
//    self.scroller.contentInset = contentInsets;
//    self.scroller.scrollIndicatorInsets = contentInsets;

}

- (void)keyboardWillHide:(NSNotification *)notification {
    currentKeyboardSize = CGSizeZero;
    self.scroller.contentInset = self.scroller.scrollIndicatorInsets = defaultInsets;
//    NSLog(@"INSETS: %f %f", self.scroller.contentInset.top, self.scroller.scrollIndicatorInsets.top);
    [self.scroller scrollRectToVisible:CGRectZero animated:YES];
}

- (IBAction)iPadDoneButtonTapped:(id)sender
{
    [self doneWithCharacter];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"doneWithCharacter"]) {
        [self doneWithCharacter];
    }
    else if ([[segue identifier] isEqualToString:@"deleteCharacter"]) {
        //[self deleteCharacter];
    }
    else if ([[segue identifier] isEqualToString:@"openApproachEntry"]) {
        FTCApproachEntryViewController *approachEntryViewController = nil;
        
        UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
        
        approachEntryViewController = (FTCApproachEntryViewController *)navController.visibleViewController;
        
        approachEntryViewController.navigationItem.title = editApproach == nil ? @"Add Approach" : @"Edit Approach";
        approachEntryViewController.delegate = self;
        approachEntryViewController.approach = editApproach;
    }
    else if ([[segue identifier] isEqualToString:@"openSkillEntry"]) {
        FTCSkillEntryViewController *skillEntryController = nil;
        
        UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
        
        skillEntryController = (FTCSkillEntryViewController *)navController.visibleViewController;
        
        skillEntryController.navigationItem.title = editSkill == nil ? @"Add Skill" : @"Edit Skill";
        skillEntryController.delegate = self;
        skillEntryController.skill = editSkill;
    }
    else if ([[segue identifier] isEqualToString:@"openStressTrackEntry"]) {
        UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
        
        FTCStressTrackEntryViewController *stressTrackEntryController = (FTCStressTrackEntryViewController *)navController.visibleViewController;
        
        stressTrackEntryController.navigationItem.title = editStressTrack == nil ? @"Add Stress Track" : @"Edit Stress Track";
        stressTrackEntryController.delegate = self;
        stressTrackEntryController.stressTrack = editStressTrack;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
//}

#pragma mark UITextFieldDelegate
- (void)textChanged:(UITextField *)textField {
    if (textField.tag == TAG_CHARACTERNAME)
    {
        NSString *name = [NSString stringWithString:textField.text];
        [[FTCDocumentManager sharedManager] renameCurrentCharacterTo:name withHandler:^(BOOL success, NSString *validationMessage) {
            if (success) {
                [self configureView];
                [CRToastManager showNotificationWithOptions:[FTCNotificationHelper successNotification:[NSString stringWithFormat:@"Character renamed to %@", name]] completionBlock:nil];
            }
            else
            {
                if (validationMessage != nil)
                {
                    //if message is nil that means operation was aborted rather
                    //than failed
                    textField.text = self.doc.name;
                    [CRToastManager showNotificationWithOptions:[FTCNotificationHelper warningNotification:@"Couldn't rename character" andSubtitle:validationMessage] completionBlock:nil];
                }
            }
        }];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
    [self textChanged:textField];
	return YES;
}

#pragma mark UITextViewDelegate
- (void) textViewDidEndEditing:(UITextView *)textView
{
    //    [self.delegate aspectLineChanged:self withText:textView.text];
    //    [self.aspectField layoutSubviews];
    if (textView.tag == TAG_DESCRIPTIONVIEW)
    {
        [self.doc setCharacterDescription:textView.text];
        [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) {
        }];
    }
}


- (BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (textView.tag == TAG_DESCRIPTIONVIEW)
    {
        if ([text isEqualToString:@"\n"]) //done
        {
            [textView resignFirstResponder];
            return NO;
        }
        return YES;
        if ((textView.text.length + text.length - range.length) <= 90)
            return YES;
        return NO;
    }
    return YES;
}

#pragma mark Callbacks

- (void)imageTapped {
    if (!_picker) {
        _picker = [[UIImagePickerController alloc] init];
        _picker.delegate = self;
        _picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        _picker.allowsEditing = NO;
        _picker.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        
    }
    if ([FTCLayoutHelper deviceIsIPad])
    {
        imagePickerPopover = [[UIPopoverController alloc] initWithContentViewController:_picker];
        [imagePickerPopover presentPopoverFromRect:photoBox.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else {
        [self presentViewController:_picker animated:YES completion:nil];
        //[self presentModalViewController:_picker animated:YES];
    }
    
}

- (void)doneWithCharacter {
    [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) {
        [self.doc closeWithCompletionHandler:^(BOOL success) {
            if (!success) {
                NSLog(@"Failed to close %@", self.doc.fileURL);
                // Continue anyway...
            }
            
            [self.delegate detailViewControllerDidClose:self];
        }];
    }];
}

#pragma mark UIImagePickerControllerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *image = (UIImage *) [info objectForKey:UIImagePickerControllerOriginalImage];
    
    CGSize mainSize = CGSizeMake(300, 300);
    UIImage *sImage = [image imageByBestFitForSize:mainSize]; //[image scaleToFitSize:mainSize];
    UIImage *tImage = [image imageByBestFitForSize:THUMBNAIL_SIZE];
    
    self.doc.photo = sImage;
    //self.photoView.image = sImage;
    [photoBox setPhoto:tImage];
    
    if ([FTCLayoutHelper deviceIsIPad]) {
        [imagePickerPopover dismissPopoverAnimated:YES];
    }
    else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    
}

@end
