//
//  FTCOperationResult.m
//  Fate Companion
//
//  Created by Joshua Barron on 1/26/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCOperationResult.h"

@implementation FTCOperationResult
@synthesize message = _message;
@synthesize success = _success;

- (id) initWithSuccess
{
    if (self = [super init]) {
        self.message = nil;
        self.success = YES;
    }
    
    return self;
}

- (id) initWithFailure:(NSString *)message
{
    if (self = [super init]) {
        self.message = message;
        self.success = NO;
    }
    return self;
}
@end
