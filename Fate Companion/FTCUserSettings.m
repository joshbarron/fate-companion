//
//  FTCUserSettings.m
//  Fate Companion
//
//  Created by Joshua Barron on 3/4/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCUserSettings.h"

#define kShowDisabledStressBoxes @"ShowDisabledStressBoxes"
#define kSkillSortOrder @"SkillSortOrder"
#define kSavedDiceRolls @"SavedDiceRolls"
#define kSavedNotes @"SavedNotes"
#define kShowTutorial @"ShowTutorial"

@implementation FTCUserSettings

+ (BOOL) showDisabledStressBoxes
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:kShowDisabledStressBoxes];
}

+ (void) setShowDisabledStressBoxes:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:kShowDisabledStressBoxes];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (SkillSortKey) skillSortOrder
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:kSkillSortOrder];
}

+ (void) setSkillSortOrder:(SkillSortKey)sortKey
{
    [[NSUserDefaults standardUserDefaults] setInteger:sortKey forKey:kSkillSortOrder];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSArray *)getSavedDiceRolls
{
    NSData *encodedObject = [[NSUserDefaults standardUserDefaults] objectForKey:kSavedDiceRolls];
    if (encodedObject == nil)
        return [[NSArray alloc] init];
    
    NSArray *savedDiceRolls = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return savedDiceRolls;
}

+ (void)setSavedDiceRolls:(NSArray *)diceRolls
{
    NSData* objData = [NSKeyedArchiver archivedDataWithRootObject:diceRolls];
    [[NSUserDefaults standardUserDefaults] setObject:objData forKey:kSavedDiceRolls];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSArray *) getSavedNotes
{
    NSData *encodedObject = [[NSUserDefaults standardUserDefaults] objectForKey:kSavedNotes];
    NSArray *savedNotes = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return savedNotes;
}

+ (void) setSavedNotes:(NSArray *)notes
{
    NSData *objData = [NSKeyedArchiver archivedDataWithRootObject:notes];
    [[NSUserDefaults standardUserDefaults] setObject:objData forKey:kSavedNotes];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BOOL) showTutorial
{
    id _showTutorial = [[NSUserDefaults standardUserDefaults] objectForKey:kShowTutorial];
    
    if (_showTutorial == nil) { //first run
        [FTCUserSettings setShowTutorial:YES];
    }
    
    return [[NSUserDefaults standardUserDefaults] boolForKey:kShowTutorial];
}

+ (void) setShowTutorial:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:kShowTutorial];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
