//
//  FTCEntry.h
//  Fate Companion
//
//  Created by Joshua Barron on 1/25/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FTCCharacterMetadata;

@interface FTCEntry : NSObject

@property (strong) NSURL * fileURL;
@property (strong) FTCCharacterMetadata * metadata;
@property (assign) UIDocumentState state;
@property (strong) NSFileVersion * version;

- (id)initWithFileURL:(NSURL *)fileURL metadata:(FTCCharacterMetadata *)metadata state:(UIDocumentState)state version:(NSFileVersion *)version;
- (NSString *) description;

@end
