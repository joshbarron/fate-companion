//
//  FTCNotesViewController.h
//  Fate Companion
//
//  Created by Joshua Barron on 3/15/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGScrollView.h"
#import "FTCNoteLine.h"

@interface FTCNotesViewController : UIViewController<UIScrollViewDelegate, UISplitViewControllerDelegate, FTCNoteLineDelegate>

@property (weak, nonatomic) IBOutlet MGScrollView *scroller;

@end
