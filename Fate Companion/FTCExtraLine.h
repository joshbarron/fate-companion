//
//  FTCExtraLine.h
//  Fate Companion
//
//  Created by Joshua Barron on 2/22/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "MGLine.h"
#import "FTCExtra.h"
#import "JVFloatLabeledTextField.h"

@class FTCExtraLine;

@protocol FTCExtraLineDelegate

- (void)extraLineWasRemoved:(FTCExtraLine *)extraLine;
- (void)extraLineNameChanged:(FTCExtraLine *)extraLine withText:(NSString *)text;

@end

@interface FTCExtraLine : MGLine<UITextFieldDelegate>

+ (FTCExtraLine *)extraLineWithExtra:(FTCExtra *)extra delegate:(id<FTCExtraLineDelegate>)delegate;

@property (weak) id<FTCExtraLineDelegate> delegate;
@property (weak) FTCExtra *extra;

@property JVFloatLabeledTextField *nameField;

@property double lineWidth;

@end
