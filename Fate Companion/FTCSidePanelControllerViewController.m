//
//  FTCSidePanelControllerViewController.m
//  Fate Companion
//
//  Created by Joshua Barron on 1/28/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCSidePanelControllerViewController.h"

@interface FTCSidePanelControllerViewController ()

@end

@implementation FTCSidePanelControllerViewController

- (FTCMainNavigationViewController *)getMainNav
{
    UINavigationController *leftNav = (UINavigationController *)self.leftPanel;
    return (FTCMainNavigationViewController *)leftNav.visibleViewController;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)awakeFromNib
{
    [self setLeftPanel:[self.storyboard instantiateViewControllerWithIdentifier:@"leftPanel"]];
    [self setCenterPanel:[self.storyboard instantiateViewControllerWithIdentifier:@"characterPanel"]];
    [self setRightPanel:nil];
    //[self setAllowLeftSwipe:NO];
//    [self setRightPanel:[self.storyboard instantiateViewControllerWithIdentifier:@"rightViewController"]];
}

@end
