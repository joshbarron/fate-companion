//
//  FTCNewsViewController.h
//  Fate Companion
//
//  Created by Joshua Barron on 4/5/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGScrollView.h"

@interface FTCNewsViewController : UIViewController<UISplitViewControllerDelegate>

@property (weak, nonatomic) IBOutlet MGScrollView *scroller;

@end
