//
//  FTCConsequenceLine.h
//  Fate Companion
//
//  Created by Joshua Barron on 3/6/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "MGLine.h"
#import "FTCConsequence.h"
#import "JVFloatLabeledTextField.h"

@class FTCConsequenceLine;

@protocol FTCConsequenceLineDelegate

- (void)consequenceLineWasRemoved:(FTCConsequenceLine *)consequenceLine;
- (void)consequenceLineWasChanged:(FTCConsequenceLine *)consequenceLine withText:(NSString *)text;

@end

@interface FTCConsequenceLine : MGLine <UITextFieldDelegate>

+ (FTCConsequenceLine *)consequenceLineWithConsequence:(FTCConsequence *)consequence delegate:(id<FTCConsequenceLineDelegate>)delegate;

@property (weak) id<FTCConsequenceLineDelegate> delegate;
@property (weak) FTCConsequence *consequence;

@property JVFloatLabeledTextField *textField;

@property double lineWidth;

@end
