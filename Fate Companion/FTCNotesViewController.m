//
//  FTCNotesViewController.m
//  Fate Companion
//
//  Created by Joshua Barron on 3/15/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCNotesViewController.h"
#import "MGLine.h"
#import "MGBox.h"
#import "FTCUserSettings.h"
#import "FTCNote.h"
#import "FontAwesomeKit/FAKIonIcons.h"
#import "FTCDetailSectionBox.h"
#import "FTCGridBox.h"
#import "FTCLayoutHelper.h"
#import "JASidePanelController.h"

@interface FTCNotesViewController ()
{
    FTCGridBox *_masterBox;
    MGLine *_dummyLine;
    FTCDetailSectionBox *noteBox;
    CGSize currentKeyboardSize;
}
@end

@implementation FTCNotesViewController
@synthesize scroller = _scroller;

- (FTCNote *)getNewNote {
    return [[FTCNote alloc] initWithText:@""];
}

- (IBAction)addButtonTapped:(id)sender {
    FTCNoteLine *noteLine = [self addNoteLine:[self getNewNote]];
    [self.scroller layoutWithSpeed:.3 completion:nil];
    [self.scroller scrollToView:noteLine withMargin:0];
    [self saveNotes];
}

- (FTCNoteLine *)addNoteLine:(FTCNote *)note
{
    FTCNoteLine *noteLine = [FTCNoteLine noteLineWithNote:note delegate:self];
    
    [noteBox.middleLines addObject:noteLine];
    return noteLine;
}


- (void) noteLineHeightChanged:(FTCNoteLine *)noteLine
{
    [self.scroller layout];
    // if there is a selection cursor…
    UITextView *textView = noteLine.noteField;
    
    if(textView.selectedRange.location != NSNotFound) {
        UIView *viewToScrollTo;
        double margin = 0;
        if (noteLine == [noteBox.middleLines lastObject]) {
            viewToScrollTo = _dummyLine;
        }
        else
        {
            viewToScrollTo = [noteBox.middleLines objectAtIndex:[noteBox.middleLines indexOfObject:noteLine] + 1];
            margin = currentKeyboardSize.height - 50;// + self.scroller.keyboardMargin;
        }
        [self.scroller scrollToView:viewToScrollTo withMargin:margin];
    }
}

- (void)saveNotes
{
    NSArray *notes = [noteBox.middleLines valueForKeyPath:@"note"];
    [FTCUserSettings setSavedNotes:notes];
}

- (void)noteLineChanged:(FTCNoteLine *)noteLine
{
    [self saveNotes];
    
}

- (void)noteLineWasRemoved:(FTCNoteLine *)noteLine
{
    [noteBox.middleLines removeObject:noteLine];
    [self saveNotes];
    [self.scroller layout];
}

- (void)configureView
{
    CGSize noteBoxSize = (CGSize){[FTCLayoutHelper getRowWidth:YES], 0};
    noteBox = [FTCDetailSectionBox boxWithSize:noteBoxSize andTitle:@"Notes" andActionImage:nil andActionCallback:nil];
    [_masterBox.boxes addObject:noteBox];
    //[noteBox.bottomLines addObject:_dummyLine];
    [self.scroller layout];
    NSArray *existingNotes = [FTCUserSettings getSavedNotes];
    
    if (existingNotes.count == 0) {
        [self addNoteLine:[self getNewNote]];
        [self saveNotes];
    }
    else {
        for (FTCNote *note in existingNotes) {
            [self addNoteLine:note];
        }
    }
    
    [self.scroller layoutWithSpeed:.3 completion:nil];
}

#pragma mark UISplitViewControllerDelegate
-(void)splitViewController:(UISplitViewController *)svc
    willHideViewController:(UIViewController *)aViewController
         withBarButtonItem:(UIBarButtonItem *)barButtonItem
      forPopoverController:(UIPopoverController *)pc
{
    NSLog(@"Will hide left side");
    FTCAppDelegate *appDelegate = [FTCLayoutHelper appDelegate];
    barButtonItem.image = [JASidePanelController defaultImage];
    
    appDelegate.menuPopover = pc;
    appDelegate.menuNavItem = barButtonItem;
    
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
}

-(void)splitViewController:(UISplitViewController *)svc
    willShowViewController:(UIViewController *)aViewController
 invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    NSLog(@"Will show left side");
    FTCAppDelegate *appDelegate = [FTCLayoutHelper appDelegate];
    
    appDelegate.menuPopover = nil;
    appDelegate.menuNavItem = nil;
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [FTCLayoutHelper setupMenuNav:self];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    currentKeyboardSize = CGSizeZero;
	// Do any additional setup after loading the view, typically from a nib.
    
    _masterBox = [FTCGridBox boxWithSize:(CGSize){[FTCLayoutHelper getGridWidth], 0}];
    _masterBox.contentLayoutMode = MGLayoutGridStyle;
    _masterBox.leftMargin = 8;
    _masterBox.topMargin = 8;
    
    self.scroller.contentLayoutMode = MGLayoutGridStyle;
    self.scroller.keyboardMargin = 10;
    self.scroller.delegate = self;
    self.scroller.canCancelContentTouches = NO;
    
    [self.scroller.boxes addObject:_masterBox];
    
    //_dummyLine = [MGLine lineWithSize:(CGSize){INPUT_WIDTH, 300}];
    [self configureView];
    
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if ([FTCLayoutHelper deviceIsIPad])
    {
        [_masterBox setSize:(CGSize){[FTCLayoutHelper getGridWidthForOrientation:toInterfaceOrientation], 0}];
        _masterBox.itemPadding = [FTCLayoutHelper getGridItemPaddingForOrientation:toInterfaceOrientation];
        [noteBox setSize:(CGSize){[FTCLayoutHelper getRowWidthForOrientation:toInterfaceOrientation doubleWide:YES]}];
        double newLineWidth = [FTCLayoutHelper getRowWidthForOrientation:toInterfaceOrientation doubleWide:YES] - 8;
        for (FTCNoteLine *line in noteBox.middleLines) {
            line.lineWidth = newLineWidth;
        }
        [self.scroller layoutWithSpeed:duration completion:nil];
    }
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if ([FTCLayoutHelper deviceIsIPad])
    {
        [self.scroller layout];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [self.scroller layout];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)keyboardWasShown:(NSNotification *)notification {
    // Get the size of the keyboard.
    currentKeyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
}

- (void)keyboardWillHide:(NSNotification *)notification {
    currentKeyboardSize = CGSizeZero;
}

@end
