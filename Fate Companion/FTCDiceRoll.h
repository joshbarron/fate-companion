//
//  FTCDiceRoll.h
//  Fate Companion
//
//  Created by Joshua Barron on 3/14/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FTCDiceRollResult.h"

@interface FTCDiceRoll : NSObject <NSCoding>

@property (strong) NSString *rollTitle;

@property (strong) FTCDiceRollResult *rollResult;

@property (strong) FTCDiceRollResult *oppositionRollResult;

- (BOOL) hasRoll;

- (BOOL) hasOpposition;

- (int) calculateOutcome;

- (id) initWithTitle:(NSString *)rollTitle andRollResult:(FTCDiceRollResult *)rollResult andOppositionRollResult:(FTCDiceRollResult *)oppositionRollResult;

@end
