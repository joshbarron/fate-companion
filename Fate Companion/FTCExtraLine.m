//
//  FTCExtraLine.m
//  Fate Companion
//
//  Created by Joshua Barron on 2/22/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCExtraLine.h"
#import "FontAwesomeKit/FAKIonIcons.h"
#import "UIControl+BlocksKit.h"
#import "FTCLayoutHelper.h"
#import "SIAlertView.h"

#define HEIGHT 50

@implementation FTCExtraLine

@synthesize delegate = _delegate;
@synthesize extra = _extra;
@synthesize nameField = _nameField;
@synthesize lineWidth = _lineWidth;

+ (double) getInputWidth:(double)totalWidth
{
    return totalWidth - 35;
}

+ (FTCExtraLine *)extraLineWithExtra:(FTCExtra *)extra delegate:(id<FTCExtraLineDelegate>)delegate
{
    double totalWidth = [FTCLayoutHelper getRowWidth:NO] - 8;
    
    double inputWidth = [FTCExtraLine getInputWidth:totalWidth];
    
    FAKIcon *deleteIcon = [FAKIonIcons ios7CloseOutlineIconWithSize:24];
    [deleteIcon addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]];
    UIImage *deleteIconImage = [deleteIcon imageWithSize:(CGSize){35, 30}];
    
    UIButton *actionButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, deleteIconImage.size.width, deleteIconImage.size.height)];
    [actionButton setBackgroundImage:deleteIconImage forState:UIControlStateNormal];
    actionButton.showsTouchWhenHighlighted = YES;
    MGBox *imgBox = [MGBox boxWithSize:actionButton.size];
    [imgBox addSubview:actionButton];
    
    
    JVFloatLabeledTextField *nameField = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(0, 0, inputWidth, HEIGHT)];
    [nameField setPlaceholder:@"Extra"];
    nameField.text = extra.name;
    nameField.font = [UIFont systemFontOfSize:16];
    
    FTCExtraLine *extraLine = [FTCExtraLine lineWithLeft:nameField right:imgBox size:(CGSize){totalWidth, HEIGHT}];
    extraLine.delegate = delegate;
    extraLine.extra = extra;
    extraLine.nameField = nameField;
    extraLine.lineWidth = totalWidth;
    
    nameField.delegate = extraLine;
    
    [actionButton bk_addEventHandler:^(id sender) {
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Remove Extra?" andMessage:@"Are you sure you wish to remove this extra?"];
        //alertView.messageFont = [UIFont systemFontOfSize:24];
        
        [alertView addButtonWithTitle:@"Cancel"
                                 type:SIAlertViewButtonTypeDefault
                              handler:nil];
        [alertView addButtonWithTitle:@"Remove" type:SIAlertViewButtonTypeDestructive handler:^(SIAlertView *alert) {
            [extraLine.delegate extraLineWasRemoved:extraLine];
        }];
        alertView.transitionStyle = SIAlertViewTransitionStyleFade;
        
        [alertView show];
    } forControlEvents:UIControlEventTouchUpInside];
    
    return extraLine;
}

- (void) setup
{
    [super setup];
    self.rasterize = YES;
}

- (void) layout {
    [self setSize:(CGSize){self.lineWidth, HEIGHT}];
    [self.nameField setSize:(CGSize){[FTCExtraLine getInputWidth:self.lineWidth], HEIGHT}];
    [super layout];
}

#pragma mark UITextFieldDelegate
- (void)textChanged:(UITextField *)textField
{
    [self.delegate extraLineNameChanged:self withText:textField.text];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self textChanged:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
    [self textChanged:textField];
	return YES;
}


@end
