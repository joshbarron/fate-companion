//
//  FTCSkillEntryViewController.m
//  Fate Companion
//
//  Created by Joshua Barron on 2/17/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCSkillEntryViewController.h"
#import "MGBox.h"
#import "MGTableBox.h"
#import "MGLine.h"
#import "FTCSkillValueTranslator.h"
#import "UIControl+BlocksKit.h"
#import "CRToast.h"
#import "FTCNotificationHelper.h"
#import "FTCLayoutHelper.h"

#define TAG_NAMEFIELD 2001
#define TAG_PICKERLINE 2002

#define TAG_OVERCOME 2003
#define TAG_CREATEADVANTAGE 2004
#define TAG_ATTACK 2005
#define TAG_DEFEND 2006

#define GLYPH_FONT @"FateCoreGlyphs"

@interface FTCSkillEntryViewController ()

@property (nonatomic, strong) NSArray *skillValues;

@end

@implementation FTCSkillEntryViewController
{
    MGTableBox *masterTableBox;
    FTCSkillValueTranslator *translator;
}
@synthesize scroller = _scroller;
@synthesize nameTextField = _nameTextField;
@synthesize valueTextField = _valueTextField;
@synthesize pickerView = _pickerView;

@synthesize skill = _skill;
@synthesize delegate = _delegate;

@synthesize skillValues = _skillValues;

@synthesize skillActions = _skillActions;
@synthesize selectedSkillValue = _selectedSkillValue;


//- (void)viewWillLayoutSubviews{
//    [super viewWillLayoutSubviews];
//    if ([FTCLayoutHelper deviceIsIPad])
//    {
//        //        self.view.bounds =CGRectMake(0, 0, 340, 340);
//        //        self.view.superview.bounds = CGRectMake(0, 0, 340, 340);
//        //        self.navigationController.view.bounds = self.view.superview.bounds;
//        //        self.navigationController.view.superview.bounds = self.navigationController.view.bounds;
//    }
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //UILabel *nameLabel = [[UILabel alloc] initWithFrame:self.nameTextField.frame];
    //nameLabel.text = @"Skill Name:";
    //[nameLabel sizeToFit];
    //self.nameTextField.floatingLabel.text = @"Skill Name";
    translator = [FTCSkillValueTranslator new];
    self.skillValues = [translator allSkillNamesAndValues];
    if (self.skill != nil)
    {
        self.skillActions = self.skill.actions;
    }
    else
    {
        self.skillActions = 0;
    }
    
    self.scroller.contentLayoutMode = MGLayoutGridStyle;
    self.scroller.scrollEnabled = NO;
    if ([FTCLayoutHelper deviceIsIPad])
    {
        self.scroller.keepFirstResponderAboveKeyboard = NO;
    }
    masterTableBox = [MGTableBox boxWithSize:(CGSize){320, 0}];
    
    [self.scroller.boxes addObject:masterTableBox];
    
    
    MGLine *instructions = [MGLine lineWithMultilineLeft:@"Tap on the fields below to edit this skill's name and level:" right:nil width:280 minHeight:20];
    instructions.leftMargin = instructions.rightMargin = 20;
    instructions.bottomMargin = instructions.topMargin = 5;
    [masterTableBox.topLines addObject:instructions];
    
    self.nameTextField = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(0, 0, 260, 40)];
    [self.nameTextField setPlaceholder:@"Skill Name"];
    self.nameTextField.font = [UIFont boldSystemFontOfSize:20];
    self.nameTextField.tag = TAG_NAMEFIELD;
    self.nameTextField.clearButtonMode = UITextFieldViewModeAlways;
    self.nameTextField.delegate = self;
    if (self.skill != nil)
    {
        self.nameTextField.text = self.skill.name;
    }
    MGLine *nameLine = [MGLine lineWithLeft:self.nameTextField right:nil size:(CGSize){260, 40}];
    nameLine.leftMargin = nameLine.rightMargin = 30;
    nameLine.bottomPadding = 5;
    //nameLine.borderStyle = MGBorderEtchedBottom;
    //nameLine.bottomBorderColor = [UIColor blackColor];
    [masterTableBox.topLines addObject:nameLine];
    
    
    self.valueTextField = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(0, 0, 260, 40)];
    [self.valueTextField setPlaceholder:@"Skill Level"];
    [[self.valueTextField valueForKey:@"textInputTraits"] setValue:[UIColor clearColor] forKey:@"insertionPointColor"];
    self.valueTextField.font = [UIFont boldSystemFontOfSize:20];
    self.valueTextField.tag = TAG_PICKERLINE;
    self.valueTextField.clearButtonMode = UITextFieldViewModeNever;
    self.valueTextField.delegate = self;
    self.pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 80)];
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    if (self.skill != nil)
    {
        self.selectedSkillValue = [self.skill.value intValue];
        self.valueTextField.text = [self.skillValues objectAtIndex:self.selectedSkillValue + 2];
    }
    self.valueTextField.inputView = self.pickerView;
    UIToolbar *valueToolbar = [[UIToolbar alloc] initWithFrame:
                               CGRectMake(0,0, 320, 44)]; //should code with variables to support view resizing
    UIBarButtonItem *doneButton =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                  target:self action:@selector(inputAccessoryViewDidFinish:)];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [valueToolbar setItems:[NSArray arrayWithObjects: flexibleSpace, doneButton, nil] animated:NO];
    self.valueTextField.inputAccessoryView = valueToolbar;
    
    
    MGLine *valueLine = [MGLine lineWithLeft:self.valueTextField right:nil size:(CGSize){260, 40}];
    valueLine.leftMargin = valueLine.rightMargin = 30;
    valueLine.topMargin = 5;
    valueLine.font = [UIFont boldSystemFontOfSize:20];
    valueLine.rightFont = [UIFont systemFontOfSize:20];
    
    [masterTableBox.middleLines addObject:valueLine];
    
    [self setupActionsLine];
    
    [self.scroller layout];
    [self.scroller scrollToView:masterTableBox withMargin:0];
    
}

- (void)setupActionsLine
{
    MGLine *actionsInstructions = [MGLine lineWithMultilineLeft:@"Tap the icons below to select what actions this skill can be used with:" right:nil width:280 minHeight:20];
    actionsInstructions.leftMargin = actionsInstructions.rightMargin = 20;
    actionsInstructions.bottomMargin = actionsInstructions.topMargin = 5;
    [masterTableBox.bottomLines addObject:actionsInstructions];
    
    MGBox *overcomeButton = [self getButtonForAction:Overcome];
    MGBox *createAdvantageButton = [self getButtonForAction:CreateAdvantage];
    MGBox *attackButton = [self getButtonForAction:Attack];
    MGBox *defendButton = [self getButtonForAction:Defend];
    NSArray *buttonArray = [NSArray arrayWithObjects:overcomeButton, createAdvantageButton, attackButton, defendButton, nil];
    MGBox *actions = [MGBox boxWithSize:(CGSize){280, 70}];
    actions.contentLayoutMode = MGLayoutGridStyle;
    [actions.boxes addObjectsFromArray:buttonArray];
    
    MGLine *actionsLine = [MGLine lineWithLeft:actions right:nil size:(CGSize){280, 70}];
    actionsLine.leftMargin = actionsLine.rightMargin = 20;
    actionsLine.topMargin = 5;
    
    [masterTableBox.bottomLines addObject:actionsLine];
}

- (MGBox *)getButtonForAction:(SkillActions) action
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 70, 70);
    //[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 70, 70)];
    NSString *label = [translator actionsStringForActions:action];
    UIColor *tintColor = [FTCLayoutHelper getTintColor];
    [button setTitle:label forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button setTitleColor:tintColor forState:UIControlStateSelected];
    [button setTitleShadowColor:[UIColor clearColor] forState:UIControlStateSelected];
    [button setTitleShadowColor:[UIColor clearColor] forState:UIControlStateHighlighted];
    button.titleLabel.font = [UIFont fontWithName:GLYPH_FONT size:54];
    [button.titleLabel setTextAlignment: NSTextAlignmentCenter];
    [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    //[button setTitleEdgeInsets:UIEdgeInsetsMake(0, 7.5, 0, 0)];
    [button bk_addEventHandler:^(id sender) {
        UIButton *button = (UIButton *)sender;
        button.selected = !button.selected;
        if (button.tag == TAG_OVERCOME)
            self.skillActions ^= Overcome;
        else if (button.tag == TAG_CREATEADVANTAGE)
            self.skillActions ^= CreateAdvantage;
        else if (button.tag == TAG_ATTACK)
            self.skillActions ^= Attack;
        else if (button.tag == TAG_DEFEND)
            self.skillActions ^= Defend;
    } forControlEvents:UIControlEventTouchUpInside];
    
    if (action == Overcome)
    {
        button.tag = TAG_OVERCOME;
        if (self.skillActions & Overcome)
            button.selected = YES;
    }
    else if (action == CreateAdvantage)
    {
        button.tag = TAG_CREATEADVANTAGE;
        if (self.skillActions & CreateAdvantage)
            button.selected = YES;
    }
    else if (action == Attack)
    {
        button.tag = TAG_ATTACK;
        if (self.skillActions & Attack)
            button.selected = YES;
    }
    else if (action == Defend)
    {
        button.tag = TAG_DEFEND;
        if (self.skillActions & Defend)
            button.selected = YES;
    }
    
    MGBox *buttonBox = [MGBox boxWithSize:button.size];
    [buttonBox addSubview:button];
    
    return buttonBox;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)inputAccessoryViewDidFinish:(id) sender
{
    NSInteger selectedIndex = [self.pickerView selectedRowInComponent:0];
    self.valueTextField.text = [self.skillValues objectAtIndex:selectedIndex];
    self.selectedSkillValue = ((int)selectedIndex) - 2;
    [self.valueTextField resignFirstResponder];
}

#pragma mark UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField.tag == TAG_NAMEFIELD)
    {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 15) ? NO : YES;
    }
    return YES;
}

- (void)textChanged:(UITextField *)textField {
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
    [self textChanged:textField];
	return YES;
}

#pragma mark - UIPickerView DataSource
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [_skillValues count];
}

#pragma mark - UIPickerView Delegate
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 30.0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [_skillValues objectAtIndex:row];
}

//If the user chooses from the pickerview, it calls this function;
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    //Let's print in the console what the user had chosen;
    self.valueTextField.text = [self.skillValues objectAtIndex:row];
    self.selectedSkillValue = ((int)row) - 2;
}


- (IBAction)cancelButtonWasTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveButtonWasTapped:(id)sender {
    NSString *name = self.nameTextField.text;
    
    if (name == nil || name.length == 0)
    {
        [CRToastManager showNotificationWithOptions:[FTCNotificationHelper warningNotification:@"Oops!" andSubtitle:@"Please enter a name for your skill."] completionBlock:nil];
        return;
    }
    
    NSString *valueText = self.valueTextField.text;
    if (valueText == nil || valueText.length == 0)
    {
        [CRToastManager showNotificationWithOptions:[FTCNotificationHelper warningNotification:@"Oops!" andSubtitle:@"Please select a level for your skill."] completionBlock:nil];
        return;
    }
    
    NSString *validationMessage = nil;
    if (![self.delegate validateSkillEntry:self withMessage:&validationMessage])
    {
        [CRToastManager showNotificationWithOptions:[FTCNotificationHelper warningNotification:@"Oops!" andSubtitle:validationMessage] completionBlock:nil];
        return;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.delegate skillWasSaved:self];
}
@end
