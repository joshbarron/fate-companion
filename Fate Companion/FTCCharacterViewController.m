//
//  FTCFirstViewController.m
//  Fate Companion
//
//  Created by Joshua Barron on 1/25/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCCharacterViewController.h"
#import "FTCDocument.h"
#import "NSDate+FormattedStrings.h"
#import "FTCEntry.h"
#import "FTCCharacterMetadata.h"
#import "MGScrollView.h"
#import "FTCSidePanelControllerViewController.h"
#import "UIViewController+JASidePanel.h"
#import "JASidePanelController.h"
#import "FTCCenterPanelSegue.h"
#import "FTCNotificationHelper.h"
#import "CRToast.h"
#import "SIAlertView.h"
#import "FTCDocumentManager.h"
#import "FTCGridBox.h"
#import "FTCLayoutHelper.h"


@interface FTCCharacterViewController ()
{
    NSMutableArray *_objects;
    NSMutableArray *_objectBoxes;
    //NSURL * _localRoot;
    //FTCDocument * _selDocument;
    UITextField * _activeTextField;
    FTCGridBox *_characterGrid;
    
    //NSMetadataQuery * _query;
    BOOL _iCloudURLsReady;
    //NSMutableArray * _iCloudURLs;
}
@end


@implementation FTCCharacterViewController
@synthesize addButton = _addButton;
@synthesize scroller = _scroller;
//@synthesize popover = _popover;
// Add new methods right after @implementation
#pragma mark Helpers

- (BOOL)docNameExistsInObjects:(NSString *)docName {
    BOOL nameExists = NO;
    for (FTCEntry * entry in _objects) {
        if ([[entry.fileURL lastPathComponent] isEqualToString:docName]) {
            nameExists = YES;
            break;
        }
    }
    return nameExists;
}

- (NSString*)getDocFilename:(NSString *)prefix uniqueInObjects:(BOOL)uniqueInObjects {
    NSInteger docCount = 0;
    NSString* newDocName = nil;
    
    // At this point, the document list should be up-to-date.
    BOOL done = NO;
    BOOL first = YES;
    while (!done) {
        if (first) {
            first = NO;
            newDocName = [NSString stringWithFormat:@"%@.%@",
                          prefix, FTC_EXTENSION];
        } else {
            newDocName = [NSString stringWithFormat:@"%@ %d.%@",
                          prefix, docCount, FTC_EXTENSION];
        }
        
        // Look for an existing document with the same name. If one is
        // found, increment the docCount value and try again.
        BOOL nameExists;
        if (uniqueInObjects) {
            nameExists = [self docNameExistsInObjects:newDocName];
        } else {
            // TODO
            return nil;
        }
        if (!nameExists) {
            break;
        } else {
            docCount++;
        }
        
    }
    
    return newDocName;
}

#pragma mark FTCCreateCharacterDelegate

- (IBAction)addNewCharacter:(id)sender
{
    [self performSegueWithIdentifier:@"openCreateCharacter" sender:self];
}

- (void) characterWasCreatedWithName:(NSString *)name andType:(CharacterType)type
{
    // Determine a unique filename to create
    NSString *fileName = [self getDocFilename:name uniqueInObjects:YES];
    [[FTCDocumentManager sharedManager] createNewCharacterWithName:name andType:type andFile:fileName success:^(BOOL success, FTCDocument *doc) {
        if (!success) {
            NSLog(@"Failed to create file."); //todo alert
            return;
        }
        NSURL * fileURL = doc.fileURL;
        NSLog(@"File created at %@", fileURL);
        FTCCharacterMetadata * metadata = doc.metadata;
        UIDocumentState state = doc.documentState;
        NSFileVersion * version = [NSFileVersion currentVersionOfItemAtURL:fileURL];
        
        // Add on the main thread and perform the segue
        [FTCDocumentManager sharedManager].currentDocument = doc;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self addOrUpdateEntryWithURL:fileURL metadata:metadata state:state version:version];
            [self performSegueWithIdentifier:@"selectCharacter" sender:self];
        });
    }];
}

#pragma mark Entry management methods

- (int)indexOfEntryWithFileURL:(NSURL *)fileURL {
    __block int retval = -1;
    [_objects enumerateObjectsUsingBlock:^(FTCEntry * entry, NSUInteger idx, BOOL *stop) {
        if ([entry.fileURL isEqual:fileURL]) {
            retval = idx;
            *stop = YES;
        }
    }];
    return retval;
}

- (FTCEntry *)entryWithFileURL:(NSURL *)fileURL {
    for (FTCEntry *entry in _objects) {
        if ([entry.fileURL isEqual:fileURL])
            return entry;
    }
    return nil;
}

- (void)addOrUpdateEntryWithURL:(NSURL *)fileURL metadata:(FTCCharacterMetadata *)metadata state:(UIDocumentState)state version:(NSFileVersion *)version {
    
    int index = [self indexOfEntryWithFileURL:fileURL];
    
    // Not found, so add
    if (index == -1) {
        
        FTCEntry * entry = [[FTCEntry alloc] initWithFileURL:fileURL metadata:metadata state:state version:version];
        
        [_objects addObject:entry];
        [self addCharacterRow:entry];
    }
    
    // Found, so edit
    else {
        
        FTCEntry * entry = [_objects objectAtIndex:index];
        entry.metadata = metadata;
        entry.state = state;
        entry.version = version;
        [self refreshCharacterRow: entry atIndex:index];
        //        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        
    }
    
    //update nav
//    FTCSidePanelControllerViewController *side = (FTCSidePanelControllerViewController *)[self sidePanelController];
//    FTCMainNavigationViewController *mainNav = [side getMainNav];
//    [mainNav updateCharacterCount:_objects.count];
}


- (void)removeEntryWithURL:(NSURL *)fileURL {
    int index = [self indexOfEntryWithFileURL:fileURL];
    [_objects removeObjectAtIndex:index];
    [self removeCharacterRow: index];
    //    [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
}



#pragma mark File management methods

- (void)loadDocAtURL:(NSURL *)fileURL {
    
    // Open doc so we can read metadata
    FTCDocument * doc = [[FTCDocument alloc] initWithFileURL:fileURL];
    [doc openWithCompletionHandler:^(BOOL success) {
        
        // Check status
        if (!success) {
            NSLog(@"Failed to open %@", fileURL);
            return;
        }
        
        // Preload metadata on background thread
        FTCCharacterMetadata * metadata = doc.metadata;
        NSURL * fileURL = doc.fileURL;
        UIDocumentState state = doc.documentState;
        NSFileVersion * version = [NSFileVersion currentVersionOfItemAtURL:fileURL];
        NSLog(@"Loaded File URL: %@", [doc.fileURL lastPathComponent]);
        
        // Close since we're done with it
        [doc closeWithCompletionHandler:^(BOOL success) {
            
            // Check status
            if (!success) {
                NSLog(@"Failed to close %@", fileURL);
                // Continue anyway...
            }
            
            // Add to the list of files on main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                [self addOrUpdateEntryWithURL:fileURL metadata:metadata state:state version:version];
            });
        }];
    }];
    
}

- (void)deleteEntry:(FTCEntry *)entry {
    // Wrap in file coordinator
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
        NSFileCoordinator* fileCoordinator = [[NSFileCoordinator alloc] initWithFilePresenter:nil];
        [fileCoordinator coordinateWritingItemAtURL:entry.fileURL
                                            options:NSFileCoordinatorWritingForDeleting
                                              error:nil
                                         byAccessor:^(NSURL* writingURL) {
                                             // Simple delete to start
                                             NSFileManager* fileManager = [[NSFileManager alloc] init];
                                             [fileManager removeItemAtURL:entry.fileURL error:nil];
                                         }];
    });
    
    // Fixup view
    [self removeEntryWithURL:entry.fileURL];
    
}

- (void)iCloudToLocal {
    NSLog(@"iCloud => local");
}

- (void)localToiCloud {
    NSLog(@"local => iCloud");
}

#pragma mark Refresh Methods

- (void)addCharacterRow:(FTCEntry *)entry
{
    //Create box
    FTCEntryBox *entryBox = [FTCEntryBox characterBox:entry withWidth:320 forView:(id<UITextFieldDelegate, FTCEntryBoxDelegate>)self];
    
    //Add it to object boxes
    [_objectBoxes addObject:entryBox];
    
    //Add it to grid
    [_characterGrid.boxes addObject:entryBox];
    
    //Relayout
    [self.scroller layoutWithSpeed:.3 completion:nil];
    //    [self.scroller layout];
}

- (void)refreshCharacterRow:(FTCEntry *)entry atIndex:(int)index
{
    //Create new box
    FTCEntryBox *newEntryBox = [FTCEntryBox characterBox:entry withWidth:320 forView:(id<UITextFieldDelegate, FTCEntryBoxDelegate>)self];
    
    //Replace it in object boxes
    [_objectBoxes replaceObjectAtIndex:index withObject:newEntryBox];
    
    //Replace it in the grid
    [_characterGrid.boxes replaceObjectAtIndex:index withObject:newEntryBox];
    
    //Relayout
    [self.scroller layout];
}

- (void)removeCharacterRow:(int)index
{
    //Remove box from object boxes
    [_objectBoxes removeObjectAtIndex:index];
    
    //Remove from grid
    [_characterGrid.boxes removeObjectAtIndex:index];
    
    //Relayout
    [self.scroller layoutWithSpeed:.3 completion:nil];
    //    [self.scroller layout];
}

- (void)refresh {
    _iCloudURLsReady = NO;
    //[_iCloudURLs removeAllObjects];
    
    [_objects removeAllObjects];
    [_objectBoxes removeAllObjects];
    //[self.tableView reloadData];
    [_characterGrid.boxes removeAllObjects];
    [self.scroller.boxes removeAllObjects];
    
    self.scroller.contentLayoutMode = MGLayoutGridStyle;
    self.scroller.bottomPadding = 8;
    
    [self.scroller.boxes addObject:_characterGrid];
    
    self.navigationItem.rightBarButtonItem.enabled = NO;
    
    FTCDocumentManager *manager = [FTCDocumentManager sharedManager];
    
    [manager initializeiCloudAccessWithCompletion:^(BOOL available) {
        FTCDocumentManager *_manager = [FTCDocumentManager sharedManager];
        
        if (![_manager iCloudAvailable]) {
            
            // If iCloud isn't available, set promoted to no (so we can ask them next time it becomes available)
            [_manager setiCloudPrompted:NO];
            
            // If iCloud was toggled on previously, warn user that the docs will be loaded locally
            if ([_manager iCloudWasOn]) {
                SIAlertView * alertView = [[SIAlertView alloc] initWithTitle:@"You're Not Using iCloud" andMessage:@"Your documents were removed from this device but remain stored in iCloud."];
                [alertView addButtonWithTitle:@"OK"
                                         type:SIAlertViewButtonTypeDefault
                                      handler:nil];
                alertView.transitionStyle = SIAlertViewTransitionStyleFade;
                
                [alertView show];
            }
            
            // No matter what, iCloud isn't available so switch it to off.
            [_manager setiCloudOn:NO];
            [_manager setiCloudWasOn:NO];
            
        } else {
            
            // Ask user if want to turn on iCloud if it's available and we haven't asked already
            if (![_manager iCloudOn] && ![_manager iCloudPrompted]) {
                
                [_manager setiCloudPrompted:YES];
                SIAlertView * alertView = [[SIAlertView alloc] initWithTitle:@"iCloud is Available" andMessage:@"Automatically store your characters in the cloud to keep them up-to-date across all your devices."];
                [alertView addButtonWithTitle:@"Later" type:SIAlertViewButtonTypeCancel handler:nil];
                [alertView addButtonWithTitle:@"Use iCloud"
                                         type:SIAlertViewButtonTypeDefault
                                      handler:^(SIAlertView *alert) {
                                          [[FTCDocumentManager sharedManager] setiCloudOn:YES];
                                          [self refresh];
                                      }];
                alertView.transitionStyle = SIAlertViewTransitionStyleFade;
                
                [alertView show];
                
            }
            
            // If iCloud newly switched off, move local docs to iCloud
            if ([_manager iCloudOn] && ![_manager iCloudWasOn]) {
                [self localToiCloud];
            }
            
            // If iCloud newly switched on, move iCloud docs to local
            if (![_manager iCloudOn] && [_manager iCloudWasOn]) {
                [self iCloudToLocal];
            }
            
            // Start querying iCloud for files, whether on or off
            [_manager startQuery:self];
            
            // No matter what, refresh with current value of iCloudOn
            [_manager setiCloudWasOn:[_manager iCloudOn]];
            
        }
        
        if (![_manager iCloudAvailable]) {
            [_manager loadLocal:self];
            //[self loadLocal];
        }
    }];
    
    
}

#pragma mark FTCDocumentManagerDelegate

- (void) documentsAvailable:(NSArray *)urls
{
    if ([[FTCDocumentManager sharedManager] iCloudOn]) {
        
        // Remove deleted files
        // Iterate backwards because we need to remove items form the array
        for (int i = _objects.count -1; i >= 0; --i) {
            FTCEntry * entry = [_objects objectAtIndex:i];
            if (![urls containsObject:entry.fileURL]) {
                [self removeEntryWithURL:entry.fileURL];
            }
        }
        
        // Add new files
        for (NSURL * fileURL in urls) {
            [self loadDocAtURL:fileURL];
        }
        
    }
    else {
        for (int i=0; i < urls.count; i++) {
            
            NSURL * fileURL = [urls objectAtIndex:i];
            if ([[fileURL pathExtension] isEqualToString:FTC_EXTENSION]) {
                NSLog(@"Found local file: %@", fileURL);
                [self loadDocAtURL:fileURL];
            }
        }
    }
    
    self.navigationItem.rightBarButtonItem.enabled = YES;
}


#pragma mark FTCDetailViewControllerDelegate

- (void)detailViewControllerDidClose:(FTCCharacterDetailViewController *)detailViewController {
    //[self.navigationController popViewControllerAnimated:YES];
//    NSFileVersion * version = [NSFileVersion currentVersionOfItemAtURL:detailViewController.doc.fileURL];
//    [self addOrUpdateEntryWithURL:detailViewController.doc.fileURL metadata:detailViewController.doc.metadata state:detailViewController.doc.documentState version:version];
    [self refresh];
}

- (void)detailViewControllerDidDelete:(FTCCharacterDetailViewController *)detailViewController {
    //todo: look at some kind of loading indicator for a little bit once coming back from these
    [self refresh];
//    int index = [self indexOfEntryWithFileURL:detailViewController.doc.fileURL];
//    FTCEntry *entry = [_objects objectAtIndex:index];
   //[self deleteEntry:entry];
    //[self removeEntryWithURL:detailViewController.doc.fileURL];
    //[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark FTCEntryBoxDelegate

- (void) boxWasTapped:(FTCEntryBox *)box
{
    int index = [_objectBoxes indexOfObject:box];
    FTCEntry *entry = [_objects objectAtIndex:index];
    FTCDocument *doc = [[FTCDocument alloc] initWithFileURL:entry.fileURL];
    [doc openWithCompletionHandler:^(BOOL success) {
        if (success) {
            [[FTCDocumentManager sharedManager] stopQuery];
            [FTCDocumentManager sharedManager].currentDocument = doc;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self performSegueWithIdentifier:@"selectCharacter" sender:self];
            });
        }
        else
        {
            //todo alert
            NSLog(@"OH NOES");
        }
    }];
}

#pragma mark UISplitViewControllerDelegate
-(void)splitViewController:(UISplitViewController *)svc
    willHideViewController:(UIViewController *)aViewController
         withBarButtonItem:(UIBarButtonItem *)barButtonItem
      forPopoverController:(UIPopoverController *)pc
{
    NSLog(@"Will hide left side");
    FTCAppDelegate *appDelegate = [FTCLayoutHelper appDelegate];
    barButtonItem.image = [JASidePanelController defaultImage];
    
    appDelegate.menuPopover = pc;
    appDelegate.menuNavItem = barButtonItem;

    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
}

-(void)splitViewController:(UISplitViewController *)svc
    willShowViewController:(UIViewController *)aViewController
 invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    NSLog(@"Will show left side");
    FTCAppDelegate *appDelegate = [FTCLayoutHelper appDelegate];
    
    appDelegate.menuPopover = nil;
    appDelegate.menuNavItem = nil;
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
}

#pragma mark View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [FTCLayoutHelper setupMenuNav:self];
	// Do any additional setup after loading the view, typically from a nib.
    //[self.addButton setAction:@selector(addNewCharacter:)];
    
    _objects = [[NSMutableArray alloc] init];
    _objectBoxes = [[NSMutableArray alloc] init];
    _characterGrid = [FTCGridBox boxWithSize:CGSizeMake([FTCLayoutHelper getGridWidth], 0)];
    _characterGrid.contentLayoutMode = MGLayoutGridStyle;
    double itemPadding = UIInterfaceOrientationIsLandscape([FTCLayoutHelper getDeviceOrientation]) ? 16 : 32;
    _characterGrid.itemPadding = itemPadding;
    //_iCloudURLs = [[NSMutableArray alloc] init];
    
    [self refresh];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    if ([FTCUserSettings showTutorial]) {
        //todo: do something interesting here.

    }
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if ([FTCLayoutHelper deviceIsIPad])
    {
        [_characterGrid setSize:(CGSize){[FTCLayoutHelper getGridWidthForOrientation:toInterfaceOrientation], 0}];
        double itemPadding = UIInterfaceOrientationIsLandscape(toInterfaceOrientation) ? 16 : 32;
        _characterGrid.itemPadding = itemPadding;
        //_characterGrid.itemPadding = [FTCLayoutHelper getGridItemPaddingForOrientation:toInterfaceOrientation];
        NSLog(@"%f", _characterGrid.itemPadding);
        [self.scroller layoutWithSpeed:duration completion:nil];
    }
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if ([FTCLayoutHelper deviceIsIPad])
    {
        [self.scroller layout];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"selectCharacter"]) {
        id viewController = nil;
        if ([FTCLayoutHelper deviceIsIPhone]) {
            UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
            viewController = navController.visibleViewController;
        }
        else {
            viewController = segue.destinationViewController;
        }
        [viewController setDelegate:self];
        //[viewController setDoc:_selDocument];
    }
    else if ([[segue identifier] isEqualToString:@"openCreateCharacter"]) {
        UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
        
        FTCCreateCharacterViewController *vc = (FTCCreateCharacterViewController *)navController.visibleViewController;
        
        vc.delegate = self;
    }
}

- (void)didBecomeActive:(NSNotification *)notification {
    [self refresh];
}

- (void)viewWillAppear:(BOOL)animated {
    [[FTCDocumentManager sharedManager] enableQueryUpdates];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[FTCDocumentManager sharedManager] disableQueryUpdates];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
