#import "MGBox.h"

@interface FTCPhotoBox : MGBox

+ (FTCPhotoBox *)photoBoxFor:(UIImage *)image withSize:(CGSize)size;

- (void)setPhoto: (UIImage *)image;

@end
