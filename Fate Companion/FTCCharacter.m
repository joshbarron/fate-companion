//
//  FTCCharacter.m
//  Fate Companion
//
//  Created by Joshua Barron on 1/25/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCCharacter.h"

@implementation FTCCharacter 
@synthesize photo = _photo;
@synthesize name = _name;
@synthesize description = _description;

@synthesize characterType = _characterType;

@synthesize fatePoints = _fatePoints;
@synthesize refresh = _refresh;

//Aspects
@synthesize highConcept = _highConcept;
@synthesize trouble = _trouble;
@synthesize aspects = _aspects;

//Skills
@synthesize skills = _skills;

//Extras
@synthesize extras = _extras;

//Stress Tracks
@synthesize stressTracks = _stressTracks;

//Consequences
@synthesize consequences = _consequences;


- (id)initWithPhoto:(UIImage *)photo andName:(NSString *)name {
    if ((self = [super init])) {
        self.photo = photo;
        self.name = name;
        self.characterType = FateCoreCharacter;
        self.description = @"";
        self.fatePoints = [NSNumber numberWithInt:0];
        self.refresh = [NSNumber numberWithInt:3];
        self.highConcept = [[FTCAspect alloc] initWithText:nil];
        self.trouble = [[FTCAspect alloc] initWithText:nil];
        self.approaches = [[NSMutableArray alloc] init];
        self.skills = [[NSMutableArray alloc] init];
        self.aspects = [[NSMutableArray alloc] init];
        self.stunts = [[NSMutableArray alloc] init];
        self.extras = [[NSMutableArray alloc] init];
        self.stressTracks = [[NSMutableArray alloc] init];
        self.consequences = [[NSMutableArray alloc] init];
    }
    return self;
}

- (id)init {
    return [self initWithPhoto:nil andName:nil];
}

#pragma mark NSCoding

#define CURRENT_VERSION 2

#define kVersionKey @"Version"
#define kPhotoKey @"Photo"
#define kNameKey @"Name"

#define kCharacterTypeKey @"CharacterType"

#define kDescriptionKey @"Description"
#define kFatePointsKey @"FatePoints"
#define kRefreshKey @"Refresh"

#define kHighConceptKey @"HighConcept"
#define kTroubleKey @"Trouble"
#define kAspectsKey @"Aspects"

#define kApproachesKey @"Approaches"

#define kSkillsKey @"Skills"

#define kStuntsKey @"Stunts"

#define kExtrasKey @"Extras"

#define kStressTracksKey @"StressTracks"

#define kConsequencesKey @"Consequences"


- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeInt:CURRENT_VERSION forKey:kVersionKey];
    NSData * photoData = UIImagePNGRepresentation(self.photo);
    [encoder encodeObject:photoData forKey:kPhotoKey];
    [encoder encodeObject:self.name forKey:kNameKey];
    [encoder encodeObject:self.description forKey:kDescriptionKey];
    
    [encoder encodeInt:self.characterType forKey:kCharacterTypeKey];
    
    [encoder encodeObject:self.fatePoints forKey:kFatePointsKey];
    [encoder encodeObject:self.refresh forKey:kRefreshKey];
    
    [encoder encodeObject:self.highConcept forKey:kHighConceptKey];
    [encoder encodeObject:self.trouble forKey:kTroubleKey];
    [encoder encodeObject:self.aspects forKey:kAspectsKey];
    
    [encoder encodeObject:self.approaches forKey:kApproachesKey];
    
    [encoder encodeObject:self.skills forKey:kSkillsKey];
    
    [encoder encodeObject:self.stunts forKey:kStuntsKey];
    
    [encoder encodeObject:self.extras forKey:kExtrasKey];
    
    [encoder encodeObject:self.stressTracks forKey:kStressTracksKey];
    
    [encoder encodeObject:self.consequences forKey:kConsequencesKey];
}

- (id)initWithCoder:(NSCoder *)decoder {
    int version = [decoder decodeIntForKey:kVersionKey];
    
    CharacterType characterType;
    if (version < 2) {
        characterType = FateCoreCharacter;
    } else {
        characterType = [decoder decodeIntForKey:kCharacterTypeKey];
    }
    
    NSData * photoData = [decoder decodeObjectForKey:kPhotoKey];
    UIImage * photo = [UIImage imageWithData:photoData];
    NSString * name = [decoder decodeObjectForKey:kNameKey];
    NSString * description = [decoder decodeObjectForKey:kDescriptionKey];
    
    NSNumber * fatePoints = [decoder decodeObjectForKey:kFatePointsKey];
    NSNumber * refresh = [decoder decodeObjectForKey:kRefreshKey];
    
    FTCAspect *highConcept = [decoder decodeObjectForKey:kHighConceptKey];
    FTCAspect *trouble = [decoder decodeObjectForKey:kTroubleKey];
    NSMutableArray *aspects = [[decoder decodeObjectForKey:kAspectsKey] mutableCopy];
    
    NSMutableArray *approaches;
    if (version < 2) {
        approaches = [[NSMutableArray alloc] init];
    } else {
        approaches = [[decoder decodeObjectForKey:kApproachesKey] mutableCopy];
    }
    
    NSMutableArray *skills = [[decoder decodeObjectForKey:kSkillsKey] mutableCopy];
    
    NSMutableArray *stunts = [[decoder decodeObjectForKey:kStuntsKey] mutableCopy];
    
    NSMutableArray *extras = [[decoder decodeObjectForKey:kExtrasKey] mutableCopy];
    
    NSMutableArray *stressTracks = [[decoder decodeObjectForKey:kStressTracksKey] mutableCopy];
    
    NSMutableArray *consequences = [[decoder decodeObjectForKey:kConsequencesKey] mutableCopy];
    
    self = [self initWithPhoto:photo andName:name];
    self.characterType = characterType;
    self.description = description;
    self.fatePoints = fatePoints;
    self.refresh = refresh;
    
    self.highConcept = highConcept;
    self.trouble = trouble;
    self.aspects = aspects;
    
    self.approaches = approaches;
    
    self.skills = skills;
    
    self.stunts = stunts;
    
    self.extras = extras;
    
    self.stressTracks = stressTracks;
    
    self.consequences = consequences;
    
    return self;
}

@end
