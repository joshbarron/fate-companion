//
//  FTCSecondViewController.m
//  Fate Companion
//
//  Created by Joshua Barron on 1/25/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCDiceRollerViewController.h"
#import "FTCUserSettings.h"
#import "FTCDiceRoller.h"
#import "FTCDiceRoll.h"
#import "FontAwesomeKit/FAKIonIcons.h"
#import "FTCLayoutHelper.h"
#import "FTCGridBox.h"
#import "JASidePanelController.h"

#define ROW_WIDTH 304

@interface FTCDiceRollerViewController ()
{
    FTCGridBox *_masterBox;
}
@end

@implementation FTCDiceRollerViewController
@synthesize scroller = _scroller;
@synthesize rollBoxes = _rollBoxes;
@synthesize popover = _popover;

- (void) saveDiceRolls
{
    NSArray *diceRolls = [self.rollBoxes valueForKeyPath:@"diceRoll"];
    [FTCUserSettings setSavedDiceRolls:diceRolls];
}

- (IBAction)addButtonTapped:(id)sender {
    FTCDiceRollBox *rollBox = [self addDiceRollBox:nil];
    [self.scroller layoutWithSpeed:.3 completion:nil];
    [self.scroller scrollToView:rollBox withMargin:0];
    [self saveDiceRolls];
}

- (FTCDiceRollBox *)addDiceRollBox:(FTCDiceRoll *)diceRoll
{
    FAKIcon *deleteIcon = [FAKIonIcons ios7CloseOutlineIconWithSize:24];
    [deleteIcon addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
    UIImage *iconImage = [deleteIcon imageWithSize:(CGSize){30, 30}];
    
    CGSize diceBoxSize = (CGSize){[FTCLayoutHelper getRowWidth:NO], 0};
    
    FTCDiceRollBox *rollBox = [FTCDiceRollBox boxWithSize:diceBoxSize andTitle:@"Dice Roll" andActionImage:iconImage andActionCallback:^(id sender) {
        UIView *view = (UIView *)sender;
        while (view != nil && ![view isKindOfClass:[FTCDiceRollBox class]])
            view = view.superview;
        
        [UIView animateWithDuration:.3 animations:^{
            view.alpha = 0.0;
            view.transform = CGAffineTransformMakeScale(.1, .1);
        } completion:^(BOOL finished) {
            [self.rollBoxes removeObject:view];
            [_masterBox.boxes removeObject:view];
            [self.scroller layoutWithSpeed:.3 completion:^{
                [self saveDiceRolls];
            }];
        }];
    }];
    if (diceRoll == nil) {
        diceRoll = [[FTCDiceRoll alloc] initWithTitle:@"Dice Roll" andRollResult:nil andOppositionRollResult:nil];
    }
    [rollBox setupDiceRoll:diceRoll];
    rollBox.delegate = self;
    [_masterBox.boxes addObject:rollBox];
    [self.rollBoxes addObject:rollBox];
    
    NSLog(@"%f %f", rollBox.width, rollBox.height);
    
    return rollBox;
}

- (void)configureView
{
    [_masterBox.boxes removeAllObjects];
    [self.rollBoxes removeAllObjects];
    NSArray *existingRolls = [FTCUserSettings getSavedDiceRolls];
    
    if (existingRolls.count == 0) {
        [self addDiceRollBox:nil];
        [self saveDiceRolls];
    }
    else {
        for (FTCDiceRoll *roll in existingRolls) {
            [self addDiceRollBox:roll];
        }
    }
    
    [self.scroller layoutWithSpeed:.3 completion:nil];
}

- (void)viewDidLayoutSubviews
{
    double width = [FTCLayoutHelper getGridWidth];
    [_masterBox setSize:(CGSize){width, 0}];

    [self configureView];
    [super viewDidLayoutSubviews];
}

- (void)viewDidLoad
{
    [super viewDidLoad]; //704
    [FTCLayoutHelper setupMenuNav:self];
    self.rollBoxes = [[NSMutableArray alloc] init];
    _masterBox = [FTCGridBox boxWithSize:(CGSize){0, 0}];
    _masterBox.sizingMode = MGResizingShrinkWrap;
    _masterBox.leftMargin = 8;
    _masterBox.topMargin = 8;
    self.scroller.contentLayoutMode = MGLayoutGridStyle;
    self.scroller.alwaysBounceHorizontal = NO;
    self.scroller.keyboardMargin = 10;
    self.scroller.delegate = self;
    self.scroller.canCancelContentTouches = NO;
    
    [self.scroller.boxes addObject:_masterBox];
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if ([FTCLayoutHelper deviceIsIPad])
    {
        [_masterBox setSize:(CGSize){[FTCLayoutHelper getGridWidthForOrientation:toInterfaceOrientation], 0}];
        _masterBox.itemPadding = [FTCLayoutHelper getGridItemPaddingForOrientation:toInterfaceOrientation];
        [self.scroller layoutWithSpeed:duration completion:nil];
    }
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if ([FTCLayoutHelper deviceIsIPad])
    {
        [self.scroller layout];
    }
}

#pragma mark UISplitViewControllerDelegate
-(void)splitViewController:(UISplitViewController *)svc
    willHideViewController:(UIViewController *)aViewController
         withBarButtonItem:(UIBarButtonItem *)barButtonItem
      forPopoverController:(UIPopoverController *)pc
{
    NSLog(@"Will hide left side");
    FTCAppDelegate *appDelegate = [FTCLayoutHelper appDelegate];
    barButtonItem.image = [JASidePanelController defaultImage];
    
    appDelegate.menuPopover = pc;
    appDelegate.menuNavItem = barButtonItem;
    
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
}

-(void)splitViewController:(UISplitViewController *)svc
    willShowViewController:(UIViewController *)aViewController
 invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    NSLog(@"Will show left side");
    FTCAppDelegate *appDelegate = [FTCLayoutHelper appDelegate];
    
    appDelegate.menuPopover = nil;
    appDelegate.menuNavItem = nil;
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
}

@end
