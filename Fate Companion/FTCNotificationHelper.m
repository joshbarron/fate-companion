//
//  FTCNotificationHelper.m
//  Fate Companion
//
//  Created by Joshua Barron on 3/5/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCNotificationHelper.h"
#import "CRToast.h"
#import "FontAwesomeKit/FAKIonIcons.h"

#define NOTIFICATION_DURATION 1.0
#define NOTIFICATION_FONT [UIFont systemFontOfSize:24]

#define ICON_SIZE (CGSize){100, 100}
#define ICON_FONT_SIZE 54

#define SUCCESS_FG_COLOR [UIColor whiteColor]
#define SUCCESS_BG_COLOR [UIColor colorWithRed:2/255.0f green:137/255.0f blue:12/255.0f alpha:1.0f]

#define WARNING_FG_COLOR [UIColor whiteColor]
#define WARNING_BG_COLOR [UIColor redColor]

@implementation FTCNotificationHelper

+ (NSMutableDictionary *)baseOptions
{
    NSMutableDictionary *options =
    [@{
       kCRToastFontKey : NOTIFICATION_FONT,
       kCRToastTextAlignmentKey : @(NSTextAlignmentLeft),
       kCRToastTimeIntervalKey : @(NOTIFICATION_DURATION),
       kCRToastAnimationInTypeKey : @(CRToastAnimationTypeLinear),
       kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeLinear),
       kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
       kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop),
       kCRToastNotificationTypeKey : @(CRToastTypeNavigationBar),
       kCRToastNotificationPresentationTypeKey : @(CRToastPresentationTypeCover)
       } mutableCopy];
    
    return options;
}

#pragma mark Success

+ (NSDictionary *) successNotification:(NSString *)title andSubtitle:(NSString *)subtitle
{
    return [FTCNotificationHelper successNotification:title andSubtitle:subtitle andDuration:NOTIFICATION_DURATION];
}

+ (NSDictionary *) successNotification:(NSString *)title andSubtitle:(NSString *)subtitle andDuration:(float)duration
{
    FAKIcon * notificationIcon = [FAKIonIcons ios7CheckmarkOutlineIconWithSize:ICON_FONT_SIZE];
    [notificationIcon addAttribute:NSForegroundColorAttributeName value:SUCCESS_FG_COLOR];
    UIImage *icon = [notificationIcon imageWithSize:ICON_SIZE];
    
    NSMutableDictionary *options = [FTCNotificationHelper baseOptions];
    options[kCRToastImageKey] = icon;
    options[kCRToastTextKey] = title;
    options[kCRToastBackgroundColorKey] = SUCCESS_BG_COLOR;
    options[kCRToastTextColorKey] = SUCCESS_FG_COLOR;
    options[kCRToastTimeIntervalKey] = @(duration);
    
    if (subtitle != nil && subtitle.length > 0)
    {
        options[kCRToastSubtitleTextKey] = subtitle;
        options[kCRToastSubtitleTextAlignmentKey] = @(NSTextAlignmentLeft);
    }
    
    return options;
}

+ (NSDictionary *) successNotification:(NSString *)title
{
    return [FTCNotificationHelper successNotification:title andSubtitle:nil];
}

#pragma mark Warning

+ (NSDictionary *) warningNotification:(NSString *)title
{
    return [FTCNotificationHelper warningNotification:title andSubtitle:nil];
}

+ (NSDictionary *) warningNotification:(NSString *)title andSubtitle:(NSString *)subtitle
{
    FAKIcon * notificationIcon = [FAKIonIcons alertCircledIconWithSize:ICON_FONT_SIZE];
    [notificationIcon addAttribute:NSForegroundColorAttributeName value:WARNING_FG_COLOR];
    UIImage *icon = [notificationIcon imageWithSize:ICON_SIZE];
    
    NSMutableDictionary *options = [FTCNotificationHelper baseOptions];
    options[kCRToastImageKey] = icon;
    options[kCRToastTextKey] = title;
    options[kCRToastBackgroundColorKey] = WARNING_BG_COLOR;
    options[kCRToastTextColorKey] = WARNING_FG_COLOR;
    options[kCRToastTimeIntervalKey] = @(NOTIFICATION_DURATION * 1.5); //lengthen the notification a bit
    
    if (subtitle != nil && subtitle.length > 0)
    {
        options[kCRToastSubtitleTextKey] = subtitle;
        options[kCRToastSubtitleTextAlignmentKey] = @(NSTextAlignmentLeft);
    }
    
    return options;
}

@end
