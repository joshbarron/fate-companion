//
//  FTCDetailSectionBox.m
//  Fate Companion
//
//  Created by Joshua Barron on 2/11/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCDetailSectionBox.h"
#import "MGLine.h"
#import "FontAwesomeKit/FAKIonIcons.h"
#import "UIControl+BlocksKit.h"
#import "FTCLayoutHelper.h"

//#define WIDTH       304.0
#define TOP_MARGIN    0//4.0
#define BOTTOM_MARGIN 0.0
#define LEFT_MARGIN   0//4.0 //4
#define CORNER_RADIUS 4.0

@implementation FTCDetailSectionBox
{
    BOOL showingEmptyMessage;
}

+ (id)boxWithSize:(CGSize)size andTitle:(NSString *)title andActionImage:(UIImage *)actionImage andActionCallback:(void (^)(id sender))actionCallback;
{
    UIColor *tintColor = [FTCLayoutHelper getTintColor];
    
    FTCDetailSectionBox *box = [FTCDetailSectionBox boxWithSize:size];
    NSString *capitalizedTitle = [title uppercaseString];
    
    MGBox *rightBox = nil;
    if (actionImage != nil)
    {
        UIButton *actionButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, actionImage.size.width, actionImage.size.height)];
        [actionButton setBackgroundImage:actionImage forState:UIControlStateNormal];
        actionButton.showsTouchWhenHighlighted = YES;
        [actionButton bk_addEventHandler:actionCallback forControlEvents:UIControlEventTouchUpInside];
        
        rightBox = [MGBox boxWithSize:actionButton.size];
        [rightBox addSubview: actionButton]; //imgView];
        rightBox.rightMargin = 10;
    }
    
    MGLine *titleLine = [MGLine lineWithLeft:capitalizedTitle right:rightBox size:(CGSize){size.width - 8, 30}];
    titleLine.backgroundColor = tintColor;
    titleLine.textColor = [UIColor whiteColor];
    titleLine.font = [UIFont fontWithName:@"AvenirNext" size:28];
    titleLine.leftPadding = 12;
    titleLine.topPadding = 4;
    titleLine.topMargin = 8;
    titleLine.bottomMargin = 4;
    titleLine.bottomPadding = 4;
    
    [box.topLines addObject:titleLine];
    box.sectionTitle = [title lowercaseString];
    
    //setup default last lines border block
    box.lastLinesBorderBlock = ^(FTCDetailSectionBox *section){
        return 1;
    };
    return box;
}

- (void) showEmptyMessage
{
    if (self.middleLines.count > 0)
        return;
    
    showingEmptyMessage = YES;
    //Add a empty line
    NSMutableString *message = [[NSMutableString alloc] initWithString:@"Your character has no "];
    [message appendString:self.sectionTitle];
    [message appendString:@"; tap the add button above to get started."];
    double width = MIN(280, self.size.width);
    MGLine *emptyMessage = [MGLine lineWithMultilineLeft:message right:nil width:width minHeight:30];
    emptyMessage.font = [UIFont systemFontOfSize:12];
    [self.middleLines addObject:emptyMessage];

}

- (void) removeEmptyMessage
{
    if (showingEmptyMessage) {
        [self.middleLines removeAllObjects];
        showingEmptyMessage = NO;
    }
}

- (void)setup {
    [super setup];
    self.width = self.width ? self.width : [FTCLayoutHelper getRowWidth:NO];
    self.topMargin = TOP_MARGIN;
    self.leftMargin = LEFT_MARGIN;
    self.sizingMode = MGResizingNone;
    //self.backgroundColor = [UIColor whiteColor];
    
    // shadow
//    self.layer.shadowColor = [UIColor colorWithWhite:0.12 alpha:1].CGColor;
//    self.layer.shadowOffset = CGSizeMake(0, 0.5);
//    self.layer.shadowRadius = 1;
//    self.layer.shadowOpacity = 1;
    
    // performance
    //self.rasterize = YES;
    
}

- (void)layout {
    FTCDetailSectionBox * __weak wself = self;
    //Call layout block if it's there
    if (self.layoutBlock != nil) {
        self.layoutBlock(wself);
    }
    //Resize header
    if ([FTCLayoutHelper deviceIsIPad])
    {
        MGLine *titleLine = (MGLine *)[self.topLines firstObject];
        double titleWidth = self.width - 8;//[FTCLayoutHelper getInputWidth:isDoubleWide];
        [titleLine setSize:(CGSize){titleWidth, titleLine.size.height}];
        //NSLog(@"set title line width: %f", titleLine.width);
    }

    NSArray *allLines = self.allLines.array;

    MGLine *topLine = allLines[0];
    CAShapeLayer *topMask = CAShapeLayer.layer;
    topMask.frame = topLine.bounds;
    
    CGFloat width = topLine.frame.size.width;
    CGFloat height = topLine.frame.size.height;
    
    CGMutablePathRef path = CGPathCreateMutable();
    
    CGPathMoveToPoint(path, NULL, (height/2), 0);
    CGPathAddLineToPoint(path, nil, width, 0);
    CGPathAddLineToPoint(path, nil, width, (height/2));
    CGPathAddLineToPoint(path, nil, width-(height/2), height);
    CGPathAddLineToPoint(path, nil, 0, height);
    CGPathAddLineToPoint(path, nil, 0, (height/2));
    CGPathAddLineToPoint(path, nil, (height/2), 0);
    CGPathCloseSubpath(path);
    
    topMask.path = path;
    CGPathRelease(path);

    topLine.layer.mask = topMask;
    
    if (self.middleLines.count > 0)
    {
        //Remove bottom margin and border from n items (typically 1)
        int numBoxesToConsider = 1;
        if (self.lastLinesBorderBlock != nil)
            numBoxesToConsider = self.lastLinesBorderBlock(wself);
        
        int totalLines = self.middleLines.count;
        for (int i = 0; i < numBoxesToConsider; i++) {
            MGLine *line = (MGLine *)[self.middleLines objectAtIndex:(totalLines - i - 1)];
            line.bottomMargin = 0;
            line.borderStyle = MGBorderNone;
        }
    }
    
    [super layout];
}


@end
