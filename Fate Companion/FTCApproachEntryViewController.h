//
//  FTCApproachEntryViewController.h
//  Fate Companion
//
//  Created by Joshua Barron on 4/14/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FTCApproach.h"
#import "JVFloatLabeledTextField.h"
#import "MGScrollView.h"

@class FTCApproachEntryViewController;

@protocol FTCApproachEntryDelegate

- (BOOL) validateApproachEntry:(FTCApproachEntryViewController *)approachEntryController withMessage:(NSString * __autoreleasing *)validationMessage;
- (void) approachWasSaved:(FTCApproachEntryViewController *)approachEntryController;

@end


@interface FTCApproachEntryViewController : UIViewController <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
- (IBAction)cancelButtonWasTapped:(id)sender;
- (IBAction)saveButtonWasTapped:(id)sender;

@property (weak, nonatomic) IBOutlet MGScrollView *scroller;
@property (strong, nonatomic) JVFloatLabeledTextField *nameTextField;
@property (strong, nonatomic) JVFloatLabeledTextField *valueTextField;
@property (strong, nonatomic) UIPickerView *pickerView;

@property (weak) FTCApproach *approach;
@property (weak) id<FTCApproachEntryDelegate> delegate;
@property int selectedApproachValue;

@end
