//
//  FTCStressTrack.m
//  Fate Companion
//
//  Created by Joshua Barron on 2/28/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCStressTrack.h"

@implementation FTCStressTrack
@synthesize name = _name;
@synthesize slots = _slots;
@synthesize stressBoxes = _stressBoxes;

- (id) initWithName:(NSString *)name andSlotCount:(NSNumber *)slots andStressBoxes:(StressBoxes)stressBoxes
{
    if (self = [super init])
    {
        self.name = name;
        self.slots = slots;
        self.stressBoxes = stressBoxes;
    }
    return self;
}

- (BOOL) isStressBoxUsed:(int)index
{
    int numSlots = [self.slots intValue];
    switch (index) {
        case 0:
            return self.stressBoxes & FirstBox ? YES : NO;
            break;
        case 1:
            return self.stressBoxes & SecondBox ? YES : NO;
            break;
        case 2:
            if (numSlots < 3)
                return NO;
            return self.stressBoxes & ThirdBox ? YES : NO;
            break;
        case 3:
            if (numSlots < 4)
                return NO;
            return self.stressBoxes & FourthBox ? YES : NO;
            break;
        case 4:
            if (numSlots < 5)
                return NO;
            return self.stressBoxes & FifthBox ? YES : NO;
            break;
        default:
            return NO;
            break;
    }
}

- (StressBoxes) toggleStateOfStressBox:(int)index
{
    int numSlots = [self.slots intValue];
    StressBoxes newState = self.stressBoxes;
    switch (index) {
        case 0:
            newState ^= FirstBox;
            break;
        case 1:
            newState ^= SecondBox;
            break;
        case 2:
            if (numSlots < 3)
                break;
            newState ^= ThirdBox;
            break;
        case 3:
            if (numSlots < 4)
                break;
            newState ^= FourthBox;
            break;
        case 4:
            if (numSlots < 5)
                break;
            newState ^= FifthBox;
            break;
        default:
            break;
    }
    return newState;
}

#pragma mark NSCoding

#define kStressTrackVersionKey @"StressTrackVersion"
#define kStressTrackNameKey @"StressTrackName"
#define kStressTrackSlotsKey @"StressTrackSlots"
#define kStressTrackStressBoxesKey @"StressTrackStressBoxes"

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeInt:1 forKey:kStressTrackVersionKey];
    [encoder encodeObject:self.name forKey:kStressTrackNameKey];
    [encoder encodeObject:self.slots forKey:kStressTrackSlotsKey];
    [encoder encodeInt:self.stressBoxes forKey:kStressTrackStressBoxesKey];
}

- (id)initWithCoder:(NSCoder *)decoder {
    [decoder decodeIntForKey:kStressTrackVersionKey];
    NSString *name = [decoder decodeObjectForKey:kStressTrackNameKey];
    NSNumber *slots = [decoder decodeObjectForKey:kStressTrackSlotsKey];
    StressBoxes stressBoxes = [decoder decodeIntForKey:kStressTrackStressBoxesKey];
    return [self initWithName:name andSlotCount:slots andStressBoxes:stressBoxes];
}

@end
