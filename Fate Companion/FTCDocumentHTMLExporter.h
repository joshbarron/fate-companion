//
//  FTCDocumentHTMLExporter.h
//  Fate Companion
//
//  Created by Joshua Barron on 4/23/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FTCDocument.h"

@interface FTCDocumentHTMLExporter : NSObject

+ (NSString *) convertDocumentToHTML:(FTCDocument *)document;

@end
