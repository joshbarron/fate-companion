//
//  UIView+FindFirstResponder.h
//  Fate Companion
//
//  Created by Joshua Barron on 4/10/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (FindFirstResponder)
- (id)findFirstResponder;
@end
