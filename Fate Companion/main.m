//
//  main.m
//  Fate Companion
//
//  Created by Joshua Barron on 1/25/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FTCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FTCAppDelegate class]));
    }
}
