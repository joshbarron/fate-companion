//
//  FTCNote.h
//  Fate Companion
//
//  Created by Joshua Barron on 3/15/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FTCNote : NSObject <NSCoding>

@property (strong) NSDate *date;
@property (strong) NSString *text;

- (id) initWithText:(NSString *)text;

- (NSString *) title;

@end
