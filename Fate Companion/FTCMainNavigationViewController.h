//
//  FTCMainNavigation.h
//  Fate Companion
//
//  Created by Joshua Barron on 1/28/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JASidePanelController.h"

@interface FTCMainNavigationViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UITableViewCell *charactersCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *diceRollerCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *notesCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *newsCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *settingsCell;

- (void) updateCharacterCount:(int)count;

@property int selectedNavigationItemIndex;

@end
