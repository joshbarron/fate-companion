//
//  FTCDocument.m
//  Fate Companion
//
//  Created by Joshua Barron on 1/25/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCDocument.h"
#import "FTCCharacter.h"
#import "FTCCharacterMetadata.h"
#import "UIImageExtras.h"

#define METADATA_FILENAME   @"character.metadata"
#define DATA_FILENAME       @"character.data"

@interface FTCDocument ()

@property (nonatomic, strong) FTCCharacter * data;
@property (nonatomic, strong) NSFileWrapper * fileWrapper;

@end

@implementation FTCDocument
@synthesize data = _data;
@synthesize fileWrapper = _fileWrapper;
@synthesize metadata = _metadata;

#pragma mark Writing stuff to disk
- (void)encodeObject:(id<NSCoding>)object toWrappers:(NSMutableDictionary *)wrappers preferredFilename:(NSString *)preferredFilename {
    @autoreleasepool {
        NSMutableData * data = [NSMutableData data];
        NSKeyedArchiver * archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
        [archiver encodeObject:object forKey:@"data"];
        [archiver finishEncoding];
        NSFileWrapper * wrapper = [[NSFileWrapper alloc] initRegularFileWithContents:data];
        [wrappers setObject:wrapper forKey:preferredFilename];
    }
}

- (id)contentsForType:(NSString *)typeName error:(NSError *__autoreleasing *)outError {
    
    if (self.metadata == nil || self.data == nil) {
        return nil;
    }
    
    NSMutableDictionary * wrappers = [NSMutableDictionary dictionary];
    [self encodeObject:self.metadata toWrappers:wrappers preferredFilename:METADATA_FILENAME];
    [self encodeObject:self.data toWrappers:wrappers preferredFilename:DATA_FILENAME];
    NSFileWrapper * fileWrapper = [[NSFileWrapper alloc] initDirectoryWithFileWrappers:wrappers];
    
    return fileWrapper;
}

#pragma mark Reading stuff from disk

- (id)decodeObjectFromWrapperWithPreferredFilename:(NSString *)preferredFilename {
    
    NSFileWrapper * fileWrapper = [self.fileWrapper.fileWrappers objectForKey:preferredFilename];
    if (!fileWrapper) {
        NSLog(@"Unexpected error: Couldn't find %@ in file wrapper!", preferredFilename);
        return nil;
    }
    
    NSData * data = [fileWrapper regularFileContents];
    NSKeyedUnarchiver * unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    
    return [unarchiver decodeObjectForKey:@"data"];
    
}

- (FTCCharacterMetadata *)metadata {
    if (_metadata == nil) {
        if (self.fileWrapper != nil) {
            //NSLog(@"Loading metadata for %@...", self.fileURL);
            self.metadata = [self decodeObjectFromWrapperWithPreferredFilename:METADATA_FILENAME];
        } else {
            self.metadata = [[FTCCharacterMetadata alloc] init];
        }
    }
    return _metadata;
}

- (FTCCharacter *)data {
    if (_data == nil) {
        if (self.fileWrapper != nil) {
            //NSLog(@"Loading character for %@...", self.fileURL);
            self.data = [self decodeObjectFromWrapperWithPreferredFilename:DATA_FILENAME];
        } else {
            self.data = [[FTCCharacter alloc] init];
        }
    }
    return _data;
}

- (BOOL)loadFromContents:(id)contents ofType:(NSString *)typeName error:(NSError *__autoreleasing *)outError {
    
    self.fileWrapper = (NSFileWrapper *) contents;
    
    // The rest will be lazy loaded...
    self.data = nil;
    self.metadata = nil;
    
    return YES;
    
}

- (NSString *) description {
    return [[self.fileURL lastPathComponent] stringByDeletingPathExtension];
}

#pragma mark Accessors

- (UIImage *)photo {
    return self.data.photo;
}

- (void)setPhoto:(UIImage *)photo {
    
    if ([self.data.photo isEqual:photo]) return;
    
    UIImage * oldPhoto = self.data.photo;
    self.data.photo = photo;
    self.metadata.thumbnail = [self.data.photo imageByScalingAndCroppingForSize:CGSizeMake(145, 145)];
    
    [self.undoManager setActionName:@"Image Change"];
    [self.undoManager registerUndoWithTarget:self selector:@selector(setPhoto:) object:oldPhoto];
}

- (NSString *)name {
    return self.data.name;
}

- (void)setName:(NSString *)name {
    if ([self.data.name isEqualToString:name]) return;
    
    NSString * oldName = self.data.name;
    self.data.name = name;
    self.metadata.name = name;
    
    [self.undoManager setActionName:@"Name Change"];
    [self.undoManager registerUndoWithTarget:self selector:@selector(setName:) object:oldName];
}

- (NSString *)characterDescription
{
    return self.data.description;
}

- (void)setCharacterDescription:(NSString *)description
{
    if ([self.data.description isEqualToString:description]) return;
    
    NSString *oldDes = self.data.description;
    self.data.description = description;
    self.metadata.description = description;
    
    [self.undoManager setActionName:@"Description Change"];
    [self.undoManager registerUndoWithTarget:self selector:@selector(setCharacterDescription:) object:oldDes];
}

- (CharacterType) characterType
{
    return self.data.characterType;
}

- (void) setCharacterType:(NSNumber *)type
{
    CharacterType _type = (CharacterType)[type intValue];
    if (self.data.characterType == _type)
         return;
    
    CharacterType oldType = self.data.characterType;
    NSNumber *_oldType = [NSNumber numberWithInt:oldType];
    self.data.characterType = _type;
    [self.undoManager setActionName:@"Character Type Change"];
    [self.undoManager registerUndoWithTarget:self selector:@selector(setCharacterType:) object:_oldType];
}

#pragma mark Fate Points and Refresh
- (NSNumber *)fatePoints
{
    return self.data.fatePoints;
}

- (void) setFatePoints:(NSNumber *)value
{
    if ([self.data.fatePoints isEqual:value])
        return;
    
    if ([value intValue] <= 0)
    {
        value = [NSNumber numberWithInt:0];
    }
    
    NSNumber *oldValue = self.data.fatePoints;
    self.data.fatePoints = value;
    
    [self.undoManager setActionName:@"Fate Points Change"];
    [self.undoManager registerUndoWithTarget:self selector:@selector(setFatePoints:) object:oldValue];
}

- (NSNumber *)refresh
{
    return self.data.refresh;
}

- (void) setRefresh:(NSNumber *)value
{
    if ([self.data.refresh isEqual:value])
        return;
    
    if ([value intValue] <= 0)
    {
        value = [NSNumber numberWithInt:0];
    }
    
    NSNumber *oldValue = self.data.refresh;
    self.data.refresh = value;
    
    [self.undoManager setActionName:@"Refresh Change"];
    [self.undoManager registerUndoWithTarget:self selector:@selector(setRefresh:) object:oldValue];
}

#pragma mark Aspects
- (FTCAspect *)highConcept
{
    return self.data.highConcept;
}

- (void)setHighConcept:(NSString *)aspectText
{
    NSString *oldText = self.data.highConcept.aspectText;
    self.data.highConcept.aspectText = aspectText;
    
    [self.undoManager setActionName:@"High Concept Change"];
    [self.undoManager registerUndoWithTarget:self selector:@selector(setHighConcept:) object:oldText];
}

- (FTCAspect *)trouble
{
    return self.data.trouble;
}

- (void)setTrouble:(NSString *)aspectText
{
    NSString *oldText = self.data.trouble.aspectText;
    self.data.trouble.aspectText = aspectText;
    
    [self.undoManager setActionName:@"Trouble Change"];
    [self.undoManager registerUndoWithTarget:self selector:@selector(setTrouble:) object:oldText];
}

- (NSArray *)aspects
{
    return self.data.aspects;
}

- (FTCAspect *)addAspectWithText:(NSString *)aspectText
{
    FTCAspect *newAspect = [[FTCAspect alloc] initWithText:aspectText];
    
    [self.data.aspects addObject:newAspect];
    
    [self.undoManager setActionName:@"Aspect Add"];
    [self.undoManager registerUndoWithTarget:self selector:@selector(removeAspect:) object:newAspect];
    
    return newAspect;
}

- (void)updateAspect:(FTCAspect *)aspect withText:(NSString *)aspectText
{
    if (![self.data.aspects containsObject:aspect])
    {
        NSLog(@"Aspect could not be found!");
        return;
    }
    
    NSString *oldText = aspect.aspectText;
    
    aspect.aspectText = aspectText;
    
    [self.undoManager setActionName:@"Aspect Change"];
    [[self.undoManager prepareWithInvocationTarget:self] updateAspect:aspect withText:oldText];
}

- (void)removeAspect:(FTCAspect *)aspect
{
    if (![self.data.aspects containsObject:aspect])
    {
        NSLog(@"Aspect could not be found!");
        return;
    }
    
    [self.data.aspects removeObject:aspect];
    [self.undoManager setActionName:@"Aspect Remove"];
    [self.undoManager registerUndoWithTarget:self selector:@selector(addAspectWithText:) object:aspect.aspectText];
}

#pragma mark Approaches

- (NSArray *)approaches
{
    return self.data.approaches;
}

- (NSArray *)approachesSortedByValue
{
    return [self.data.approaches sortedArrayUsingComparator:^(id a, id b) {
        NSNumber *first = ((FTCApproach *)a).value;
        NSNumber *second = ((FTCApproach *)b).value;
        
        if ([first integerValue] > [second integerValue])
        {
            return (NSComparisonResult)NSOrderedAscending;
        }
        else if ([first integerValue] < [second integerValue])
        {
            return (NSComparisonResult)NSOrderedDescending;
        }
        
        return (NSComparisonResult)NSOrderedSame;
    }];
}

- (NSArray *)approachesSortedAlphabetically
{
    return [self.data.approaches sortedArrayUsingComparator:^(id a, id b) {
        NSString *first = ((FTCApproach *)a).name;
        NSString *second = ((FTCApproach *)b).name;
        
        return [first compare:second];
    }];
}

- (FTCApproach *)addApproachWithName:(NSString *)name andValue:(NSNumber *)value isCustom:(BOOL)isCustom
{
    FTCApproach *approach = [[FTCApproach alloc] initWithName:name andValue:value isCustom:isCustom];
    [self addApproach:approach];
    return approach;
}

- (void) addApproach:(FTCApproach *)approach
{
    [self.data.approaches addObject:approach];
    
    [self.undoManager setActionName:@"Approach Add"];
    [self.undoManager registerUndoWithTarget:self selector:@selector(removeApproach:) object:approach];
}

- (void) removeApproach:(FTCApproach *)approach
{
    if (![self.data.approaches containsObject:approach])
    {
        NSLog(@"Approach could not be found!");
        return;
    }
    
    [self.data.approaches removeObject:approach];
    
    [self.undoManager setActionName:@"Approach Remove"];
    [self.undoManager registerUndoWithTarget:self selector:@selector(addApproach:) object:approach];
}

- (void) changeApproachName:(FTCApproach *)approach toName:(NSString *)newName
{
    if (![self.data.approaches containsObject:approach])
    {
        NSLog(@"Approach could not be found!");
        return;
    }
    
    NSString *oldValue = approach.name;
    approach.name = newName;
    [self.undoManager setActionName:@"Approach Name Change"];
    [[self.undoManager prepareWithInvocationTarget:self] changeApproachName:approach toName:oldValue];
}

- (void) changeApproachValue:(FTCApproach *)approach toValue:(NSNumber *)value
{
    if (![self.data.approaches containsObject:approach])
    {
        NSLog(@"Approach could not be found!");
        return;
    }
    
    NSNumber *oldValue = approach.value;
    approach.value = value;
    [self.undoManager setActionName:@"Approach Value Change"];
    [[self.undoManager prepareWithInvocationTarget:self] changeApproachValue:approach toValue:oldValue];
}

#pragma mark Skills

- (NSArray *)skills
{
    return self.data.skills;
}

- (NSArray *)skillsSortedByValue
{
    return [self.data.skills sortedArrayUsingComparator:^(id a, id b) {
        NSNumber *first = ((FTCSkill *)a).value;
        NSNumber *second = ((FTCSkill *)b).value;
        
        if ([first integerValue] > [second integerValue])
        {
            return (NSComparisonResult)NSOrderedAscending;
        }
        else if ([first integerValue] < [second integerValue])
        {
            return (NSComparisonResult)NSOrderedDescending;
        }
        
        return (NSComparisonResult)NSOrderedSame;
    }];
}

- (NSArray *)skillsSortedAlphabetically
{
    return [self.data.skills sortedArrayUsingComparator:^(id a, id b) {
        NSString *first = ((FTCSkill *)a).name;
        NSString *second = ((FTCSkill *)b).name;
        
        return [first compare:second];
    }];
}

- (FTCSkill *)addSkillWithName:(NSString *)name andValue:(NSNumber *)value isCustom:(BOOL)isCustom andActions:(SkillActions)actions
{
    //create the skill
    FTCSkill *skill = [[FTCSkill alloc] initWithName:name andValue:value isCustom:isCustom andActions:actions];
    [self addSkill:skill];
    return skill;
}

- (void)addSkill:(FTCSkill *)skill
{
    [self.data.skills addObject:skill];
    
    [self.undoManager setActionName:@"Skill Add"];
    [self.undoManager registerUndoWithTarget:self selector:@selector(removeSkill:) object:skill];
}

- (FTCSkill *)findSkillWithName:(NSString *)name
{
    //find the skill
    NSUInteger indexOfSkill = [self.data.skills indexOfObjectPassingTest:
                               ^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                                   return [((FTCSkill *)obj).name isEqualToString:name];
                               }];
    
    if (indexOfSkill == NSNotFound)
    {
        return nil;
    }
    
    return [self.data.skills objectAtIndex:indexOfSkill];
}

- (void)removeSkill:(FTCSkill *)skill
{
    if (![self.data.skills containsObject:skill])
    {
        NSLog(@"Skill could not be found!");
        return;
    }
    
    [self.data.skills removeObject:skill];
    
    [self.undoManager setActionName:@"Skill Remove"];
    [self.undoManager registerUndoWithTarget:self selector:@selector(addSkill:) object:skill];
}

- (void)changeSkillWithName:(NSString *)name toNewName:(NSString *)newName
{
    FTCSkill *skill = [self findSkillWithName:name];
    if (skill == nil)
    {
        NSLog(@"Skill could not be found!");
        return;
    }
    
    skill.name = newName;
    [self.undoManager setActionName:@"Skill Name Change"];
    [[self.undoManager prepareWithInvocationTarget:self] changeSkillWithName:newName toNewName:name];
}

- (void)changeSkillWithName:(NSString *)name toValue:(NSNumber *) value
{
    FTCSkill *skill = [self findSkillWithName:name];
    if (skill == nil)
    {
        NSLog(@"Skill could not be found!");
        return;
    }
    NSNumber *oldValue = skill.value;
    skill.value = value;
    [self.undoManager setActionName:@"Skill Value Change"];
    [[self.undoManager prepareWithInvocationTarget:self] changeSkillWithName:name toValue:oldValue];
}

- (void)changeSkillWithName:(NSString *)name toActions:(NSNumber *) actions
{
    FTCSkill *skill = [self findSkillWithName:name];
    if (skill == nil)
    {
        NSLog(@"Skill could not be found!");
        return;
    }
    SkillActions oldActions = skill.actions;
    skill.actions = [actions intValue];
    [self.undoManager setActionName:@"Skill Actions Change"];
    [[self.undoManager prepareWithInvocationTarget:self] changeSkillWithName:name toActions:[NSNumber numberWithInt:oldActions]];
}

#pragma mark Stunts
- (NSArray *)stunts
{
    return self.data.stunts;
}

- (FTCStunt *)addStuntWithName:(NSString *)name andStuntText:(NSString *)stuntText
{
    //create the stunt
    FTCStunt *stunt = [[FTCStunt alloc] initWithName:name andStuntText:stuntText];
    [self addStunt:stunt];
    return stunt;
}

- (void)addStunt:(FTCStunt *)stunt
{
    [self.data.stunts addObject:stunt];
    
    [self.undoManager setActionName:@"Stunt Add"];
    [self.undoManager registerUndoWithTarget:self selector:@selector(removeStunt:) object:stunt];
}


- (void)changeStuntName:(FTCStunt *)stunt toName:(NSString *)newName
{
    if (![self.data.stunts containsObject:stunt])
    {
        NSLog(@"Stunt could not be found!");
        return;
    }
    
    NSString *oldName = stunt.name;
    if ([oldName isEqualToString:newName])
        return;
    
    stunt.name = newName;
    
    [self.undoManager setActionName:@"Stunt Name Change"];
    [[self.undoManager prepareWithInvocationTarget:self] changeStuntName:stunt toName:oldName];
}

- (void)changeStuntText:(FTCStunt *)stunt toText:(NSString *)newText
{
    if (![self.data.stunts containsObject:stunt])
    {
        NSLog(@"Stunt could not be found!");
        return;
    }
    
    NSString *oldText = stunt.stuntText;
    if ([oldText isEqualToString:newText])
        return;
    
    stunt.stuntText = newText;
    
    [self.undoManager setActionName:@"Stunt Text Change"];
    [[self.undoManager prepareWithInvocationTarget:self] changeStuntText:stunt toText:oldText];
}

- (void)removeStunt:(FTCStunt *)stunt
{
    if (![self.data.stunts containsObject:stunt])
    {
        NSLog(@"Stunt could not be found!");
        return;
    }
    
    [self.data.stunts removeObject:stunt];
    
    [self.undoManager setActionName:@"Stunt Remove"];
    [self.undoManager registerUndoWithTarget:self selector:@selector(addStunt:) object:stunt];
}

#pragma mark Extras
- (NSArray *)extras
{
    return self.data.extras;
}

- (FTCExtra *)addExtraWithName:(NSString *)name
{
    //create the extra
    FTCExtra *extra = [[FTCExtra alloc] initWithName:name];
    [self addExtra:extra];
    return extra;
}

- (void)addExtra:(FTCExtra *)extra
{
    [self.data.extras addObject:extra];
    
    [self.undoManager setActionName:@"Extra Add"];
    [self.undoManager registerUndoWithTarget:self selector:@selector(removeExtra:) object:extra];
}


- (void)changeExtraName:(FTCExtra *)extra toName:(NSString *)newName
{
    if (![self.data.extras containsObject:extra])
    {
        NSLog(@"Extra could not be found!");
        return;
    }
    
    NSString *oldName = extra.name;
    if ([oldName isEqualToString:newName])
        return;
    
    extra.name = newName;
    
    [self.undoManager setActionName:@"Extra Name Change"];
    [[self.undoManager prepareWithInvocationTarget:self] changeExtraName:extra toName:oldName];
}

- (void)removeExtra:(FTCExtra *)extra
{
    if (![self.data.extras containsObject:extra])
    {
        NSLog(@"Extra could not be found!");
        return;
    }
    
    [self.data.extras removeObject:extra];
    
    [self.undoManager setActionName:@"Extra Remove"];
    [self.undoManager registerUndoWithTarget:self selector:@selector(addExtra:) object:extra];
}

#pragma mark Stress Tracks
- (NSArray *)stressTracks
{
    return self.data.stressTracks;
}

- (NSArray *)stressTracksSortedByName
{
    return [self.data.stressTracks sortedArrayUsingComparator:^(id a, id b) {
        NSString *first = ((FTCStressTrack *)a).name;
        NSString *second = ((FTCStressTrack *)b).name;
        
        return [first compare:second];
    }];
}

- (FTCStressTrack *)addStressTrackWithName:(NSString *)name andSlots:(NSNumber *)slots
{
    FTCStressTrack *stressTrack = [[FTCStressTrack alloc] initWithName:name andSlotCount:slots andStressBoxes:0];
    [self addStressTrack:stressTrack];
    return stressTrack;
}

- (void) addStressTrack:(FTCStressTrack *)stressTrack
{
    [self.data.stressTracks addObject:stressTrack];
    
    [self.undoManager setActionName:@"Stress Track Add"];
    [self.undoManager registerUndoWithTarget:self selector:@selector(removeStressTrack:) object:stressTrack];
}

- (void) removeStressTrack:(FTCStressTrack *)stressTrack
{
    if (![self.data.stressTracks containsObject:stressTrack])
    {
        NSLog(@"Stress Track could not be found!");
        return;
    }
    
    [self.data.stressTracks removeObject:stressTrack];
    
    [self.undoManager setActionName:@"Stress Track Remove"];
    [self.undoManager registerUndoWithTarget:self selector:@selector(addStressTrack:) object:stressTrack];
}

- (void)updateStressTrack:(FTCStressTrack *)stressTrack withName:(NSString *)newName andSlots:(NSNumber *)slots
{
    if (![self.data.stressTracks containsObject:stressTrack])
    {
        NSLog(@"Stress Track could not be found!");
        return;
    }
    
    NSString *oldName = stressTrack.name;
    NSNumber *oldSlots = stressTrack.slots;
    
    if (![newName isEqualToString:oldName])
    {
        stressTrack.name = newName;
    }
    if (![slots isEqualToNumber:oldSlots])
    {
        stressTrack.slots = slots;
        //Reset the boxes
        stressTrack.stressBoxes = 0;
    }
    
    [self.undoManager setActionName:@"Stress Track Change"];
    [[self.undoManager prepareWithInvocationTarget:self] updateStressTrack:stressTrack withName:oldName andSlots:oldSlots];

}

- (void)changeStressTrackBoxes:(FTCStressTrack *)stressTrack toStressBoxes:(NSNumber *)stressBoxes
{
    if (![self.data.stressTracks containsObject:stressTrack])
    {
        NSLog(@"Stress Track could not be found!");
        return;
    }
    
    StressBoxes oldStressBoxes = stressTrack.stressBoxes;
    stressTrack.stressBoxes = [stressBoxes intValue];
    [self.undoManager setActionName:@"Stress Track Boxes Change"];
    [[self.undoManager prepareWithInvocationTarget:self] changeStressTrackBoxes:stressTrack toStressBoxes:[NSNumber numberWithInt:oldStressBoxes]];
}

#pragma mark Consequences

- (NSArray *)consequences
{
    return self.data.consequences;
}

- (NSArray *)consequencesSortedByType
{
    return [self.data.consequences sortedArrayUsingComparator:^(id a, id b) {
        FTCConsequence *_a = (FTCConsequence *)a;
        FTCConsequence *_b = (FTCConsequence *)b;
        
        return [[NSNumber numberWithInt:_a.type] compare:[NSNumber numberWithInt:_b.type]];
    }];
}

- (FTCConsequence *) addConsequenceWithType:(ConsequenceType)type isOptional:(BOOL)isOptional
{
    FTCConsequence *consequence = [[FTCConsequence alloc] initWithType:type andText:nil isOptional:isOptional];
    [self addConsequence:consequence];
    return consequence;
}

- (void) addConsequence:(FTCConsequence *)consequence
{
    [self.data.consequences addObject:consequence];
    
    [self.undoManager setActionName:@"Consequence Add"];
    [self.undoManager registerUndoWithTarget:self selector:@selector(removeConsequence:) object:consequence];
}

- (void) removeConsequence:(FTCConsequence *)consequence
{
    if (![self.data.consequences containsObject:consequence])
    {
        NSLog(@"Consequence could not be found!");
        return;
    }
    
    [self.data.consequences removeObject:consequence];
    
    [self.undoManager setActionName:@"Consequence Remove"];
    [self.undoManager registerUndoWithTarget:self selector:@selector(addConsequence:) object:consequence];
}

- (void) updateConsequence:(FTCConsequence *)consequence withText:(NSString *)text
{
    if (![self.data.consequences containsObject:consequence])
    {
        NSLog(@"Consequence could not be found!");
        return;
    }
    
    if ([consequence.text isEqualToString:text])
        return;
    
    NSString *oldText = consequence.text;
    consequence.text = text;
    
    [self.undoManager setActionName:@"Consequence Change"];
    [[self.undoManager prepareWithInvocationTarget:self] updateConsequence:consequence withText:oldText];
}

@end
