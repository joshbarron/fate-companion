//
//  FTCDocumentHTMLExporter.m
//  Fate Companion
//
//  Created by Joshua Barron on 4/23/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCDocumentHTMLExporter.h"
#import "FTCSkillValueTranslator.h"

@implementation FTCDocumentHTMLExporter

+ (NSString *)convertDocumentToHTML:(FTCDocument *)document
{
    NSString *head = [FTCDocumentHTMLExporter createHTMLHeader];
    NSString *body = [FTCDocumentHTMLExporter createHTMLBody:document];
    NSString *page = [NSString stringWithFormat:@"<!DOCTYPE html><html>%@%@</html>", head, body];
    
    return page;
}

+ (NSString *)inTag:(NSString *)tag text:(NSString *)text
{
    return [NSString stringWithFormat:@"<%@>%@</%@>", tag, text, tag];
}

+ (NSString *)inTag:(NSString *)tag withExtra:(NSString *)extra text:(NSString *)text
{
    return [NSString stringWithFormat:@"<%@ %@>%@</%@>", tag, extra, text, tag];
}

+ (NSString *)createHTMLHeader
{
    NSString *path = [[NSBundle mainBundle] pathForResource: @"bootstrapmin" ofType: @"css"];
    NSString *css = [NSString stringWithContentsOfFile:path usedEncoding:nil error:nil];
    
    NSString *cssTag = [FTCDocumentHTMLExporter inTag:@"style" text:css];
    
    return [FTCDocumentHTMLExporter inTag:@"head" text:cssTag];
}

+ (NSString *)createHTMLBody:(FTCDocument *)doc
{
    NSMutableString *body = [NSMutableString new];
    NSString *pageHeader = [FTCDocumentHTMLExporter generatePageHeaderWithName:doc.name andDescription:doc.characterDescription andImage:doc.photo andType:doc.characterType];
    [body appendString:pageHeader];
    
    NSString *fp = [FTCDocumentHTMLExporter generateSectionHeader:@"Fate Points and Refresh" withBody:[FTCDocumentHTMLExporter generateFP:[doc.fatePoints intValue] andRefresh:[doc.refresh intValue]]];
    [body appendString:fp];
    
    NSString *aspects = [FTCDocumentHTMLExporter generateSectionHeader:@"Aspects" withBody:[FTCDocumentHTMLExporter generateAspects:doc]];
    [body appendString:aspects];
    
    if (doc.characterType == FateCoreCharacter) {
        if (doc.skills.count) {
        NSString *skills = [FTCDocumentHTMLExporter generateSectionHeader:@"Skills" withBody:[FTCDocumentHTMLExporter generateSkills:doc]];
        [body appendString:skills];
        }
    }
    else {
        if (doc.approaches.count) {
        NSString *approaches = [FTCDocumentHTMLExporter generateSectionHeader:@"Approaches" withBody:[FTCDocumentHTMLExporter generateApproaches:doc]];
        [body appendString:approaches];
        }
    }
    
    if (doc.stunts.count) {
        NSString *stunts = [FTCDocumentHTMLExporter generateSectionHeader:@"Stunts" withBody:[FTCDocumentHTMLExporter generateStunts:doc]];
        [body appendString:stunts];
    }
    
    if (doc.characterType == FateCoreCharacter && doc.extras.count) {
        NSString *extras = [FTCDocumentHTMLExporter generateSectionHeader:@"Extras" withBody:[FTCDocumentHTMLExporter generateExtras:doc]];
        [body appendString:extras];
    }
    
    if (doc.stressTracks.count) {
        NSString *st = [FTCDocumentHTMLExporter generateSectionHeader:@"Stress" withBody:[FTCDocumentHTMLExporter generateStress:doc]];
        [body appendString:st];
    }
    
    if (doc.consequences.count) {
        NSString *consequences = [FTCDocumentHTMLExporter generateSectionHeader:@"Consequences" withBody:[FTCDocumentHTMLExporter generateConsequences:doc]];
        [body appendString:consequences];
    }
    
    return [FTCDocumentHTMLExporter inTag:@"body" text:body];
}

+ (NSString *)generatePageHeaderWithName:(NSString *)characterName andDescription:(NSString *)description andImage:(UIImage *)image andType:(CharacterType)type
{
    NSString *ctype = type == FateCoreCharacter ? @"Fate Core" : @"Fate Accelerated";
    NSString *headline = [NSString stringWithFormat:@"%@%@", characterName, [FTCDocumentHTMLExporter inTag:@"p" withExtra:@"class='lead text-muted'" text:ctype]];
    
    NSString *mediaHeading = [FTCDocumentHTMLExporter inTag:@"h1" withExtra:@"class='media-heading'" text:headline];
    
    NSString *mediaBody = [FTCDocumentHTMLExporter inTag:@"div" withExtra:@"class='media-body'" text:[NSString stringWithFormat:@"%@%@", mediaHeading, description]];
    
    
    NSString *encodedImage = [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    NSString *img = [NSString stringWithFormat:@"<img src='data:image/png;base64,%@' class='media-image' style='display:none;width:50px;height:50px;' />", encodedImage];
    
    NSString *imgSection = [FTCDocumentHTMLExporter inTag:@"div" withExtra:@"class='pull-left" text:img];
    
    NSString *media = [FTCDocumentHTMLExporter inTag:@"div" withExtra:@"class='media'" text:[NSString stringWithFormat:@"%@%@", /*imgSection*/@"", mediaBody]];
    
    NSString *pheader = [FTCDocumentHTMLExporter inTag:@"div" withExtra:@"class='page-header'" text:media];
    
    return pheader;
}

+ (NSString *)generateSectionHeader:(NSString *)title withBody:(NSString *)body
{
    NSString *header = [FTCDocumentHTMLExporter inTag:@"h2" text:title];
    
    return [NSString stringWithFormat:@"%@%@", header, body];
}

+ (NSString *)generateFP:(int)fatePoints andRefresh:(int)refresh
{
    NSString *fp = [FTCDocumentHTMLExporter inTag:@"p" text:[NSString stringWithFormat:@"<strong>Fate Points:</strong> %d", fatePoints]];
    NSString *ref = [FTCDocumentHTMLExporter inTag:@"p" text:[NSString stringWithFormat:@"<strong>Refresh:</strong> %d", refresh]];
    return [NSString stringWithFormat:@"%@%@", fp, ref];
}

+ (NSString *)generateAspects:(FTCDocument *)doc
{
    NSMutableString *aspects = [NSMutableString new];
    [aspects appendString:[FTCDocumentHTMLExporter generateAspectWithTitle:@"High Concept" andText:doc.highConcept.aspectText]];
    [aspects appendString:[FTCDocumentHTMLExporter generateAspectWithTitle:@"Trouble" andText:doc.trouble.aspectText]];
    
    for (FTCAspect *aspect in doc.aspects) {
        [aspects appendString:[FTCDocumentHTMLExporter generateAspectWithTitle:@"Aspect" andText:aspect.aspectText]];
    }
    
    return aspects;
}

+ (NSString *)generateAspectWithTitle:(NSString *)title andText:(NSString *)text
{
    return [NSString stringWithFormat:@"<h3>%@</h3><pre>%@</pre>", title, text];
}

+ (NSString *)generateSkills:(FTCDocument *)doc
{
    NSMutableString *skills = [NSMutableString new];
    FTCSkillValueTranslator *translator = [[FTCSkillValueTranslator alloc] init];
    
    for (FTCSkill *skill in doc.skillsSortedByValue) {
        [skills appendString:[NSString stringWithFormat:@"<p><strong>%@:</strong> %@</p>", skill.name, [translator nameAndSkillValue:[skill.value intValue]]]];
    }
    
    return skills;
}

+ (NSString *)generateApproaches:(FTCDocument *)doc
{
    NSMutableString *approaches = [NSMutableString new];
    FTCSkillValueTranslator *translator = [[FTCSkillValueTranslator alloc] init];
    
    for (FTCApproach *approach in doc.approachesSortedByValue) {
        [approaches appendString:[NSString stringWithFormat:@"<p><strong>%@:</strong> %@</p>", approach.name, [translator nameAndSkillValue:[approach.value intValue]]]];
    }
    
    return approaches;
}

+ (NSString *)generateStunts:(FTCDocument *)doc
{
    NSMutableString *stunts = [NSMutableString new];
    
    for (FTCStunt *stunt in doc.stunts) {
        [stunts appendString:[NSString stringWithFormat:@"<h3>%@</h3><pre>%@</pre>", stunt.name, stunt.stuntText]];
    }
    
    return stunts;
}

+ (NSString *)generateExtras:(FTCDocument *)doc
{
    NSMutableString *extras = [NSMutableString new];
    
    for (FTCExtra *extra in doc.extras) {
        [extras appendString:[NSString stringWithFormat:@"<p><i>%@</i></p>", extra.name]];
    }
    
    return extras;
}

+ (NSString *)generateStress:(FTCDocument *)doc
{
    NSMutableString *stressTracks = [NSMutableString new];
    
    NSString *box = [NSString stringWithFormat:@"<span style='font-size:16px;padding-left:15px;border:2px solid; margin-right:10px; overflow:hidden; float:left'>&nbsp;</span>"];
    
    for (FTCStressTrack *stressTrack in doc.stressTracksSortedByName) {
        [stressTracks appendString:[NSString stringWithFormat:@"<h3 style='clear:both'>%@</h3>", stressTrack.name]];
        for(int i = 0; i < [stressTrack.slots intValue]; i++) {
            [stressTracks appendString:box];
        }
    }
    [stressTracks appendString:@"<div style='clear:both;margin-top:10px;'></div>"];
    
    return stressTracks;
}

+ (NSString *)generateConsequences:(FTCDocument *)doc
{
    NSMutableString *consequences = [NSMutableString new];
    
    for (FTCConsequence *consequence in doc.consequencesSortedByType) {
        [consequences appendString:[NSString stringWithFormat:@"<p><strong>%@:</strong> %@</p>", [consequence labelForType], consequence.text]];
    }
    
    return consequences;
}


@end
