//
//  FTCMainNavigation.m
//  Fate Companion
//
//  Created by Joshua Barron on 1/28/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCMainNavigationViewController.h"
#import "UIViewController+JASidePanel.h"
#import "FTCLayoutHelper.h"
#import "FTCCharacterDetailViewController.h"

@implementation FTCMainNavigationViewController
@synthesize charactersCell = _charactersCell;
@synthesize diceRollerCell = _diceRollerCell;
@synthesize notesCell = _notesCell;
@synthesize newsCell = _newsCell;
@synthesize settingsCell = _settingsCell;

@synthesize selectedNavigationItemIndex = _selectedNavigationItemIndex;

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //if (self.selectedNavigationItemIndex == indexPath.item)
     //   return;
    
    self.selectedNavigationItemIndex = indexPath.item;
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    //todo: self.splitviewcontroller notify right side of pending doom, dismiss popover menu
    UINavigationController *target;
    if (cell == self.charactersCell) {
        target = [self.storyboard instantiateViewControllerWithIdentifier:@"characterPanel"];
    }
    else if (cell == self.diceRollerCell) {
        target = [self.storyboard instantiateViewControllerWithIdentifier:@"dicePanel"];
    }
    else if (cell == self.notesCell) {
        target = [self.storyboard instantiateViewControllerWithIdentifier:@"notesPanel"];
    }
    else if (cell == self.newsCell) {
        target = [self.storyboard instantiateViewControllerWithIdentifier:@"newsPanel"];
    }
    else if (cell == self.settingsCell) {
        target = [self.storyboard instantiateViewControllerWithIdentifier:@"settingsPanel"];
    }
    else
    {
        NSLog(@"Invalid nav item selected.");
        return;
    }
    if ([FTCLayoutHelper deviceIsIPhone]) {
        [self.sidePanelController setCenterPanel:target];
        [self.sidePanelController showCenterPanelAnimated:YES];
    }
    else {
        FTCSplitViewController *splitViewController = [FTCLayoutHelper splitViewController:self];
        
        UINavigationController *nav = (UINavigationController *)splitViewController.detailViewController;
        
        if (nav != nil) {
            UIViewController *visible = [nav visibleViewController];
            
            BOOL isPopoverController = [visible isKindOfClass:[FTCSkillEntryViewController class]] || [visible isKindOfClass:[FTCApproachEntryViewController class]] || [visible isKindOfClass:[FTCStressTrackEntryViewController class]];
            
            if (isPopoverController)
                return; //don't change menus
        }
        
        [splitViewController changeDetailViewController:target];
    }
}

- (void) updateCharacterCount:(int)count
{
    NSString *charactersSubtitle = count == 1 ? @"1 saved character" : [NSString stringWithFormat:@"%d saved characters", count];
    self.charactersCell.detailTextLabel.text = charactersSubtitle;
    [self.view layoutSubviews];
}

- (void) viewDidLoad
{
    self.selectedNavigationItemIndex = 0;
}

@end
