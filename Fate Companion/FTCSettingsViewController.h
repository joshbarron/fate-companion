//
//  FTCSettingsViewController.h
//  Fate Companion
//
//  Created by Joshua Barron on 1/28/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "MGScrollView.h"

@interface FTCSettingsViewController : UIViewController<UISplitViewControllerDelegate, MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet MGScrollView *scroller;

@end
