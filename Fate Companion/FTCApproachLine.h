//
//  FTCApproachLine.h
//  Fate Companion
//
//  Created by Joshua Barron on 4/14/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "MGLine.h"
#import "FTCApproach.h"

@class FTCApproachLine;

@protocol FTCApproachLineDelegate

- (void) approachLineWasRemoved:(FTCApproachLine *)approachLine;
- (void) rollDiceForApproachLine:(FTCApproachLine *)approachLine;
- (void) approachLineWasTapped:(FTCApproachLine *)approachLine;

@end

@interface FTCApproachLine : MGLine

+ (FTCApproachLine *)approachLineWithApproach:(FTCApproach *)approach delegate:(id<FTCApproachLineDelegate>)delegate;

@property (weak) id<FTCApproachLineDelegate> delegate;
@property (weak) FTCApproach *approach;

@property double lineWidth;

@end
