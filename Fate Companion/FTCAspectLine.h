//
//  FTCAspectLine.h
//  Fate Companion
//
//  Created by Joshua Barron on 2/15/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "MGLine.h"
#import "FTCAspect.h"
#import "JVFloatLabeledTextView.h"

@class FTCAspectLine;

@protocol FTCAspectLineDelegate

- (void) aspectLineWasRemoved:(FTCAspectLine *)aspectLine;
- (void) aspectLineChanged:(FTCAspectLine *)aspectLine withText:(NSString *)text;
- (void) aspectLineHeightChanged:(FTCAspectLine *)aspectLine;

@end

@interface FTCAspectLine : MGLine <UITextViewDelegate>

+ (FTCAspectLine *)aspectLineWithAspect:(FTCAspect *)aspect placeholder:(NSString *)placeholder tag:(int)tag mandatory:(bool)isMandatory delegate:(id<FTCAspectLineDelegate>)delegate;

@property bool isMandatory;
@property (weak) id<FTCAspectLineDelegate> delegate;
@property (strong) JVFloatLabeledTextView *aspectField;
@property (weak) FTCAspect *aspect;

@property double lineWidth;

@end
