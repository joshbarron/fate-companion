//
//  FTCConsequence.h
//  Fate Companion
//
//  Created by Joshua Barron on 2/27/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, ConsequenceType) {
    ConsequenceMild = 2,
    ConsequenceModerate = 4,
    ConsequenceSevere = 8
};

@interface FTCConsequence : NSObject<NSCoding>

- (id) initWithType:(ConsequenceType)type andText:(NSString *)text isOptional:(BOOL)optional;
- (NSString *) labelForType;

@property (strong) NSString *text;
@property ConsequenceType type;
@property BOOL isOptional;

@end
