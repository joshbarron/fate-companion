//
//  FTCStandardSkillsHelper.h
//  Fate Companion
//
//  Created by Joshua Barron on 2/21/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "FTCSkillDefinition.h"
#import "FTCDocument.h"

@interface FTCStandardSkillsHelper : NSObject

@property NSArray * standardSkills;

- (NSArray *) undesignatedStandardSkillsForCharacter:(FTCDocument *)characterDoc;

@end
