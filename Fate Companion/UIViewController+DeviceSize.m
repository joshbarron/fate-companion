//
//  UIViewController+DeviceSize.m
//  Fate Companion
//
//  Created by Joshua Barron on 1/29/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "UIViewController+DeviceSize.h"

@implementation UIViewController (DeviceSize)

- (bool) isPhone {
    // iPhone or iPad?
    UIDevice *device = UIDevice.currentDevice;
    return device.userInterfaceIdiom == UIUserInterfaceIdiomPhone;
}

- (CGSize) getGridSize:(bool)isPortrait {
    bool phone = [self isPhone];
    
    // grid size
    if (phone) {
        return isPortrait ? IPHONE_PORTRAIT_GRID : IPHONE_LANDSCAPE_GRID;
    }
    else {
        return isPortrait ? IPAD_PORTRAIT_GRID : IPAD_LANDSCAPE_GRID;
    }
}

@end
