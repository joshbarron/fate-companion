//
//  FTCSettingsViewController.m
//  Fate Companion
//
//  Created by Joshua Barron on 1/28/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCSettingsViewController.h"
#import "MGBox.h"
#import "MGLine.h"
#import "FTCUserSettings.h"
#import "UIControl+BlocksKit.h"
#import "FTCGridBox.h"
#import "FTCLayoutHelper.h"
#import "FTCDetailSectionBox.h"
#import "JASidePanelController.h"
#import "SIAlertView.h"
#import "Appirater.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface FTCSettingsViewController () {
    FTCDetailSectionBox *settingsGrid, *infoGrid, *copyrightGrid;
    FTCGridBox *_masterBox;
}
@end

@implementation FTCSettingsViewController
@synthesize scroller = _scroller;

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if ([FTCLayoutHelper deviceIsIPad])
    {
        [_masterBox setSize:(CGSize){[FTCLayoutHelper getGridWidthForOrientation:toInterfaceOrientation], 0}];
        
        [self resizeSettingsGrid:toInterfaceOrientation];
        [self resizeInfoGrid:toInterfaceOrientation];
        [self resizeCopyrightGrid:toInterfaceOrientation];
        
        [self.scroller layoutWithSpeed:duration completion:nil];
    }
}



- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if ([FTCLayoutHelper deviceIsIPad])
    {
        [self.scroller layout];
    }
}

#pragma mark UISplitViewControllerDelegate
-(void)splitViewController:(UISplitViewController *)svc
    willHideViewController:(UIViewController *)aViewController
         withBarButtonItem:(UIBarButtonItem *)barButtonItem
      forPopoverController:(UIPopoverController *)pc
{
    NSLog(@"Will hide left side");
    FTCAppDelegate *appDelegate = [FTCLayoutHelper appDelegate];
    barButtonItem.image = [JASidePanelController defaultImage];
    
    appDelegate.menuPopover = pc;
    appDelegate.menuNavItem = barButtonItem;
    
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
}

-(void)splitViewController:(UISplitViewController *)svc
    willShowViewController:(UIViewController *)aViewController
 invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    NSLog(@"Will show left side");
    FTCAppDelegate *appDelegate = [FTCLayoutHelper appDelegate];
    
    appDelegate.menuPopover = nil;
    appDelegate.menuNavItem = nil;
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [FTCLayoutHelper setupMenuNav:self];
	_masterBox = [FTCGridBox boxWithSize:(CGSize){[FTCLayoutHelper getGridWidth], 0}];
    _masterBox.contentLayoutMode = MGLayoutGridStyle;
    _masterBox.leftMargin = 8;
    _masterBox.topMargin = 8;
    
    self.scroller.contentLayoutMode = MGLayoutGridStyle;
    self.scroller.keyboardMargin = 10;
    //self.scroller.delegate = self;
    self.scroller.canCancelContentTouches = NO;
    
    [self.scroller.boxes addObject:_masterBox];

    [self loadSettingsSection];
    [self loadInfoSection];
    [self loadCopyrightSection];
    
    [self.scroller layoutWithSpeed:0.3 completion:nil];
    [self.scroller scrollToView:settingsGrid withMargin:8];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self willAnimateRotationToInterfaceOrientation:self.interfaceOrientation
                                           duration:1];
    [self didRotateFromInterfaceOrientation:UIInterfaceOrientationPortrait];
}

#pragma mark Table Sections
- (void)loadSettingsSection
{
    CGSize settingsGridSize = (CGSize){[FTCLayoutHelper getRowWidth:YES], 0};
    settingsGrid = [FTCDetailSectionBox boxWithSize:settingsGridSize andTitle:@"Settings" andActionImage:nil andActionCallback:nil];
    [_masterBox.boxes addObject:settingsGrid];
    
    //Body (settings)
    CGSize rowSize = (CGSize){[FTCLayoutHelper getInputWidth:YES], 30};
    
    //Show disabled stress boxes
    UISwitch *showDisabledStressBoxes = [UISwitch new];
    [showDisabledStressBoxes setOn:[FTCUserSettings showDisabledStressBoxes]];
    
    MGLine *stressBoxLine = [MGLine lineWithLeft:@"Show disabled stress boxes" right:showDisabledStressBoxes size:rowSize];
    [showDisabledStressBoxes bk_addEventHandler:^(id sender) {
        [FTCUserSettings setShowDisabledStressBoxes:((UISwitch *)sender).isOn];
    } forControlEvents:UIControlEventValueChanged];
    
    [settingsGrid.middleLines addObject:stressBoxLine];
    
    //Skill sort order
    SkillSortKey sortKey = [FTCUserSettings skillSortOrder];
    NSArray *sortOptions = [NSArray arrayWithObjects:@"By value", @"Alphabetically", nil];
    UISegmentedControl *skillSortControl = [[UISegmentedControl alloc] initWithItems:sortOptions];
    skillSortControl.selectedSegmentIndex = sortKey;
    [skillSortControl bk_addEventHandler:^(id sender) {
        [FTCUserSettings setSkillSortOrder:((UISegmentedControl *)sender).selectedSegmentIndex];
    } forControlEvents:UIControlEventValueChanged];
    
    MGLine *skillSortLine = [MGLine lineWithLeft:@"Sort skills" right:skillSortControl size:rowSize];
    skillSortLine.topMargin = 8;
    
    [settingsGrid.middleLines addObject:skillSortLine];
    
    //Show tutorial
//    UISwitch *showTutorial = [UISwitch new];
//    [showTutorial setOn:[FTCUserSettings showTutorial]];
//    
//    MGLine *showTutorialLine = [MGLine lineWithLeft:@"Show first-time help" right:showTutorial size:rowSize];
//    [showTutorial bk_addEventHandler:^(id sender) {
//        [FTCUserSettings setShowTutorial:((UISwitch *)sender).isOn];
//    } forControlEvents:UIControlEventValueChanged];
//    showTutorialLine.topMargin = 8;
//    
//    [settingsGrid.middleLines addObject:showTutorialLine];
}

- (void) resizeSettingsGrid:(UIInterfaceOrientation)toInterfaceOrientation
{
    CGSize boxSize = (CGSize){[FTCLayoutHelper getRowWidthForOrientation:toInterfaceOrientation doubleWide:YES], 0};
    CGSize lineSize = (CGSize){[FTCLayoutHelper getInputWidthForOrientation:toInterfaceOrientation doubleWide:YES], 30};
    
    [settingsGrid setSize:boxSize];
    
    for (MGLine *line in settingsGrid.middleLines) {
        [line setSize:lineSize];
    }
}

- (void)loadInfoSection
{
    // The info grid
    CGSize infoGridSize = (CGSize){[FTCLayoutHelper getRowWidth:YES], 0};
    infoGrid = [FTCDetailSectionBox boxWithSize:infoGridSize andTitle:@"About Fate Companion" andActionImage:nil andActionCallback:nil];
    [_masterBox.boxes addObject:infoGrid];
    
    //Body
    id bodyText = @"**Fate Companion** is lovingly developed by Joshua Barron.\n\n"
    "If you have questions, comments, or a feature request, please leave me a review or "
    "use the feedback button below to get in touch.  Thanks for using Fate Companion!|mush";
    MGLine *infoBody = [MGLine multilineWithText:bodyText font:[UIFont systemFontOfSize:16] width:[FTCLayoutHelper getRowWidth:YES] padding:UIEdgeInsetsMake(0, 0, 0, 0)];// size:rowSize];
    //infoBody.sizingMode = MGResizingShrinkWrap;
    //[infoBody sizeToFit];
    [infoGrid.middleLines addObject:infoBody];
    
    UIButton *rateButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [rateButton setTitle:@"Rate Fate Companion" forState:UIControlStateNormal];
    rateButton.titleLabel.font = [UIFont systemFontOfSize:20];
    rateButton.showsTouchWhenHighlighted = YES;
    [rateButton sizeToFit];
    [rateButton bk_addEventHandler:^(id sender) {
        NSLog(@"Rate button tapped!");
        [Appirater rateApp];
    } forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *feedbackButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [feedbackButton setTitle:@"Send us feedback" forState:UIControlStateNormal];
    feedbackButton.titleLabel.font = [UIFont systemFontOfSize:20];
    feedbackButton.showsTouchWhenHighlighted = YES;
    [feedbackButton sizeToFit];
    [feedbackButton bk_addEventHandler:^(id sender) {
        NSLog(@"Feedback button tapped!");
        if ([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
            mailer.mailComposeDelegate = self;
            [mailer setSubject:@"Fate Companion Feedback"];
            NSArray *toRecipients = [NSArray arrayWithObjects:@"fatecompanion@barronsoftware.com", nil];
            [mailer setToRecipients:toRecipients];
            [self presentViewController:mailer animated:YES completion:nil];
        }
        else
        {
            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Can't send feedback" andMessage:@"Sorry, it looks like you don't have e-mail set up on this device."];
            
            [alertView addButtonWithTitle:@"OK"
                                     type:SIAlertViewButtonTypeDefault
                                  handler:nil];
            alertView.transitionStyle = SIAlertViewTransitionStyleFade;

            [alertView show];
        }
    } forControlEvents:UIControlEventTouchUpInside];
    CGSize rowSize = (CGSize){[FTCLayoutHelper getRowWidth:YES], 30};
    if ([FTCLayoutHelper deviceIsIPad])
    {
        MGLine *buttonLine = [MGLine lineWithLeft:rateButton right:feedbackButton size:rowSize];
        buttonLine.sizingMode = MGResizingShrinkWrap;
        buttonLine.topMargin = 8;
        buttonLine.leftItemsAlignment = NSTextAlignmentCenter;
        buttonLine.rightItemsAlignment = NSTextAlignmentCenter;
        buttonLine.leftWidth = buttonLine.rightWidth = buttonLine.width / 2;
        [infoGrid.middleLines addObject:buttonLine];
    }
    else
    {
        MGLine *rateLine = [MGLine lineWithLeft:rateButton right:nil size:rowSize];
                            MGLine *feedbackLine = [MGLine lineWithLeft:feedbackButton right:nil size:rowSize];
        rateLine.topMargin = feedbackLine.topMargin = 8;
        rateLine.leftItemsAlignment = feedbackLine.leftItemsAlignment = NSTextAlignmentCenter;
        //rateLine.sizingMode = feedbackLine.sizingMode = MGResizingShrinkWrap;
        //[rateLine sizeToFit];
        [infoGrid.middleLines addObject:rateLine];
        [infoGrid.middleLines addObject:feedbackLine];
    }
}

- (void) resizeInfoGrid:(UIInterfaceOrientation)toInterfaceOrientation
{
    CGSize boxSize = (CGSize){[FTCLayoutHelper getRowWidthForOrientation:toInterfaceOrientation doubleWide:YES], 0};
    CGSize lineSize = (CGSize){[FTCLayoutHelper getInputWidthForOrientation:toInterfaceOrientation doubleWide:YES], 30};
    
    [infoGrid setSize:boxSize];
    
    MGLine *infoBody = (MGLine *)[infoGrid.middleLines firstObject];
    [infoBody setSize:lineSize];
    
    MGLine *buttonLine = (MGLine *)[infoGrid.middleLines objectAtIndex:1];
    [buttonLine setSize:lineSize];
    buttonLine.leftWidth = buttonLine.rightWidth = buttonLine.width / 2;
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self becomeFirstResponder];
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void)loadCopyrightSection
{
    // The info grid
    CGSize copyrightGridSize = (CGSize){[FTCLayoutHelper getRowWidth:YES], 0};
    copyrightGrid = [FTCDetailSectionBox boxWithSize:copyrightGridSize andTitle:@"Copyright Notice" andActionImage:nil andActionCallback:nil];
    [_masterBox.boxes addObject:copyrightGrid];
    
    //Body
    id copyright =
    @"Fate Companion™ is © Joshua Barron, 2014.  Fate Companion™ is a trademark of Joshua Barron. All rights reserved.\n\n"
    "Fate™ is a trademark of Evil Hat Productions, LLC. The Powered by Fate logo is © Evil Hat Productions, LLC and is used with permission.\n\n"
    "The Fate Core font is © Evil Hat Productions, LLC and is used with permission. The Four Actions icons were designed by Jeremy Keller.\n\n"
    "This work is based on Fate Core System and Fate Accelerated Edition (found at http://www.faterpg.com/), products of Evil Hat Productions, LLC, developed, authored, and edited by Leonard Balsera, Brian Engard, Jeremy Keller, Ryan Macklin, Mike Olson, Clark Valentine, Amanda Valentine, Fred Hicks, and Rob Donoghue, and licensed for our use under the Creative Commons Attribution 3.0 Unported license (http://creativecommons.org/licenses/by/3.0/).";
    MGLine *copyrightBody = [MGLine multilineWithText:copyright font:[UIFont systemFontOfSize:16] width:[FTCLayoutHelper getRowWidth:YES] padding:UIEdgeInsetsMake(0, 0, 0, 0)];
    
    [copyrightGrid.middleLines addObject:copyrightBody];
}


- (void) resizeCopyrightGrid:(UIInterfaceOrientation)toInterfaceOrientation
{
    CGSize boxSize = (CGSize){[FTCLayoutHelper getRowWidthForOrientation:toInterfaceOrientation doubleWide:YES], 0};
    CGSize lineSize = (CGSize){[FTCLayoutHelper getInputWidthForOrientation:toInterfaceOrientation doubleWide:YES], 30};
    
    [copyrightGrid setSize:boxSize];
    MGLine *copyrightBody = (MGLine *)[copyrightGrid.middleLines firstObject];
    
    [copyrightBody setSize:lineSize];
}

@end
