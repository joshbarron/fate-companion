//
//  FTCDiceRoller.h
//  Fate Companion
//
//  Created by Joshua Barron on 3/6/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FTCDiceRollResult.h"

@interface FTCDiceRoller : NSObject

+ (FTCDiceRollResult *)rollDiceWithModifier:(int)modifier;
+ (NSArray *)rollDiceWithModifier:(int)modifier withRepeat:(int)rollCount;

@end
