//
//  FTCNewsStory.m
//  Fate Companion
//
//  Created by Joshua Barron on 4/5/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCNewsStory.h"

@implementation FTCNewsStory
@synthesize title = _title;
@synthesize body = _body;
@synthesize posted = _posted;

- (id) initWithTitle:(NSString *)title andBody:(NSString *)body andPostedDate:(NSDate *)posted
{
    if (self = [super init])
    {
        self.title = title;
        self.body = body;
        self.posted = posted;
    }
    return self;
}

- (NSString *)mushedBody
{
    NSMutableString *mBody = [NSMutableString stringWithString:self.body];
    [mBody appendString:@"|mush"];
    return mBody;
}

- (NSString *)postedDateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSString *strDate = [dateFormatter stringFromDate:self.posted];
    return strDate;
}

@end
