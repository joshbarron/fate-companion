//
//  FTCDiceRoller.m
//  Fate Companion
//
//  Created by Joshua Barron on 3/6/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCDiceRoller.h"

@implementation FTCDiceRoller

+ (FTCDiceRollResult *)rollDiceWithModifier:(int)modifier
{
    return [[FTCDiceRollResult alloc] initWithRoll:[FTCDiceRoller dieRoll] andSecond:[FTCDiceRoller dieRoll] andThird:[FTCDiceRoller dieRoll] andFourth:[FTCDiceRoller dieRoll] andModifier:modifier];
}

+ (NSArray *)rollDiceWithModifier:(int)modifier withRepeat:(int)rollCount
{
    NSMutableArray *rolls = [NSMutableArray arrayWithCapacity:rollCount];
    
    for (int i = 0; i < rollCount; i++) {
        [rolls addObject:[FTCDiceRoller rollDiceWithModifier:modifier]];
    }
    
    return rolls;
}

+ (DieRollResult)dieRoll
{
    //casting to an int here seems to solve some overflow bugs
    int x = arc4random_uniform(3) - 1; //(0/1/2) - 1 => (-1,0,1)
    //NSLog(@"%d", x);
    return x; //(0/1/2) - 1 => (-1,0,1)
}

@end
