//
//  FTCNoteBox.h
//  Fate Companion
//
//  Created by Joshua Barron on 3/15/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "MGLine.h"
#import "FTCNote.h"
#import "JVFloatLabeledTextView.h"

@class FTCNoteLine;

@protocol FTCNoteLineDelegate

- (void) noteLineWasRemoved:(FTCNoteLine *)noteLine;
- (void) noteLineChanged:(FTCNoteLine *)noteLine;
- (void) noteLineHeightChanged:(FTCNoteLine *)noteLine;

@end

@interface FTCNoteLine : MGLine <UITextViewDelegate>

+ (FTCNoteLine *)noteLineWithNote:(FTCNote *)note delegate:(id<FTCNoteLineDelegate>)delegate;

@property (strong) FTCNote *note;
@property (strong) JVFloatLabeledTextView *noteField;
@property (weak) id<FTCNoteLineDelegate> delegate;

@property double lineWidth;

@end
