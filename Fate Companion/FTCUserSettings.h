//
//  FTCUserSettings.h
//  Fate Companion
//
//  Created by Joshua Barron on 3/4/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, SkillSortKey) {
    ByValue = 0,
    Alphabetically = 1,
};

@interface FTCUserSettings : NSObject

+ (BOOL) showDisabledStressBoxes;
+ (void) setShowDisabledStressBoxes:(BOOL)value;

+ (SkillSortKey) skillSortOrder;
+ (void) setSkillSortOrder:(SkillSortKey)sortKey;

+ (NSArray *) getSavedDiceRolls;
+ (void) setSavedDiceRolls:(NSArray *)diceRolls;

+ (NSArray *) getSavedNotes;
+ (void) setSavedNotes:(NSArray *)notes;

+ (BOOL) showTutorial;
+ (void) setShowTutorial:(BOOL)value;

@end
