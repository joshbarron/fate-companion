//
//  UIViewController+DeviceSize.h
//  Fate Companion
//
//  Created by Joshua Barron on 1/29/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <UIKit/UIKit.h>

#define ROW_SIZE               (CGSize){304, 44}

#define IPHONE_PORTRAIT_GRID   (CGSize){312, 0}
#define IPHONE_LANDSCAPE_GRID  (CGSize){160, 0}
#define IPHONE_TABLES_GRID     (CGSize){320, 0}

#define IPAD_PORTRAIT_GRID     (CGSize){136, 0}
#define IPAD_LANDSCAPE_GRID    (CGSize){390, 0}
#define IPAD_TABLES_GRID       (CGSize){624, 0}

@interface UIViewController (DeviceSize)

- (bool) isPhone;

- (CGSize) getGridSize:(bool) isPortrait;

@end
