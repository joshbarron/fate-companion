//
//  FTCApproachEntryViewController.m
//  Fate Companion
//
//  Created by Joshua Barron on 4/14/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCApproachEntryViewController.h"
#import "MGBox.h"
#import "MGTableBox.h"
#import "MGLine.h"
#import "FTCSkillValueTranslator.h"
#import "UIControl+BlocksKit.h"
#import "CRToast.h"
#import "FTCNotificationHelper.h"
#import "FTCLayoutHelper.h"

#define GLYPH_FONT @"FateCoreGlyphs"

@interface FTCApproachEntryViewController ()

@property (nonatomic, strong) NSArray *skillValues;

@end

@implementation FTCApproachEntryViewController
{
    MGTableBox *masterTableBox;
    FTCSkillValueTranslator *translator;
}
@synthesize scroller = _scroller;
@synthesize nameTextField = _nameTextField;
@synthesize valueTextField = _valueTextField;
@synthesize pickerView = _pickerView;

@synthesize approach = _approach;
@synthesize delegate = _delegate;
@synthesize selectedApproachValue = _selectedApproachValue;
@synthesize skillValues = _skillValues;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    translator = [FTCSkillValueTranslator new];
    self.skillValues = [translator allSkillNamesAndValues];
    
    self.scroller.contentLayoutMode = MGLayoutGridStyle;
    self.scroller.scrollEnabled = NO;
    if ([FTCLayoutHelper deviceIsIPad])
    {
        self.scroller.keepFirstResponderAboveKeyboard = NO;
    }
    masterTableBox = [MGTableBox boxWithSize:(CGSize){320, 0}];
    
    [self.scroller.boxes addObject:masterTableBox];
    
    
    MGLine *instructions = [MGLine lineWithMultilineLeft:@"Tap on the fields below to edit this approach's name and level:" right:nil width:280 minHeight:20];
    instructions.leftMargin = instructions.rightMargin = 20;
    instructions.bottomMargin = instructions.topMargin = 5;
    [masterTableBox.topLines addObject:instructions];
    
    self.nameTextField = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(0, 0, 260, 40)];
    [self.nameTextField setPlaceholder:@"Approach Name"];
    self.nameTextField.font = [UIFont boldSystemFontOfSize:20];
    //self.nameTextField.tag = TAG_NAMEFIELD;
    self.nameTextField.clearButtonMode = UITextFieldViewModeAlways;
    self.nameTextField.delegate = self;
    if (self.approach != nil)
    {
        self.nameTextField.text = self.approach.name;
    }
    MGLine *nameLine = [MGLine lineWithLeft:self.nameTextField right:nil size:(CGSize){260, 40}];
    nameLine.leftMargin = nameLine.rightMargin = 30;
    nameLine.bottomPadding = 5;
    [masterTableBox.topLines addObject:nameLine];
    
    self.valueTextField = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(0, 0, 260, 40)];
    [self.valueTextField setPlaceholder:@"Approach Level"];
    [[self.valueTextField valueForKey:@"textInputTraits"] setValue:[UIColor clearColor] forKey:@"insertionPointColor"];
    self.valueTextField.font = [UIFont boldSystemFontOfSize:20];
    //self.valueTextField.tag = TAG_PICKERLINE;
    self.valueTextField.clearButtonMode = UITextFieldViewModeNever;
    self.valueTextField.delegate = self;
    self.pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 80)];
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    if (self.approach != nil)
    {
        self.selectedApproachValue = [self.approach.value intValue];
        self.valueTextField.text = [self.skillValues objectAtIndex:self.selectedApproachValue + 2];
    }
    self.valueTextField.inputView = self.pickerView;
    UIToolbar *valueToolbar = [[UIToolbar alloc] initWithFrame:
                               CGRectMake(0,0, 320, 44)]; //should code with variables to support view resizing
    UIBarButtonItem *doneButton =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                  target:self action:@selector(inputAccessoryViewDidFinish:)];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [valueToolbar setItems:[NSArray arrayWithObjects: flexibleSpace, doneButton, nil] animated:NO];
    self.valueTextField.inputAccessoryView = valueToolbar;
    
    
    MGLine *valueLine = [MGLine lineWithLeft:self.valueTextField right:nil size:(CGSize){260, 40}];
    valueLine.leftMargin = valueLine.rightMargin = 30;
    valueLine.topMargin = 5;
    valueLine.font = [UIFont boldSystemFontOfSize:20];
    valueLine.rightFont = [UIFont systemFontOfSize:20];
    
    [masterTableBox.middleLines addObject:valueLine];
    
    [self.scroller layout];
    [self.scroller scrollToView:masterTableBox withMargin:0];
}

- (void)inputAccessoryViewDidFinish:(id) sender
{
    NSInteger selectedIndex = [self.pickerView selectedRowInComponent:0];
    self.valueTextField.text = [self.skillValues objectAtIndex:selectedIndex];
    self.selectedApproachValue = ((int)selectedIndex) - 2;
    [self.valueTextField resignFirstResponder];
}

#pragma mark UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self.nameTextField)
    {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 15) ? NO : YES;
    }
    return YES;
}

- (void)textChanged:(UITextField *)textField {
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
    [self textChanged:textField];
	return YES;
}

#pragma mark - UIPickerView DataSource
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [_skillValues count];
}

#pragma mark - UIPickerView Delegate
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 30.0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [_skillValues objectAtIndex:row];
}

//If the user chooses from the pickerview, it calls this function;
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.valueTextField.text = [self.skillValues objectAtIndex:row];
    self.selectedApproachValue = ((int)row) - 2;
}


- (IBAction)cancelButtonWasTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveButtonWasTapped:(id)sender {
    NSString *name = self.nameTextField.text;
    
    if (name == nil || name.length == 0)
    {
        [CRToastManager showNotificationWithOptions:[FTCNotificationHelper warningNotification:@"Oops!" andSubtitle:@"Please enter a name for your approach."] completionBlock:nil];
        return;
    }
    
    NSString *valueText = self.valueTextField.text;
    if (valueText == nil || valueText.length == 0)
    {
        [CRToastManager showNotificationWithOptions:[FTCNotificationHelper warningNotification:@"Oops!" andSubtitle:@"Please select a level for your approach."] completionBlock:nil];
        return;
    }
    
    NSString *validationMessage = nil;
    if (![self.delegate validateApproachEntry:self withMessage:&validationMessage])
    {
        [CRToastManager showNotificationWithOptions:[FTCNotificationHelper warningNotification:@"Oops!" andSubtitle:validationMessage] completionBlock:nil];
        return;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.delegate approachWasSaved:self];
}

@end
