//
//  FTCStandardSkillsHelper.m
//  Fate Companion
//
//  Created by Joshua Barron on 2/21/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCStandardSkillsHelper.h"
#import "FTCSkill.h"

#define SKILLS_PLIST @"StandardSkills"

#define SKILLS_KEY @"StandardSkills"
#define NAME_KEY @"Name"
#define ACTIONS_KEY @"SkillActions"

@implementation FTCStandardSkillsHelper
@synthesize standardSkills = _standardSkills;

- (id) init {
    if (self = [super init])
    {
        [self setup];
    }
    return self;
}

- (void) setup
{
    NSDictionary *plist = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:SKILLS_PLIST ofType:@"plist"]];
    
    NSArray *array = [plist objectForKey:SKILLS_KEY];
    
    NSMutableArray *m_skills = [[NSMutableArray alloc] init];
    
    for (NSDictionary *skillDict in array) {
        NSString *name = [skillDict objectForKey:NAME_KEY];
        SkillActions actions = [((NSNumber *)[skillDict objectForKey:ACTIONS_KEY]) intValue];
        
        FTCSkillDefinition *skillDef = [[FTCSkillDefinition alloc] initWithName:name andActions:actions];
        
        [m_skills addObject:skillDef];
    }
    
    self.standardSkills = m_skills;
}

- (NSArray *) undesignatedStandardSkillsForCharacter:(FTCDocument *)characterDoc
{
    NSArray *usedSkillNames = [characterDoc.skills valueForKeyPath:@"name"];
    
    NSIndexSet *unusedObjectIndexes = [self.standardSkills indexesOfObjectsPassingTest:^BOOL (id obj, NSUInteger idx, BOOL *stop){
        FTCSkillDefinition *skillDef = (FTCSkillDefinition *)obj;
        return ![usedSkillNames containsObject:skillDef.name];
    }];
    
    return [self.standardSkills objectsAtIndexes:unusedObjectIndexes];
}

@end
