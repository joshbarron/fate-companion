//
//  FTCAspectLine.m
//  Fate Companion
//
//  Created by Joshua Barron on 2/15/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCAspectLine.h"
#import "FontAwesomeKit/FAKIonIcons.h"
#import "UIControl+BlocksKit.h"
#import "FTCLayoutHelper.h"
#import "SIAlertView.h"

#define HEIGHT 30

@implementation FTCAspectLine
@synthesize isMandatory = _isMandatory;
@synthesize delegate = _delegate;
@synthesize aspectField = _aspectField;
@synthesize aspect = _aspect;
@synthesize lineWidth = _lineWidth;

+ (double) getInputWidth:(double)totalWidth
{
    return totalWidth - 35;
}

+ (FTCAspectLine *)aspectLineWithAspect:(FTCAspect *)aspect placeholder:(NSString *)placeholder tag:(int)tag mandatory:(bool)isMandatory delegate:(id<FTCAspectLineDelegate>)delegate
{
    double totalWidth = [FTCLayoutHelper getRowWidth:YES] - 6;
    
    double inputWidth = [FTCAspectLine getInputWidth:totalWidth];
    
    //int inputWidth = size.width - 35;
    
    JVFloatLabeledTextView *aspectField = [[JVFloatLabeledTextView alloc] initWithFrame:CGRectMake(0, 0, inputWidth, HEIGHT)];
    //highConceptField = self.characterName;
    //aspectField.borderStyle = UITextBorderStyleRoundedRect;
    aspectField.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
    [aspectField setPlaceholder: placeholder];
    [aspectField setText: aspect.aspectText];
    aspectField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    aspectField.keyboardType = UIKeyboardTypeAlphabet;
    aspectField.returnKeyType = UIReturnKeyDefault;
    aspectField.tag = tag;
    aspectField.scrollEnabled = NO;
    
    MGBox *rightBox = nil;
    
    if (!isMandatory) {
        FAKIcon *aspectsIcon = [FAKIonIcons ios7CloseOutlineIconWithSize:24];
        [aspectsIcon addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]];
        UIImage *aspectsIconImage = [aspectsIcon imageWithSize:(CGSize){35, 30}];
        
        UIButton *actionButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, aspectsIconImage.size.width, aspectsIconImage.size.height)];
        [actionButton setBackgroundImage:aspectsIconImage forState:UIControlStateNormal];
        actionButton.showsTouchWhenHighlighted = YES;
        rightBox = [MGBox boxWithSize:actionButton.size];
        [rightBox addSubview: actionButton]; //imgView];
        rightBox.rightMargin = 0;
        //rightBox.tag = RIGHT_ITEM_TAG;
    }
    
    FTCAspectLine *aspectLine = [FTCAspectLine lineWithLeft:aspectField right:rightBox size:(CGSize){totalWidth, HEIGHT}];
    aspectLine.delegate = delegate;
    aspectLine.isMandatory = isMandatory;
    aspectLine.aspectField = aspectField;
    aspectLine.aspect = aspect;
    aspectLine.tag = tag;
    aspectLine.lineWidth = totalWidth;
    aspectLine.sizingMode = MGResizingShrinkWrap;
    aspectField.delegate = aspectLine;
    
    double padding = aspectField.textContainer.lineFragmentPadding;
    aspectLine.leftMargin = -padding;
    aspectLine.rightMargin = -padding;
    
    [aspectField layoutSubviews];
    
    CGSize textViewSize = [aspectField sizeThatFits:CGSizeMake(aspectField.frame.size.width, FLT_MAX)];
    aspectField.height = textViewSize.height;
    [aspectLine setHeight:aspectField.height];
    [aspectLine sizeToFit];
    
    UIToolbar *valueToolbar = [[UIToolbar alloc] initWithFrame:
                               CGRectMake(0,0, 320, 44)]; //should code with variables to support view resizing
    UIBarButtonItem *doneButton =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                  target:aspectLine action:@selector(inputAccessoryViewDidFinish:)];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [valueToolbar setItems:[NSArray arrayWithObjects: flexibleSpace, doneButton, nil] animated:NO];
    aspectField.inputAccessoryView = valueToolbar;
    
    if (!isMandatory && rightBox != nil) {
        UIButton *actionButton = (UIButton *)[rightBox.subviews objectAtIndex:0];
        [actionButton bk_addEventHandler:^(id sender) {
            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Remove Aspect?" andMessage:@"Are you sure you want to remove this aspect?"];
            //alertView.messageFont = [UIFont systemFontOfSize:24];
            
            [alertView addButtonWithTitle:@"Cancel"
                                     type:SIAlertViewButtonTypeDefault
                                  handler:nil];
            [alertView addButtonWithTitle:@"Remove" type:SIAlertViewButtonTypeDestructive handler:^(SIAlertView *alert) {
                [aspectLine.delegate aspectLineWasRemoved:aspectLine];
            }];
            alertView.transitionStyle = SIAlertViewTransitionStyleFade;
            
            [alertView show];

        } forControlEvents:UIControlEventTouchUpInside];
    }
    return aspectLine;
}

- (void)setup {
    [super setup];
    self.bottomMargin = 0;
    self.rasterize = YES;
    
}

- (void) layout {

    [self setSize:(CGSize){self.lineWidth, HEIGHT}];
    [self.aspectField setSize:(CGSize){[FTCAspectLine getInputWidth:self.lineWidth], HEIGHT}];
    CGSize textViewSize = [self.aspectField sizeThatFits:CGSizeMake(self.aspectField.frame.size.width, FLT_MAX)];
    self.aspectField.height = textViewSize.height;
    [self setHeight:self.aspectField.height];
    [self sizeToFit];
    
    [super layout];
}

#pragma mark UITextViewDelegate
- (void)inputAccessoryViewDidFinish:(id) sender
{
    [self.aspectField resignFirstResponder];
}

- (void) textViewDidChange:(UITextView *)textView
{
    CGSize textViewSize = [textView sizeThatFits:CGSizeMake(textView.frame.size.width, FLT_MAX)];
    
    if (textView.height != textViewSize.height) {
        NSLog(@"Height changed");
        textView.height = textViewSize.height;
        [self setHeight:textView.height];
        [self sizeToFit];
        [self.delegate aspectLineHeightChanged:self];
    }
}

- (void) textViewDidEndEditing:(UITextView *)textView
{
    [self.delegate aspectLineChanged:self withText:textView.text];
    [self.aspectField layoutSubviews];
}

@end
