//
//  FTCSkillEntryViewController.h
//  Fate Companion
//
//  Created by Joshua Barron on 2/17/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FTCSkill.h"
#import "JVFloatLabeledTextField.h"
#import "MGScrollView.h"

@class FTCSkillEntryViewController;

@protocol FTCSkillEntryDelegate

- (BOOL) validateSkillEntry:(FTCSkillEntryViewController *)skillEntryController withMessage:(NSString * __autoreleasing *)validationMessage;
- (void) skillWasSaved:(FTCSkillEntryViewController *)skillEntryController;

@end

@interface FTCSkillEntryViewController : UIViewController <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
- (IBAction)cancelButtonWasTapped:(id)sender;
- (IBAction)saveButtonWasTapped:(id)sender;

@property (weak, nonatomic) IBOutlet MGScrollView *scroller;
@property (strong, nonatomic) JVFloatLabeledTextField *nameTextField;
@property (strong, nonatomic) JVFloatLabeledTextField *valueTextField;
@property (strong, nonatomic) UIPickerView *pickerView;

@property SkillActions skillActions;
@property int selectedSkillValue;

@property (weak) FTCSkill *skill;
@property (weak) id<FTCSkillEntryDelegate> delegate;

@end
