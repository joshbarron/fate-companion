//
//  FTCNote.m
//  Fate Companion
//
//  Created by Joshua Barron on 3/15/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCNote.h"
#import "NSDate+RelativeTime.h"

@implementation FTCNote
@synthesize date = _date;
@synthesize text = _text;

- (id) initWithText:(NSString *)text
{
    if (self = [super init])
    {
        self.date = [NSDate date];
        self.text = text;
    }
    return self;
}

- (id) initWithDate:(NSDate *)date andText:(NSString *)text
{
    if (self = [super init])
    {
        self.date = date;
        self.text = text;
    }
    return self;
}

- (NSString *)title
{
    return [self.date relativeTime];
}

#pragma mark NSCoding

#define kNoteVersionKey @"NoteVersion"
#define kNoteDateKey @"NoteDate"
#define kNoteTextKey @"NoteText"

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeInt:1 forKey:kNoteVersionKey];
    [encoder encodeObject:self.date forKey:kNoteDateKey];
    [encoder encodeObject:self.text forKey:kNoteTextKey];
}

- (id)initWithCoder:(NSCoder *)decoder {
    [decoder decodeIntForKey:kNoteVersionKey];
    NSDate *date = [decoder decodeObjectForKey:kNoteDateKey];
    NSString *text = [decoder decodeObjectForKey:kNoteTextKey];
    
    return [self initWithDate:date andText:text];
}

@end
