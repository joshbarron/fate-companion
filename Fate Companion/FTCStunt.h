//
//  FTCStunt.h
//  Fate Companion
//
//  Created by Joshua Barron on 2/22/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FTCStunt : NSObject<NSCoding>

@property (strong) NSString * name;
@property (strong) NSString * stuntText;

- (id) initWithName:(NSString *)name andStuntText:(NSString *)stuntText;

@end
