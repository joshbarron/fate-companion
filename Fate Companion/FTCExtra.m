//
//  FTCExtra.m
//  Fate Companion
//
//  Created by Joshua Barron on 2/27/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCExtra.h"

@implementation FTCExtra
@synthesize name = _name;

- (id) initWithName:(NSString *)name
{
    if (self = [super init])
    {
        self.name = name;
    }
    return self;
}

#pragma mark NSCoding

#define kExtraVersionKey @"ExtraVersion"
#define kExtraNameKey @"ExtraName"
#define kExtraTextKey @"ExtraText"

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeInt:1 forKey:kExtraVersionKey];
    [encoder encodeObject:self.name forKey:kExtraNameKey];
}

- (id)initWithCoder:(NSCoder *)decoder {
    [decoder decodeIntForKey:kExtraVersionKey];
    NSString *name = [decoder decodeObjectForKey:kExtraNameKey];
    return [self initWithName:name];
}

@end
