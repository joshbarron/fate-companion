//
//  FTCCharacterMetadata.m
//  Fate Companion
//
//  Created by Joshua Barron on 1/25/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCCharacterMetadata.h"

@implementation FTCCharacterMetadata
@synthesize thumbnail = _thumbnail;
@synthesize name = _name;
@synthesize description = _description;

- (id)initWithThumbnail:(UIImage *)thumbnail andName:(NSString *)name andDescription:(NSString *)description {
    if ((self = [super init])) {
        self.thumbnail = thumbnail;
        self.name = name;
        self.description = description;
    }
    return self;
}

- (id)init {
    return [self initWithThumbnail:nil andName:nil andDescription:nil];
}

#pragma mark NSCoding

#define kVersionKey @"Version"
#define kThumbnailKey @"Thumbnail"
#define kNameKey @"Name"
#define kDescriptionKey @"Description"

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeInt:1 forKey:kVersionKey];
    NSData * thumbnailData = UIImagePNGRepresentation(self.thumbnail);
    [encoder encodeObject:thumbnailData forKey:kThumbnailKey];
    [encoder encodeObject:self.name forKey:kNameKey];
    [encoder encodeObject:self.description forKey:kDescriptionKey];
}

- (id)initWithCoder:(NSCoder *)decoder {
    [decoder decodeIntForKey:kVersionKey];
    NSData * thumbnailData = [decoder decodeObjectForKey:kThumbnailKey];
    UIImage * thumbnail = [UIImage imageWithData:thumbnailData];
    NSString * name = [decoder decodeObjectForKey:kNameKey];
    NSString * description = [decoder decodeObjectForKey:kDescriptionKey];
    return [self initWithThumbnail: thumbnail andName:name andDescription:description];
}
@end
