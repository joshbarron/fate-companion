//
//  FTCStuntLine.m
//  Fate Companion
//
//  Created by Joshua Barron on 2/22/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCStuntLine.h"
#import "FontAwesomeKit/FAKIonIcons.h"
#import "UIControl+BlocksKit.h"
#import "MGTableBox.h"
#import "FTCLayoutHelper.h"
#import "SIAlertView.h"

#define HEIGHT 50

@interface FTCStuntLine ()

@property MGBox *mainBox;
@property MGBox *imgBox;
@property MGLine *textViewLine;

@end

@implementation FTCStuntLine
@synthesize lineWidth = _lineWidth;
@synthesize delegate = _delegate;
@synthesize stunt = _stunt;
@synthesize nameField = _nameField;
@synthesize stuntTextField = _stuntTextField;
@synthesize textViewLine = _textViewLine;

+ (double) getInputWidth:(double)totalWidth
{
    return totalWidth - 35;
}

+ (FTCStuntLine *)stuntLineWithStunt:(FTCStunt *)stunt delegate:(id<FTCStuntLineDelegate>)delegate
{
    double totalWidth = [FTCLayoutHelper getRowWidth:NO] - 8;
    
    double inputWidth = [FTCStuntLine getInputWidth:totalWidth];
    
    CGSize mainSize = CGSizeMake(inputWidth, HEIGHT);

    FAKIcon *deleteIcon = [FAKIonIcons ios7CloseOutlineIconWithSize:24];
    [deleteIcon addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]];
    UIImage *deleteIconImage = [deleteIcon imageWithSize:(CGSize){35, 30}];
    
    UIButton *actionButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, deleteIconImage.size.width, deleteIconImage.size.height)];
    [actionButton setBackgroundImage:deleteIconImage forState:UIControlStateNormal];
    actionButton.showsTouchWhenHighlighted = YES;
    MGBox *imgBox = [MGBox boxWithSize:actionButton.size];
    [imgBox addSubview:actionButton];
    
    
    JVFloatLabeledTextField *nameField = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(0, 0, mainSize.width, mainSize.height)];
    [nameField setPlaceholder:@"Stunt"];
    nameField.text = stunt.name;
    nameField.font = [UIFont boldSystemFontOfSize:16];
    
    MGLine *mainLine = [MGLine lineWithLeft:nameField right:nil size:mainSize];
    mainLine.bottomPadding = 2;
    mainLine.font = [UIFont fontWithName:@"HelveticaNeueBold" size:16];
    [mainLine sizeToFit];
    
    
    JVFloatLabeledTextView *stuntTextField = [[JVFloatLabeledTextView alloc] initWithFrame:CGRectMake(0, 0, mainSize.width, mainSize.height)];
    [stuntTextField setPlaceholder:@"Description"];
    stuntTextField.text = stunt.stuntText;
    stuntTextField.font = [UIFont systemFontOfSize:14];
    stuntTextField.scrollEnabled = NO;
    
    double padding = stuntTextField.textContainer.lineFragmentPadding;
    
    MGLine *subLine = [MGLine lineWithLeft:stuntTextField right:nil size:(CGSize) {mainSize.width, mainSize.height}];
    subLine.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
    subLine.leftMargin = subLine.rightMargin = -padding;
    
    CGSize textViewSize = [stuntTextField sizeThatFits:CGSizeMake(stuntTextField.frame.size.width, FLT_MAX)];
    stuntTextField.height = textViewSize.height;
    [subLine setHeight:stuntTextField.height];
    [subLine sizeToFit];
    
    int totalHeight = mainLine.height + subLine.height;
    MGBox *mainBox = [MGBox boxWithSize:(CGSize){mainSize.width, mainLine.height + subLine.height}];
    mainBox.contentLayoutMode = MGLayoutTableStyle;
    [mainBox.boxes addObject:mainLine];
    [mainBox.boxes addObject:subLine];
    
    FTCStuntLine *stuntLine = [FTCStuntLine lineWithLeft:mainBox right:imgBox size:(CGSize){totalWidth, totalHeight}];
    stuntLine.delegate = delegate;
    stuntLine.stunt = stunt;
    stuntLine.nameField = nameField;
    stuntLine.stuntTextField = stuntTextField;
    stuntLine.textViewLine = subLine;
    stuntLine.lineWidth = totalWidth;
    stuntLine.mainBox = mainBox;
    stuntLine.imgBox = imgBox;
    
    nameField.delegate = stuntLine;
    stuntTextField.delegate = stuntLine;
    
    
    UIToolbar *valueToolbar = [[UIToolbar alloc] initWithFrame:
                               CGRectMake(0,0, 320, 44)]; //should code with variables to support view resizing
    UIBarButtonItem *doneButton =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                  target:stuntLine action:@selector(inputAccessoryViewDidFinish:)];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [valueToolbar setItems:[NSArray arrayWithObjects: flexibleSpace, doneButton, nil] animated:NO];
    stuntTextField.inputAccessoryView = valueToolbar;
    
    [actionButton bk_addEventHandler:^(id sender) {
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Remove Stunt?" andMessage:@"Are you sure you wish to remove this stunt?"];
        //alertView.messageFont = [UIFont systemFontOfSize:24];
        
        [alertView addButtonWithTitle:@"Cancel"
                                 type:SIAlertViewButtonTypeDefault
                              handler:nil];
        [alertView addButtonWithTitle:@"Remove" type:SIAlertViewButtonTypeDestructive handler:^(SIAlertView *alert) {
            [stuntLine.delegate stuntLineWasRemoved:stuntLine];
        }];
        alertView.transitionStyle = SIAlertViewTransitionStyleFade;
        
        [alertView show];
        
        
    } forControlEvents:UIControlEventTouchUpInside];
    
    return stuntLine;
}

- (void)setup {
    [super setup];
    //self.sizingMode = MGResizingShrinkWrap;
    self.bottomMargin = 4;
    self.bottomPadding = 6;
    //self.font = [UIFont fontWithName:@"HelveticaNeue" size:24];
    
    self.borderStyle = MGBorderEtchedBottom;
    self.bottomBorderColor = [UIColor blackColor];
}

- (void) layout {
    double inputWidth = [FTCStuntLine getInputWidth:self.lineWidth];
    [self setSize:(CGSize){self.lineWidth, HEIGHT}];
    
    [self.nameField setSize:(CGSize){inputWidth, HEIGHT}];
    
    [self.stuntTextField setSize:(CGSize){inputWidth, HEIGHT}];
    CGSize textViewSize = [self.stuntTextField sizeThatFits:CGSizeMake(self.stuntTextField.frame.size.width, FLT_MAX)];
    self.stuntTextField.height = textViewSize.height;
    int totalHeight = self.nameField.height + self.stuntTextField.height;
    [self setHeight:totalHeight];
    
    for (MGBox *box in self.mainBox.boxes) {
        [box setSize:(CGSize){inputWidth, box.height}];
    }
    
    [self.mainBox setSize:(CGSize){inputWidth, self.mainBox.height}];
    
    [self sizeToFit];
    [super layout];
}

#pragma mark UITextFieldDelegate
- (void)textChanged:(UITextField *)textField
{
    [self.delegate stuntLineNameChanged:self withText:textField.text];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self textChanged:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
    [self textChanged:textField];
	return YES;
}

#pragma mark UITextViewDelegate
- (void)inputAccessoryViewDidFinish:(id) sender
{
    [self.stuntTextField resignFirstResponder];
}

- (void) textViewDidChange:(UITextView *)textView
{
    CGSize textViewSize = [textView sizeThatFits:CGSizeMake(textView.frame.size.width, FLT_MAX)];
    
    if (textView.height != textViewSize.height) {
        NSLog(@"Height changed: %f %f", textView.height, textViewSize.height);
        textView.height = textViewSize.height;
        [self.textViewLine setHeight:textView.height];
        [self setHeight:textView.height + HEIGHT];
        [self.mainBox setHeight:textView.height + HEIGHT];
        [self sizeToFit];
        [self.delegate stuntLineHeightChanged:self];
    }
}

- (void) textViewDidEndEditing:(UITextView *)textView
{
    [self.delegate stuntLineTextChanged:self withText:self.stuntTextField.text];
    [self.stuntTextField layoutSubviews];
}



@end
