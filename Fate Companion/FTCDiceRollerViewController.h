//
//  FTCSecondViewController.h
//  Fate Companion
//
//  Created by Joshua Barron on 1/25/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGScrollView.h"
#import "MGBox.h"
#import "FTCDiceRollBox.h"

@interface FTCDiceRollerViewController : UIViewController<UIScrollViewDelegate, UISplitViewControllerDelegate, FTCDiceRollBoxDelegate>

@property (weak, nonatomic) IBOutlet MGScrollView *scroller;

@property NSMutableArray *rollBoxes;

@property (nonatomic, strong) UIPopoverController *popover;

@end
