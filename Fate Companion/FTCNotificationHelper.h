//
//  FTCNotificationHelper.h
//  Fate Companion
//
//  Created by Joshua Barron on 3/5/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FTCNotificationHelper : NSObject

+ (NSDictionary *) successNotification:(NSString *)title;
+ (NSDictionary *) successNotification:(NSString *)title andSubtitle:(NSString *)subtitle;
+ (NSDictionary *) successNotification:(NSString *)title andSubtitle:(NSString *)subtitle andDuration:(float)duration;

+ (NSDictionary *) warningNotification:(NSString *)title;
+ (NSDictionary *) warningNotification:(NSString *)title andSubtitle:(NSString *)subtitle;
@end
