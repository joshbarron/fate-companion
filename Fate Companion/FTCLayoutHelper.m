//
//  FTCLayoutHelper.m
//  Fate Companion
//
//  Created by Joshua Barron on 3/15/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCLayoutHelper.h"

#define IPHONE_PORTRAIT_WIDTH 320
#define IPAD_PORTRAIT_WIDTH 768
#define IPAD_LANDSCAPE_WIDTH 704

#define IPHONE_PORTRAIT_PADDING 4
#define IPAD_PORTRAIT_PADDING 16 //32
#define IPAD_LANDSCAPE_PADDING 24 //16

#define IPHONE_ROW_WIDTH 304
#define IPAD_PORTRAIT_ROW_WIDTH 336 //352 - 16
#define IPAD_LANDSCAPE_ROW_WIDTH 304 //320 - 16

#define IPHONE_INPUT_WIDTH 296
#define IPAD_PORTRAIT_INPUT_WIDTH 328 //336 - 8
#define IPAD_LANDSCAPE_INPUT_WIDTH 296 //304 - 8

@implementation FTCLayoutHelper

+ (FTCAppDelegate *) appDelegate
{
    return (FTCAppDelegate *)[[UIApplication sharedApplication] delegate];
}

+ (UIColor *)getTintColor
{
//    UIColor *tc = [[[[UIApplication sharedApplication] delegate] window] tintColor];
//    
//    if (tc == nil || [tc isKindOfClass:[UICachedDeviceWhiteColor Class]]) {
//        tc = [UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0];
//    }
    
    return [UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0];
}

+ (void) setupMenuNav:(UIViewController *)vc
{
    if ([FTCLayoutHelper deviceIsIPad])
    {
        FTCAppDelegate *appDelegate = [FTCLayoutHelper appDelegate];
        if (vc.navigationItem.leftBarButtonItem == nil &&
            appDelegate.menuNavItem != nil)
        {
            [vc.navigationItem setLeftBarButtonItem:appDelegate.menuNavItem animated:YES];
            [appDelegate.menuPopover dismissPopoverAnimated:YES];
        }
    }
}

+ (BOOL) deviceIsIPhone
{
    return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone;
}

+ (BOOL) deviceIsIPad
{
    return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
}

+ (FTCSplitViewController *) splitViewController:(UIViewController *)vc
{
    return (FTCSplitViewController *)[vc splitViewController];
}

+ (UIInterfaceOrientation) getDeviceOrientation
{
    return [[UIApplication sharedApplication] statusBarOrientation];
}

+ (double) getGridWidth
{
    if ([FTCLayoutHelper deviceIsIPhone])
    {
        return IPHONE_PORTRAIT_WIDTH;
    }
    else if ([FTCLayoutHelper deviceIsIPad])
    {
        UIInterfaceOrientation orientation = [FTCLayoutHelper getDeviceOrientation];
        if (UIInterfaceOrientationIsLandscape(orientation)) {
            return IPAD_LANDSCAPE_WIDTH;
        }
        else
        {
            return IPAD_PORTRAIT_WIDTH;
        }
    }
    return 0;
}

+ (double) getGridWidthForOrientation:(UIInterfaceOrientation) orientation
{
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        return IPAD_PORTRAIT_WIDTH;
    }
    else
    {
        return IPAD_LANDSCAPE_WIDTH;
    }
}

+ (double) getGridItemPadding
{
    if ([FTCLayoutHelper deviceIsIPhone]) {
        return IPHONE_PORTRAIT_PADDING;
    }
    else if ([FTCLayoutHelper deviceIsIPad])
    {
        UIInterfaceOrientation orientation = [FTCLayoutHelper getDeviceOrientation];
        if (UIInterfaceOrientationIsLandscape(orientation)) {
            return IPAD_LANDSCAPE_PADDING;
        }
        else
        {
            return IPAD_PORTRAIT_PADDING;
        }
    }
    return 0;
}

+ (double) getGridItemPaddingForOrientation:(UIInterfaceOrientation) orientation
{
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        return IPAD_PORTRAIT_PADDING;
    }
    else
    {
        return IPAD_LANDSCAPE_PADDING;
    }
}

+ (double) getRowWidth:(BOOL)doubleWide
{
    return [FTCLayoutHelper getRowWidthForOrientation:[FTCLayoutHelper getDeviceOrientation] doubleWide:doubleWide];
}

+ (double) getRowWidthForOrientation:(UIInterfaceOrientation) orientation doubleWide:(BOOL)doubleWide
{
    if ([FTCLayoutHelper deviceIsIPhone])
        return IPHONE_ROW_WIDTH;
    else
    {
        if (UIInterfaceOrientationIsPortrait(orientation))
        {
            return doubleWide ? IPAD_PORTRAIT_ROW_WIDTH * 2 + (2 * IPAD_PORTRAIT_PADDING) :
            IPAD_PORTRAIT_ROW_WIDTH;
        }
        else
        {
            return doubleWide ? IPAD_LANDSCAPE_ROW_WIDTH * 2 + (2 * IPAD_LANDSCAPE_PADDING) :
            IPAD_LANDSCAPE_ROW_WIDTH;
        }
    }
}

+ (double) getInputWidth:(BOOL)doubleWide
{
    return [FTCLayoutHelper getInputWidthForOrientation:[FTCLayoutHelper getDeviceOrientation] doubleWide:doubleWide];
}

+ (double) getInputWidthForOrientation:(UIInterfaceOrientation) orientation doubleWide:(BOOL)doubleWide
{
    if ([FTCLayoutHelper deviceIsIPhone])
        return IPHONE_INPUT_WIDTH;
    else
    {
        if (UIInterfaceOrientationIsPortrait(orientation))
        {
            if (doubleWide)
                return IPAD_PORTRAIT_INPUT_WIDTH * 2 + (2 * IPAD_PORTRAIT_PADDING);
            
            return IPAD_PORTRAIT_INPUT_WIDTH;
        }
        else
        {
            if (doubleWide)
                return IPAD_LANDSCAPE_INPUT_WIDTH * 2 + (2 * IPAD_LANDSCAPE_PADDING);
            return IPAD_LANDSCAPE_INPUT_WIDTH;
        }
    }
}
@end
