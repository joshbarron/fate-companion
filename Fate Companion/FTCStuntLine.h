//
//  FTCStuntLine.h
//  Fate Companion
//
//  Created by Joshua Barron on 2/22/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "MGLine.h"
#import "FTCStunt.h"
#import "JVFloatLabeledTextField.h"
#import "JVFloatLabeledTextView.h"

@class FTCStuntLine;

@protocol FTCStuntLineDelegate

- (void)stuntLineWasRemoved:(FTCStuntLine *)stuntLine;
//- (void)stuntLineWasTapped:(FTCStuntLine *)stuntLine;
- (void)stuntLineHeightChanged:(FTCStuntLine *)stuntLine;
- (void)stuntLineNameChanged:(FTCStuntLine *)stuntLine withText:(NSString *)text;
- (void)stuntLineTextChanged:(FTCStuntLine *)stuntLine withText:(NSString *)text;

@end

@interface FTCStuntLine : MGLine<UITextFieldDelegate, UITextViewDelegate>

+ (FTCStuntLine *)stuntLineWithStunt:(FTCStunt *)stunt delegate:(id<FTCStuntLineDelegate>)delegate;

@property (weak) id<FTCStuntLineDelegate> delegate;
@property (weak) FTCStunt *stunt;

@property JVFloatLabeledTextField *nameField;
@property JVFloatLabeledTextView *stuntTextField;

@property double lineWidth;

@end
