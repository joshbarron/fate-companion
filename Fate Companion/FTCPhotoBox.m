//
//  Created by matt on 28/09/12.
//

#import "FTCPhotoBox.h"

@implementation FTCPhotoBox

#pragma mark - Init

- (void)setup {
    
    // positioning
    self.topMargin = 4;
    self.leftMargin = 4;
    
    // background
    self.backgroundColor = [UIColor colorWithRed:0.94 green:0.94 blue:0.95 alpha:1];
    
    // shadow
    self.layer.shadowColor = [UIColor colorWithWhite:0.12 alpha:1].CGColor;
    self.layer.shadowOffset = CGSizeMake(0, 0.5);
    self.layer.shadowRadius = 1;
    self.layer.shadowOpacity = 1;
    
//    UIBezierPath *path = [UIBezierPath bezierPath];
//    [path addArcWithCenter:CGPointMake(self.size.width / 2.0, self.size.height / 2.0) radius:self.size.width * 0.40 startAngle:0 endAngle:M_PI * 2.0 clockwise:YES];
//    CAShapeLayer *layer = [[CAShapeLayer alloc] init];
//    layer.path = [path CGPath];
//    self.layer.mask = layer;
}

#pragma mark - Factories

+ (FTCPhotoBox *)photoBoxFor:(UIImage *)image withSize:(CGSize)size
{
    FTCPhotoBox *box = [FTCPhotoBox boxWithSize:size];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    [imageView setSize:size];
    [box addSubview:imageView];
    
    return box;
}

#pragma mark - Layout

- (void)layout {
    [super layout];
    
    // speed up shadows
    self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
}

- (void)setPhoto: (UIImage *)image
{
    UIImageView *imageView = (UIImageView *)[self.subviews objectAtIndex:0];
    [imageView setImage:image];
    
    [self layout];
}

@end
