//
//  FTCStunt.m
//  Fate Companion
//
//  Created by Joshua Barron on 2/22/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCStunt.h"

@implementation FTCStunt
@synthesize name = _name;
@synthesize stuntText = _stuntText;

- (id) initWithName:(NSString *)name andStuntText:(NSString *)stuntText
{
    if (self = [super init])
    {
        self.name = name;
        self.stuntText = stuntText;
    }
    return self;
}

#pragma mark NSCoding

#define kStuntVersionKey @"StuntVersion"
#define kStuntNameKey @"StuntName"
#define kStuntTextKey @"StuntText"

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeInt:1 forKey:kStuntVersionKey];
    [encoder encodeObject:self.name forKey:kStuntNameKey];
    [encoder encodeObject:self.stuntText forKey:kStuntTextKey];
}

- (id)initWithCoder:(NSCoder *)decoder {
    [decoder decodeIntForKey:kStuntVersionKey];
    NSString *name = [decoder decodeObjectForKey:kStuntNameKey];
    NSString *stuntText = [decoder decodeObjectForKey:kStuntTextKey];
    return [self initWithName:name andStuntText:stuntText];
}

@end
