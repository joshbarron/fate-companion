//
//  FTCDetailSectionBox.h
//  Fate Companion
//
//  Created by Joshua Barron on 2/11/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "MGTableBox.h"

typedef void (^ IteratorBlock)(id, int);

@interface FTCDetailSectionBox : MGTableBox

@property (strong) NSString * sectionTitle;
@property (copy) void (^layoutBlock)(FTCDetailSectionBox *);
@property (copy) int (^lastLinesBorderBlock)(FTCDetailSectionBox *);

+ (id)boxWithSize:(CGSize)size andTitle:(NSString *)title andActionImage:(UIImage *)actionImage andActionCallback:(void (^)(id sender))actionCallback;

- (void) showEmptyMessage;
- (void) removeEmptyMessage;

@end
