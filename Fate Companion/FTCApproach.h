//
//  FTCApproach.h
//  Fate Companion
//
//  Created by Joshua Barron on 4/14/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FTCDiceRoller.h"

@interface FTCApproach : NSObject<NSCoding>

@property (strong) NSString * name;
@property (strong) NSNumber * value;
@property BOOL isCustom;

-(id) initWithName:(NSString *)name andValue:(NSNumber *)value isCustom:(BOOL)custom;

-(FTCDiceRollResult *)rollDice;

@end
