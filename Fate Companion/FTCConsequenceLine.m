//
//  FTCConsequenceLine.m
//  Fate Companion
//
//  Created by Joshua Barron on 3/6/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCConsequenceLine.h"
#import "FontAwesomeKit/FAKIonIcons.h"
#import "UIControl+BlocksKit.h"
#import "FTCLayoutHelper.h"
#import "SIAlertView.h"

#define HEIGHT 50

@implementation FTCConsequenceLine
@synthesize delegate = _delegate;
@synthesize consequence = _consequence;
@synthesize textField = _textField;
@synthesize lineWidth = _lineWidth;

+ (double) getInputWidth:(double)totalWidth
{
    return totalWidth - 35;
}

+ (FTCConsequenceLine *) consequenceLineWithConsequence:(FTCConsequence *)consequence delegate:(id<FTCConsequenceLineDelegate>)delegate
{
    double totalWidth = [FTCLayoutHelper getRowWidth:NO] - 8;
    
    double inputWidth = [FTCConsequenceLine getInputWidth:totalWidth];
    
    FAKIcon *deleteIcon = [FAKIonIcons ios7CloseOutlineIconWithSize:24];
    [deleteIcon addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]];
    UIImage *deleteIconImage = [deleteIcon imageWithSize:(CGSize){35, 30}];
    
    MGBox *imgBox = nil;
    UIButton *actionButton = nil;
    if (consequence.isOptional)
    {
        actionButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, deleteIconImage.size.width, deleteIconImage.size.height)];
        [actionButton setBackgroundImage:deleteIconImage forState:UIControlStateNormal];
        actionButton.showsTouchWhenHighlighted = YES;
        imgBox = [MGBox boxWithSize:actionButton.size];
        [imgBox addSubview:actionButton];
    }
    JVFloatLabeledTextField *textField = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(0, 0, inputWidth, HEIGHT)];
    NSString *placeholder = [consequence labelForType];
    [textField setPlaceholder:placeholder];
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.text = consequence.text;
    textField.font = [UIFont systemFontOfSize:16];
    
    FTCConsequenceLine *consequenceLine = [FTCConsequenceLine lineWithLeft:textField right:imgBox size:(CGSize){totalWidth, HEIGHT}];
    consequenceLine.delegate = delegate;
    consequenceLine.consequence = consequence;
    consequenceLine.textField = textField;
    consequenceLine.lineWidth = totalWidth;
    
    textField.delegate = consequenceLine;
    
    if (consequence.isOptional)
    {
        [actionButton bk_addEventHandler:^(id sender) {
            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Remove Consequence?" andMessage:@"Are you sure you wish to remove this consequence?"];
            //alertView.messageFont = [UIFont systemFontOfSize:24];
            
            [alertView addButtonWithTitle:@"Cancel"
                                     type:SIAlertViewButtonTypeDefault
                                  handler:nil];
            [alertView addButtonWithTitle:@"Remove" type:SIAlertViewButtonTypeDestructive handler:^(SIAlertView *alert) {
                [consequenceLine.delegate consequenceLineWasRemoved:consequenceLine];
            }];
            alertView.transitionStyle = SIAlertViewTransitionStyleFade;
            
            [alertView show];
        } forControlEvents:UIControlEventTouchUpInside];
    }
    return consequenceLine;
}

- (void) setup
{
    [super setup];
    
    self.borderStyle = MGBorderEtchedBottom;
    self.bottomBorderColor = [UIColor blackColor];
    
    self.rasterize = YES;
}

- (void) layout {
    [self setSize:(CGSize){self.lineWidth, HEIGHT}];
    [self.textField setSize:(CGSize){[FTCConsequenceLine getInputWidth:self.lineWidth], HEIGHT}];
    [super layout];
}

#pragma mark UITextFieldDelegate
- (void)textChanged:(UITextField *)textField
{
    [self.delegate consequenceLineWasChanged:self withText:textField.text];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self textChanged:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
    [self textChanged:textField];
	return YES;
}

@end
