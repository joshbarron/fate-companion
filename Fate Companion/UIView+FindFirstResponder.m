//
//  UIView+FindFirstResponder.m
//  Fate Companion
//
//  Created by Joshua Barron on 4/10/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "UIView+FindFirstResponder.h"

@implementation UIView (FindFirstResponder)
- (id)findFirstResponder
{
    if (self.isFirstResponder) {
        return self;
    }
    for (UIView *subView in self.subviews) {
        id responder = [subView findFirstResponder];
        if (responder) return responder;
    }
    return nil;
}
@end
