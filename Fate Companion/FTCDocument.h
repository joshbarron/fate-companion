//
//  FTCDocument.h
//  Fate Companion
//
//  Created by Joshua Barron on 1/25/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FTCSkill.h"
#import "FTCAspect.h"
#import "FTCStunt.h"
#import "FTCExtra.h"
#import "FTCStressTrack.h"
#import "FTCConsequence.h"
#import "FTCApproach.h"
#import "FTCCharacter.h"

@class FTCCharacter;
@class FTCCharacterMetadata;

#define FTC_EXTENSION @"ftc"

@interface FTCDocument : UIDocument

// Data
- (UIImage *)photo;
- (void)setPhoto:(UIImage *)photo;
- (NSString *)name;
- (void)setName:(NSString *)name;
- (NSString *)characterDescription;
- (void)setCharacterDescription:(NSString *)description;

- (CharacterType) characterType;
- (void)setCharacterType:(NSNumber *)type;

//Fate points and refresh
- (NSNumber *)fatePoints;
- (void)setFatePoints:(NSNumber *)value;
- (NSNumber *)refresh;
- (void)setRefresh:(NSNumber *)value;

//Aspects
- (FTCAspect *)highConcept;
- (void)setHighConcept:(NSString *)aspectText;
- (FTCAspect *)trouble;
- (void)setTrouble:(NSString *)aspectText;

- (NSArray *)aspects;
- (FTCAspect *)addAspectWithText:(NSString *)aspectText;
- (void)updateAspect:(FTCAspect *)aspect withText:(NSString *)aspectText;
- (void)removeAspect:(FTCAspect *)aspect;

//Approaches (FAE)
- (NSArray *)approaches;
- (NSArray *)approachesSortedByValue;
- (NSArray *)approachesSortedAlphabetically;
- (FTCApproach *)addApproachWithName:(NSString *)name andValue:(NSNumber *)value isCustom:(BOOL)isCustom;
- (void)addApproach:(FTCApproach *)approach;
- (void)removeApproach:(FTCApproach *)approach;
- (void)changeApproachName:(FTCApproach *)approach toName:(NSString *)newName;
- (void)changeApproachValue:(FTCApproach *)approach toValue:(NSNumber *)value;

//Skills
- (NSArray *)skills;
- (NSArray *)skillsSortedByValue;
- (NSArray *)skillsSortedAlphabetically;
- (FTCSkill *)addSkillWithName:(NSString *)name andValue:(NSNumber *)value isCustom:(BOOL)isCustom andActions:(SkillActions)actions;
- (void)addSkill:(FTCSkill *)skill;
- (void)removeSkill:(FTCSkill *)skill;
- (void)changeSkillWithName:(NSString *)name toNewName:(NSString *)newName;
- (void)changeSkillWithName:(NSString *)name toValue:(NSNumber *) value;
- (void)changeSkillWithName:(NSString *)name toActions:(NSNumber *) actions;

//Stunts
- (NSArray *)stunts;
- (FTCStunt *)addStuntWithName:(NSString *)name andStuntText:(NSString *)stuntText;
- (void)addStunt:(FTCStunt *)stunt;
- (void)changeStuntName:(FTCStunt *)stunt toName:(NSString *)newName;
- (void)changeStuntText:(FTCStunt *)stunt toText:(NSString *)newText;
- (void)removeStunt:(FTCStunt *)stunt;

//Extras
- (NSArray *)extras;
- (FTCExtra *)addExtraWithName:(NSString *)name;
- (void)addExtra:(FTCExtra *)extra;
- (void)changeExtraName:(FTCExtra *)extra toName:(NSString *)newName;
- (void)removeExtra:(FTCExtra *)extra;

//Stress
- (NSArray *)stressTracks;
- (NSArray *)stressTracksSortedByName;
- (FTCStressTrack *)addStressTrackWithName:(NSString *)name andSlots:(NSNumber *)slots;
- (void)addStressTrack:(FTCStressTrack *)stressTrack;
- (void)updateStressTrack:(FTCStressTrack *)stressTrack withName:(NSString *)newName andSlots:(NSNumber *)slots;
- (void)changeStressTrackBoxes:(FTCStressTrack *)stressTrack toStressBoxes:(NSNumber *)stressBoxes;
- (void)removeStressTrack:(FTCStressTrack *)stressTrack;

//Consequences
- (NSArray *)consequences;
- (NSArray *)consequencesSortedByType;
- (FTCConsequence *)addConsequenceWithType:(ConsequenceType)type isOptional:(BOOL)isOptional;
- (void) addConsequence:(FTCConsequence *)consequence;
- (void) updateConsequence:(FTCConsequence *)consequence withText:(NSString *)text;
- (void) removeConsequence:(FTCConsequence *)consequence;

// Metadata
@property (nonatomic, strong) FTCCharacterMetadata * metadata;

@end
