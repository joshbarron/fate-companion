//
//  FTCSkillDefinition.m
//  Fate Companion
//
//  Created by Joshua Barron on 2/21/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCSkillDefinition.h"

@implementation FTCSkillDefinition
@synthesize name = _name;
@synthesize actions = _actions;

- (id) initWithName:(NSString *)name andActions:(SkillActions) actions
{
    if (self = [super init])
    {
        self.name = name;
        self.actions = actions;
    }
    return self;
}

@end
