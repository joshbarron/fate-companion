//
//  FTCConsequence.m
//  Fate Companion
//
//  Created by Joshua Barron on 2/27/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCConsequence.h"

@implementation FTCConsequence
@synthesize type = _type;
@synthesize text = _text;
@synthesize isOptional = _isOptional;

- (id) initWithType:(ConsequenceType)type andText:(NSString *)text isOptional:(BOOL)optional
{
    if (self = [super init])
    {
        self.text = text;
        self.type = type;
        self.isOptional = optional;
    }
    return self;
}

- (NSString *)labelForType
{
    switch (self.type) {
        case ConsequenceMild:
            return @"Mild (2)";
        case ConsequenceModerate:
            return @"Moderate (4)";
        case ConsequenceSevere:
            return @"Severe (6)";
            
        default:
            return @"Unknown";
    }
}

#pragma mark NSCoding

#define kConsequenceVersionKey @"ConsequenceVersion"
#define kTextKey @"ConsequenceText"
#define kConsequenceTypeKey @"ConsequenceType"
#define kIsOptionalKey @"ConsequenceOptional"

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeInt:1 forKey:kConsequenceVersionKey];
    [encoder encodeObject:self.text forKey:kTextKey];
    [encoder encodeInt:self.type forKey:kConsequenceTypeKey];
    [encoder encodeBool:self.isOptional forKey:kIsOptionalKey];
}

- (id)initWithCoder:(NSCoder *)decoder {
    [decoder decodeIntForKey:kConsequenceVersionKey];
    NSString *text = [decoder decodeObjectForKey:kTextKey];
    ConsequenceType type = [decoder decodeIntForKey:kConsequenceTypeKey];
    BOOL isOptional = [decoder decodeBoolForKey:kIsOptionalKey];
    
    return [self initWithType:type andText:text isOptional:isOptional];
}

@end
