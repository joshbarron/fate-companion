//
//  FTCDocumentManager.h
//  Fate Companion
//
//  Created by Joshua Barron on 3/8/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FTCDocument.h"
#import "FTCCharacter.h"

@protocol FTCDocumentManagerDelegate

- (void)documentsAvailable:(NSArray *)urls;

@end

@interface FTCDocumentManager : NSObject

+ (FTCDocumentManager *) sharedManager;

- (NSURL *) iCloudRoot;
- (NSURL *)localRoot;

- (BOOL) iCloudAvailable;

//Settings related stuff
- (BOOL) iCloudOn;
- (void) setiCloudOn:(BOOL)on;
- (BOOL) iCloudWasOn;
- (void) setiCloudWasOn:(BOOL)on;
- (BOOL) iCloudPrompted;
- (void) setiCloudPrompted:(BOOL)prompted;

- (void)initializeiCloudAccessWithCompletion:(void (^)(BOOL available)) completion;


//Documents
- (NSURL *)getDocURL:(NSString *)filename;
- (void) createNewCharacterWithName:(NSString *)name andType:(CharacterType)characterType andFile:(NSString *)filename success:(void (^)(BOOL success, FTCDocument *document))completionHandler;
- (void)renameCurrentCharacterTo:(NSString *)name withHandler:(void (^)(BOOL success, NSString *validationMessage)) completionHandler;
- (void)deleteSelectedCharacter:(void (^)(BOOL success, NSString *validationMessage))completionHandler;

- (void) loadLocal:(id<FTCDocumentManagerDelegate>) delegate;

- (void) startQuery:(id<FTCDocumentManagerDelegate>) delegate;
- (void) stopQuery;
- (void) enableQueryUpdates;
- (void) disableQueryUpdates;

@property (strong) FTCDocument *currentDocument;

@end
