//
//  FTCCharacterMetadata.h
//  Fate Companion
//
//  Created by Joshua Barron on 1/25/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FTCCharacterMetadata : NSObject <NSCoding>

@property (strong) UIImage * thumbnail;
@property (strong) NSString * name;
@property (strong) NSString * description;

@end
