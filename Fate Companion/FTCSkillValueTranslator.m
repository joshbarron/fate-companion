//
//  FTCSkillValueTranslator.m
//  Fate Companion
//
//  Created by Joshua Barron on 2/15/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCSkillValueTranslator.h"

#define ACTION_OVERCOME @"o"
#define ACTION_CREATE_ADVANTAGE @"c"
#define ACTION_ATTACK @"a"
#define ACTION_DEFEND @"d"

@implementation FTCSkillValueTranslator

- (NSString *) actionsStringForActions:(SkillActions)actions
{
    NSMutableString *actionsString = [[NSMutableString alloc] init];
    
    if (actions & Overcome) {
        [actionsString appendString:ACTION_OVERCOME];
    }
    if (actions & CreateAdvantage) {
        [actionsString appendString:ACTION_CREATE_ADVANTAGE];
    }
    if (actions & Attack) {
        [actionsString appendString:ACTION_ATTACK];
    }
    if (actions & Defend) {
        [actionsString appendString:ACTION_DEFEND];
    }
    
    if (actionsString.length > 1)
    {
        [actionsString appendString:@" "];
    }
    
    return actionsString;
}

- (NSString *) valueKeyForValue:(int)value
{
    NSMutableString *key = [[NSMutableString alloc] initWithString:@"SKILL_LABEL_"];
    
    NSString *numAsString = [@(value) description];
    [key appendString:numAsString];
    return [[NSString alloc] initWithString:key];
}

- (NSString *) nameForSkillValue:(int)value
{
    NSString *valueKey = [self valueKeyForValue:value];
    NSString *valueName = [[NSUserDefaults standardUserDefaults] stringForKey:valueKey];
    
    if (valueName == nil)
        valueName = [self getDefaultNameForValue:value];
    
    return valueName;
}

- (NSString *) nameAndSkillValue:(int)value
{
    NSString *nameText = [self nameForSkillValue:value];
    
    NSMutableString *valueText = [[NSMutableString alloc] initWithString:nameText];
    
    [valueText appendString:@" ("];
    [valueText appendString:[@(value) description]];
    [valueText appendString:@")"];
    
    return [[NSString alloc] initWithString:valueText];
}

- (NSArray *) allSkillNamesAndValues
{
    NSMutableArray *skillNames = [[NSMutableArray alloc] init];
    for (int i = -2; i < 9; i++)
    {
        [skillNames addObject:[self nameAndSkillValue:i]];
    }
    
    return skillNames;
}

- (NSString *)getDefaultNameForValue:(int)value
{
    NSString *name = [self defaultLabelForValue:value];
    
    //Set the name in user defaults
    if (name != nil) {
        NSString *valueKey = [self valueKeyForValue:value];
        [[NSUserDefaults standardUserDefaults] setObject:name forKey:valueKey];
    }
    
    return name != nil ? name : @"Undefined";
}

- (NSString *)defaultLabelForValue:(int)value
{
    NSString *label;
    switch (value) {
        case -2:
            label = @"Terrible";
            break;
        case -1:
            label = @"Poor";
            break;
        case 0:
            label = @"Mediocre";
            break;
        case 1:
            label = @"Average";
            break;
        case 2:
            label = @"Fair";
            break;
        case 3:
            label = @"Good";
            break;
        case 4:
            label = @"Great";
            break;
        case 5:
            label = @"Superb";
            break;
        case 6:
            label = @"Fantastic";
            break;
        case 7:
            label = @"Epic";
            break;
        case 8:
            label = @"Legendary";
            break;
        default:
            if (value > 8)
                label = @"Unbelievable";
            else
                label = @"Horrible";
            break;
    }
    return label;
}

@end
