//
//  FTCSkill.h
//  Fate Companion
//
//  Created by Joshua Barron on 2/16/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FTCDiceRoller.h"

typedef NS_OPTIONS(NSInteger, SkillActions) {
    Overcome = 1 << 0,
    CreateAdvantage = 1 << 1,
    Attack = 1 << 2,
    Defend = 1 << 3
};

@interface FTCSkill : NSObject<NSCoding>

@property (strong) NSString * name;
@property (strong) NSNumber * value;
@property BOOL isCustom;

@property SkillActions actions;

-(id) initWithName:(NSString *)name andValue:(NSNumber *)value isCustom:(BOOL)custom andActions:(SkillActions)actions;

- (FTCDiceRollResult *)rollDice;

@end
