//
//  FTCStressTrackEntryViewController.h
//  Fate Companion
//
//  Created by Joshua Barron on 2/28/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FTCStressTrack.h"
#import "JVFloatLabeledTextField.h"
#import "MGScrollView.h"

@class FTCStressTrackEntryViewController;

@protocol FTCStressTrackEntryDelegate

- (BOOL) validateStressTrackEntry:(FTCStressTrackEntryViewController *)stressTrackEntryController withMessage:(NSString * __autoreleasing *)validationMessage;
- (void) stressTrackWasSaved:(FTCStressTrackEntryViewController *)stressTrackEntryController;

@end

@interface FTCStressTrackEntryViewController : UIViewController<UITextFieldDelegate>
- (IBAction)cancelButtonWasTapped:(id)sender;
- (IBAction)saveButtonWasTapped:(id)sender;

@property (weak, nonatomic) IBOutlet MGScrollView *scroller;
@property (strong, nonatomic) JVFloatLabeledTextField *nameTextField;
@property (strong, nonatomic) UIStepper *stepper;

@property int selectedNumberOfBoxes;

@property (weak) FTCStressTrack *stressTrack;
@property (weak) id<FTCStressTrackEntryDelegate> delegate;


@end
