//
//  FTCDocumentManager.m
//  Fate Companion
//
//  Created by Joshua Barron on 3/8/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCDocumentManager.h"


@interface FTCDocumentManager()
{
    NSURL *_iCloudRoot, *_localRoot;
    BOOL _iCloudAvailable;
    NSMetadataQuery * _query;
    NSMutableArray * _knownURLs;
    id<FTCDocumentManagerDelegate> _delegate;
}
@end

@implementation FTCDocumentManager
@synthesize currentDocument = _currentDocument;

+ (FTCDocumentManager *) sharedManager
{
    static FTCDocumentManager *_sharedInstance = nil;
    
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[FTCDocumentManager alloc] init];
    });
    
    return _sharedInstance;
}

- (id) init
{
    if (self = [super init])
    {
        _knownURLs = [[NSMutableArray alloc] init];
    }
    return self;
}

- (NSURL *) iCloudRoot
{
    return _iCloudRoot;
}

- (NSURL *)localRoot {
    if (_localRoot != nil) {
        return _localRoot;
    }
    
    NSArray * paths = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    _localRoot = [paths objectAtIndex:0];
    return _localRoot;
}

- (BOOL) iCloudAvailable
{
    return _iCloudAvailable;
}

- (void)initializeiCloudAccessWithCompletion:(void (^)(BOOL available)) completion {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        _iCloudRoot = [[NSFileManager defaultManager] URLForUbiquityContainerIdentifier:nil];
        if (_iCloudRoot != nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"iCloud available at: %@", _iCloudRoot);
                _iCloudAvailable = YES;
                completion(TRUE);
            });
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"iCloud not available");
                _iCloudAvailable = NO;
                completion(FALSE);
            });
        }
    });
}

#pragma mark Settings Related
- (BOOL)iCloudOn {
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"iCloudOn"];
}

- (void)setiCloudOn:(BOOL)on {
    [[NSUserDefaults standardUserDefaults] setBool:on forKey:@"iCloudOn"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)iCloudWasOn {
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"iCloudWasOn"];
}

- (void)setiCloudWasOn:(BOOL)on {
    [[NSUserDefaults standardUserDefaults] setBool:on forKey:@"iCloudWasOn"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)iCloudPrompted {
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"iCloudPrompted"];
}

- (void)setiCloudPrompted:(BOOL)prompted {
    [[NSUserDefaults standardUserDefaults] setBool:prompted forKey:@"iCloudPrompted"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSMetadataQuery *)documentQuery {
    NSMetadataQuery * query = [[NSMetadataQuery alloc] init];
    if (query) {
        
        // Search documents subdir only
        [query setSearchScopes:[NSArray arrayWithObject:NSMetadataQueryUbiquitousDocumentsScope]];
        
        // Add a predicate for finding the documents
        NSString * filePattern = [NSString stringWithFormat:@"*.%@", FTC_EXTENSION];
        [query setPredicate:[NSPredicate predicateWithFormat:@"%K LIKE %@",
                             NSMetadataItemFSNameKey, filePattern]];
        
    }
    return query;
}

#pragma mark Queries

- (void) startQuery:(id<FTCDocumentManagerDelegate>) delegate
{
    [self stopQuery];
    
    NSLog(@"Starting to watch iCloud dir...");
    
    _query = [self documentQuery];
    
    _delegate = delegate;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(processiCloudFiles:)
                                                 name:NSMetadataQueryDidFinishGatheringNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(processiCloudFiles:)
                                                 name:NSMetadataQueryDidUpdateNotification
                                               object:nil];
    
    [_query startQuery];
}

- (void) stopQuery
{
    if (_query) {
        
        NSLog(@"No longer watching iCloud dir...");
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NSMetadataQueryDidFinishGatheringNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NSMetadataQueryDidUpdateNotification object:nil];
        [_query stopQuery];
        _query = nil;
    }
}

- (void)loadLocal:(id<FTCDocumentManagerDelegate>) delegate {
    
    NSArray * localDocuments = [[NSFileManager defaultManager] contentsOfDirectoryAtURL:[self localRoot]includingPropertiesForKeys:nil options:0 error:nil];
    NSLog(@"Found %d local files.", localDocuments.count);
    
    NSArray *sorted = [localDocuments sortedArrayUsingComparator:^NSComparisonResult(NSURL *a, NSURL *b){
        return [[a lastPathComponent] compare:[b lastPathComponent]];
    }];
    
    [delegate documentsAvailable:sorted];
}

- (void)processiCloudFiles:(NSNotification *)notification {
    
    // Always disable updates while processing results
    [self disableQueryUpdates];
    
    [_knownURLs removeAllObjects];
    
    // The query reports all files found, every time.
    NSArray * queryResults = [_query results];
    for (NSMetadataItem * result in queryResults) {
        NSURL * fileURL = [result valueForAttribute:NSMetadataItemURLKey];
        NSNumber * aBool = nil;
        
        // Don't include hidden files
        [fileURL getResourceValue:&aBool forKey:NSURLIsHiddenKey error:nil];
        if (aBool && ![aBool boolValue]) {
            [_knownURLs addObject:fileURL];
        }
        
    }
    
    NSLog(@"Found %d iCloud files.", _knownURLs.count);
    
    NSArray *sorted = [_knownURLs sortedArrayUsingComparator:^NSComparisonResult(NSURL *a, NSURL *b){
        return [[a lastPathComponent] compare:[b lastPathComponent]];
    }];
    
    if (_delegate) {
        [_delegate documentsAvailable:sorted];
    }
    
    [self enableQueryUpdates];
    
}

- (void) enableQueryUpdates
{
    if (_query) {
        [_query enableUpdates];
    }
}

- (void) disableQueryUpdates
{
    if (_query) {
        [_query disableUpdates];
    }
}

#pragma mark Documents

- (NSURL *)getDocURL:(NSString *)filename {
    if ([self iCloudOn]) {
        NSURL * docsDir = [[self iCloudRoot] URLByAppendingPathComponent:@"Documents" isDirectory:YES];
        return [docsDir URLByAppendingPathComponent:filename];
    } else {
        return [self.localRoot URLByAppendingPathComponent:filename];
    }
}

- (void)createNewCharacterWithName:(NSString *)name andType:(CharacterType)characterType andFile:(NSString *)filename success:(void (^)(BOOL success, FTCDocument *document))completionHandler
{
    NSURL *fileURL = [self getDocURL:filename];
    
    NSLog(@"Want to create file at %@", fileURL);
    
    [self disableQueryUpdates];
    // Create new document and save to the filename
    FTCDocument * doc = [[FTCDocument alloc] initWithFileURL:fileURL];
    [doc setName:name];
    
    //Set type
    [doc setCharacterType:[NSNumber numberWithInt:characterType]];
    
    //If FAE, add the default approaches
    if (characterType == FateAcceleratedCharacter) {
        [doc addApproachWithName:@"Careful" andValue:[NSNumber numberWithInt:0] isCustom:NO];
        [doc addApproachWithName:@"Clever" andValue:[NSNumber numberWithInt:0] isCustom:NO];
        [doc addApproachWithName:@"Flashy" andValue:[NSNumber numberWithInt:0] isCustom:NO];
        [doc addApproachWithName:@"Forceful" andValue:[NSNumber numberWithInt:0] isCustom:NO];
        [doc addApproachWithName:@"Quick" andValue:[NSNumber numberWithInt:0] isCustom:NO];
        [doc addApproachWithName:@"Sneaky" andValue:[NSNumber numberWithInt:0] isCustom:NO];
    }
    
    //Add the default stress tracks
    if (characterType == FateCoreCharacter) {
        [doc addStressTrackWithName:@"Physical" andSlots:[NSNumber numberWithInt:2]];
        [doc addStressTrackWithName:@"Mental" andSlots:[NSNumber numberWithInt:2]];
    } else {
        [doc addStressTrackWithName:@"Stress" andSlots:[NSNumber numberWithInt:3]];
    }
    //Add the default consequences
    [doc addConsequenceWithType:ConsequenceMild isOptional:NO];
    [doc addConsequenceWithType:ConsequenceModerate isOptional:NO];
    [doc addConsequenceWithType:ConsequenceSevere isOptional:NO];
    [doc saveToURL:fileURL forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success) {
        completionHandler(success, doc);
    }];
    
    [self enableQueryUpdates];
}

- (void)renameCurrentCharacterTo:(NSString *)name withHandler:(void (^)(BOOL success, NSString *validationMessage)) completionHandler
{
    if (self.currentDocument == nil)
    {
        completionHandler(NO, @"Error locating document.");
        return;
    }
    
    if ([self.currentDocument.name isEqualToString:name])
    {
        completionHandler(NO, nil);
        return;
    }
    
    if (!name.length)
    {
        completionHandler(NO, @"Your character cannot have an empty name.");
        return;
    }
    
    NSString *newFilename = [NSString stringWithFormat:@"%@.%@", name, FTC_EXTENSION];
    if ([self documentExistsWithName:newFilename]) {
        NSString *msg = [NSString stringWithFormat:@"There is already a character named %@.", name];
        completionHandler(NO, msg);
        return;
    }
    
    NSURL *newDocURL = [self getDocURL:newFilename];
    NSLog(@"Moving %@ to %@", self.currentDocument.fileURL, newDocURL);
    
    // Rename by saving/deleting - hack?
    NSURL *origURL = self.currentDocument.fileURL;
    FTCDocument * doc = self.currentDocument;
    
    [doc openWithCompletionHandler:^(BOOL success) {
        if (!success)
            NSLog(@"ALERT ALERT ALERT");
        [doc setName:name];
        [doc saveToURL:newDocURL forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success) {
            NSLog(@"Doc saved to %@", newDocURL);
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
                NSFileCoordinator* fileCoordinator = [[NSFileCoordinator alloc] initWithFilePresenter:nil];
                [fileCoordinator coordinateWritingItemAtURL:origURL
                                                    options:NSFileCoordinatorWritingForDeleting
                                                      error:nil
                                                 byAccessor:^(NSURL* writingURL) {
                                                     NSFileManager* fileManager = [[NSFileManager alloc] init];
                                                     [fileManager removeItemAtURL:writingURL error:nil];
                                                 }];
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"Doc deleted at %@", origURL);
                    completionHandler(YES, nil);
                });
            });
        }];
    }];
}


- (void)deleteSelectedCharacter:(void (^)(BOOL success, NSString *validationMessage))completionHandler {
    // Wrap in file coordinator
    if (self.currentDocument == nil)
        return;
    
    NSURL *docURL = [self.currentDocument.fileURL copy];
    [self.currentDocument closeWithCompletionHandler:^(BOOL s) {
        if (!s) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completionHandler(NO, @"Could not close document.");
            });
        }
        else {
            [self disableQueryUpdates];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
                NSFileCoordinator* fileCoordinator = [[NSFileCoordinator alloc] initWithFilePresenter:nil];
                [fileCoordinator coordinateWritingItemAtURL:docURL
                                                    options:NSFileCoordinatorWritingForDeleting
                                                      error:nil
                                                 byAccessor:^(NSURL* writingURL) {
                                                     // Simple delete to start
                                                     NSFileManager* fileManager = [[NSFileManager alloc] init];
                                                     [fileManager removeItemAtURL:docURL error:nil];
                                                 }];
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.currentDocument = nil;
                    completionHandler(YES, @"Character deleted.");
                });
                [self enableQueryUpdates];
            });
        }
    }];
}

- (BOOL) documentExistsWithName:(NSString *)fileName
{
    for (NSURL *url in _knownURLs) {
        if ([[url lastPathComponent] isEqualToString:fileName])
            return YES;
    }
    return NO;
}

@end
