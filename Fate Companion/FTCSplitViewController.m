//
//  FTCSplitViewController.m
//  Fate Companion
//
//  Created by Joshua Barron on 3/15/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCSplitViewController.h"

@interface FTCSplitViewController ()

@end

@implementation FTCSplitViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        _masterViewController = self.viewControllers[0];
        _detailViewController = self.viewControllers[1];
    }
    return self;
}

- (void)changeDetailViewController:(UINavigationController *)target
{
    if (_detailViewController != target) {
        _detailViewController = target;
        self.delegate = (id<UISplitViewControllerDelegate>)(target.visibleViewController);
        self.viewControllers = [NSArray arrayWithObjects:_masterViewController, target, nil];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.delegate = (id<UISplitViewControllerDelegate>)((UINavigationController *)_detailViewController).visibleViewController;
    [self.view bringSubviewToFront:self.detailViewController.view];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

@end
