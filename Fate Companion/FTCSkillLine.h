//
//  FTCSkillLine.h
//  Fate Companion
//
//  Created by Joshua Barron on 2/15/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "MGLine.h"
#import "FTCSkill.h"

@class FTCSkillLine;

@protocol FTCSkillLineDelegate

- (void) skillLineWasRemoved:(FTCSkillLine *)skillLine;
- (void) rollDiceForSkillLine:(FTCSkillLine *)skillLine;
- (void) skillLineWasTapped:(FTCSkillLine *)skillLine;

@end

@interface FTCSkillLine : MGLine

+ (FTCSkillLine *)skillLineWithSkill:(FTCSkill *)skill delegate:(id<FTCSkillLineDelegate>)delegate;

@property (weak) id<FTCSkillLineDelegate> delegate;
@property (weak) FTCSkill *skill;

@property double lineWidth;

@end
