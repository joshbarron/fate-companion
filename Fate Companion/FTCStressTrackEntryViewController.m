//
//  FTCStressTrackEntryViewController.m
//  Fate Companion
//
//  Created by Joshua Barron on 2/28/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCStressTrackEntryViewController.h"
#import "MGTableBox.h"
#import "MGLine.h"
#import "MGBox.h"
#import "CRToast.h"
#import "FTCNotificationHelper.h"
#import "FTCLayoutHelper.h"

#define MAX_STRESS_BOXES 5
#define TAG_NAMEFIELD 5001
#define TAG_NUMBOXES_FIELD 5002
#define TAG_NUMBOXES_LINE 5003

@interface FTCStressTrackEntryViewController ()
{
    NSArray *stressBoxCounts;
    MGTableBox *masterTableBox;
}
@end

@implementation FTCStressTrackEntryViewController
@synthesize scroller = _scroller;
@synthesize nameTextField = _nameTextField;
@synthesize stepper = _stepper;
@synthesize selectedNumberOfBoxes = _selectedNumberOfBoxes;
@synthesize stressTrack = _stressTrack;
@synthesize delegate = _delegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    stressBoxCounts = [NSArray arrayWithObjects:@"1", @"2", @"3", @"4", @"5", nil];
    
    if (self.stressTrack != nil)
    {
        self.selectedNumberOfBoxes = [self.stressTrack.slots intValue];
    }
    else
    {
        self.selectedNumberOfBoxes = 2;
    }
    
    self.scroller.contentLayoutMode = MGLayoutGridStyle;
    self.scroller.scrollEnabled = NO;
    if ([FTCLayoutHelper deviceIsIPad]) {
        self.scroller.keepFirstResponderAboveKeyboard = NO;
    }
    masterTableBox = [MGTableBox boxWithSize:(CGSize){320, 0}];
    
    [self.scroller.boxes addObject:masterTableBox];
    
    MGLine *instructions = [MGLine lineWithMultilineLeft:@"Tap on the fields below to edit this stress track's name and number of boxes:" right:nil width:280 minHeight:20];
    instructions.leftMargin = instructions.rightMargin = 20;
    instructions.bottomMargin = instructions.topMargin = 5;
    [masterTableBox.topLines addObject:instructions];
    
    self.nameTextField = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(0, 0, 260, 40)];
    [self.nameTextField setPlaceholder:@"Stress Track Name"];
    self.nameTextField.font = [UIFont boldSystemFontOfSize:20];
    self.nameTextField.tag = TAG_NAMEFIELD;
    self.nameTextField.clearButtonMode = UITextFieldViewModeAlways;
    self.nameTextField.delegate = self;
    if (self.stressTrack != nil)
    {
        self.nameTextField.text = self.stressTrack.name;
    }
    MGLine *nameLine = [MGLine lineWithLeft:self.nameTextField right:nil size:(CGSize){260, 40}];
    nameLine.leftMargin = nameLine.rightMargin = 30;
    nameLine.bottomPadding = 5;
    //nameLine.borderStyle = MGBorderEtchedBottom;
    //nameLine.bottomBorderColor = [UIColor blackColor];
    [masterTableBox.topLines addObject:nameLine];
    
    MGLine *valueHeaderLine = [MGLine lineWithLeft:@"Number of Stress Track Boxes" right:nil size:(CGSize){260, 20}];
    valueHeaderLine.font = [UIFont systemFontOfSize:12];
    valueHeaderLine.leftItemsAlignment = NSTextAlignmentCenter;
    valueHeaderLine.leftMargin = valueHeaderLine.rightMargin = 20;
    valueHeaderLine.topMargin = 5;
    
    [masterTableBox.topLines addObject:valueHeaderLine];
    
    NSString *numBoxes = [NSString stringWithFormat:@"%d", self.selectedNumberOfBoxes];
    
    MGLine *valueLine = [MGLine lineWithLeft:numBoxes right:nil size:(CGSize){260, 30}];
    valueLine.font = [UIFont boldSystemFontOfSize:30];
    valueLine.leftItemsAlignment = NSTextAlignmentCenter;
    valueLine.leftMargin = valueLine.rightMargin = 20;
    valueLine.topMargin = 5;
    valueLine.tag = TAG_NUMBOXES_LINE;
    
    [masterTableBox.topLines addObject:valueLine];
    
    self.stepper = [[UIStepper alloc] initWithFrame:CGRectMake(0, 10, 0, 0)];
    [self.stepper addTarget:self action:@selector(stepperValueChanged:) forControlEvents:UIControlEventValueChanged];
    self.stepper.value = self.selectedNumberOfBoxes;
    self.stepper.minimumValue = 1;
    self.stepper.maximumValue = MAX_STRESS_BOXES;
    self.stepper.stepValue = 1;
    double x = 130 - (self.stepper.width / 2);
    [self.stepper setX:x];
    
    MGBox *stepperLine = [MGBox boxWithSize:(CGSize){260, 35}];
    [stepperLine addSubview:self.stepper];
    stepperLine.leftMargin = stepperLine.rightMargin = 20;
//    stepperLine.bottomMargin = stepperLine.topMargin = 5;
    
    [masterTableBox.topLines addObject:stepperLine];
    
    [self.scroller layout];
    [self.scroller scrollToView:masterTableBox withMargin:0];

}

#pragma mark UIStepper

- (void)stepperValueChanged:(UIStepper *) stepper
{
    MGLine *valueLine = (MGLine *)[masterTableBox viewWithTag:TAG_NUMBOXES_LINE];
    self.selectedNumberOfBoxes = (int)stepper.value;
    [valueLine setMultilineLeft:[NSString stringWithFormat:@"%d", self.selectedNumberOfBoxes]];
    [valueLine layout];
}

#pragma mark UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField.tag == TAG_NAMEFIELD)
    {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 15) ? NO : YES;
    }
    return YES;
}

- (void)textChanged:(UITextField *)textField {
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
    [self textChanged:textField];
	return YES;
}

- (IBAction)cancelButtonWasTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveButtonWasTapped:(id)sender {
    NSString *name = self.nameTextField.text;
    
    if (name == nil || name.length == 0)
    {
        [CRToastManager showNotificationWithOptions:[FTCNotificationHelper warningNotification:@"Oops!" andSubtitle:@"Please enter a name for your stress track."] completionBlock:nil];
        return;
    }
    
    NSString *validationMessage = nil;
    if (![self.delegate validateStressTrackEntry:self withMessage:&validationMessage])
    {
        [CRToastManager showNotificationWithOptions:[FTCNotificationHelper warningNotification:@"Oops!" andSubtitle:validationMessage] completionBlock:nil];
        return;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.delegate stressTrackWasSaved:self];
}


@end
