//
//  UIColor+LightAndDark.h
//  Fate Companion
//
//  Created by Joshua Barron on 2/1/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (LightAndDark)

- (UIColor *)lighterColor;
- (UIColor *)darkerColor;

@end
