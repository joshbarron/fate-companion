//
//  FTCSkillDefinition.h
//  Fate Companion
//
//  Created by Joshua Barron on 2/21/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "FTCSkill.h"

@interface FTCSkillDefinition : NSObject

@property (strong) NSString * name;
@property SkillActions actions;

- (id) initWithName:(NSString *)name andActions:(SkillActions) actions;

@end
