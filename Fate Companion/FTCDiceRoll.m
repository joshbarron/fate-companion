//
//  FTCDiceRoll.m
//  Fate Companion
//
//  Created by Joshua Barron on 3/14/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCDiceRoll.h"

@implementation FTCDiceRoll
@synthesize rollTitle = _rollTitle;
@synthesize rollResult = _rollResult;
@synthesize oppositionRollResult = _oppositionRollResult;

- (BOOL) hasRoll
{
    return self.rollResult != nil;
}

- (BOOL) hasOpposition
{
    return self.oppositionRollResult != nil;
}

- (int) calculateOutcome
{
    if ([self hasRoll])
    {
        if ([self hasOpposition]) {
            return self.rollResult.finalResult - self.oppositionRollResult.finalResult;
        }
        return self.rollResult.finalResult;
    }
    return 0;
}

- (id) initWithTitle:(NSString *)rollTitle andRollResult:(FTCDiceRollResult *)rollResult andOppositionRollResult:(FTCDiceRollResult *)oppositionRollResult
{
    if (self = [super init])
    {
        self.rollTitle = rollTitle;
        self.rollResult = rollResult;
        self.oppositionRollResult = oppositionRollResult;
    }
    return self;
}

#pragma mark NSCoding

#define kDiceRollVersionKey @"DiceRollVersionKey"
#define kRollTitleKey @"RollTitle"
#define kRollResultKey @"RollResult"
#define kOppositionRollResultKey @"OppositionRollResult"

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeInt:1 forKey:kDiceRollVersionKey];
    [encoder encodeObject:self.rollTitle forKey:kRollTitleKey];
    [encoder encodeObject:self.rollResult forKey:kRollResultKey];
    [encoder encodeObject:self.oppositionRollResult forKey:kOppositionRollResultKey];
}

- (id)initWithCoder:(NSCoder *)decoder {
    [decoder decodeIntForKey:kDiceRollVersionKey];
    NSString *rollTitle = [decoder decodeObjectForKey:kRollTitleKey];
    FTCDiceRollResult *rollResult = [decoder decodeObjectForKey:kRollResultKey];
    FTCDiceRollResult *oppositionRollResult = [decoder decodeObjectForKey:kOppositionRollResultKey];
    
    return [self initWithTitle:rollTitle andRollResult:rollResult andOppositionRollResult:oppositionRollResult];
}

@end
