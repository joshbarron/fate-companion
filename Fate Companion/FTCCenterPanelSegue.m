//
//  FTCCenterPanelSegue.m
//  Fate Companion
//
//  Created by Joshua Barron on 2/8/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCCenterPanelSegue.h"
#import "FTCSidePanelControllerViewController.h"
#import "UIViewController+JASidePanel.h"

@implementation FTCCenterPanelSegue

@synthesize panelName = _panelName;
//@synthesize setDelegate = _setDelegate;

- (void) perform
{
    UIViewController *viewController = (UIViewController *)self.sourceViewController;
    
    if (self.panelName != nil) {
        [viewController.sidePanelController setCenterPanel:[viewController.storyboard instantiateViewControllerWithIdentifier:self.panelName]];
    }
    else {
        [viewController.sidePanelController setCenterPanel:self.destinationViewController];
    }
    
    [viewController.sidePanelController showCenterPanelAnimated:YES];
//    if (self.setDelegate)
//    {
//        [(id)viewController.sidePanelController.centerPanel setDelegate:viewController];
//    }
}


@end
