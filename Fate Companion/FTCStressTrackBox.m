//
//  FTCStressTrackBox.m
//  Fate Companion
//
//  Created by Joshua Barron on 2/28/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCStressTrackBox.h"
#import "UIControl+BlocksKit.h"

#define GLYPH_FONT @"FateCoreGlyphs"

#define STRESS_UNUSED @"b"
#define STRESS_USED @"."

#define GLYPH_SIZE 30
#define NUMBER_SIZE 20

#define BOX_SIZE (CGSize){50, 20}
#define BUTTON_RECT (CGRect){0, 0, BOX_SIZE.width, BOX_SIZE.height}

@interface FTCStressTrackBox()

@property UIButton *button;

@end

@implementation FTCStressTrackBox

@synthesize delegate = _delegate;
@synthesize index = _index;
@synthesize isUsed = _isUsed;
@synthesize button = _button;
@synthesize isEnabled = _isEnabled;

+ (FTCStressTrackBox *)stressTrackBoxWithIndex:(int)index andState:(BOOL)isUsed enabled:(BOOL)isEnabled delegate:(FTCStressTrackLine *)delegate
{
    FTCStressTrackBox *stressBox = [FTCStressTrackBox boxWithSize:BOX_SIZE];
    stressBox.index = index;
    stressBox.isUsed = isUsed;
    stressBox.isEnabled = isEnabled;
    stressBox.delegate = delegate;
    [stressBox setup];
    return stressBox;
}

- (void) setup
{
    [super setup];
    
    if (self.delegate == nil)
        return;
    
    if (self.button == nil)
    {
        self.button = [UIButton buttonWithType:UIButtonTypeCustom];
    }
    
    
    UIFont *glyphFont = [UIFont fontWithName:GLYPH_FONT size:16];
    UIFont *font = [UIFont systemFontOfSize:16];
    UIColor *fontColor = self.isEnabled ? [UIColor blackColor] : [UIColor lightGrayColor];

    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    NSDictionary *commonAttrs = @{NSFontAttributeName: [glyphFont fontWithSize:GLYPH_SIZE],
                                  NSForegroundColorAttributeName: fontColor,
                                  NSParagraphStyleAttributeName: paragraphStyle,
                                  NSBaselineOffsetAttributeName : @8,
                                  NSTextEffectAttributeName : NSTextEffectLetterpressStyle
                                  };
    NSDictionary *superscriptAttrs = @{NSFontAttributeName : [font fontWithSize:NUMBER_SIZE],
                                       NSBaselineOffsetAttributeName : @16,
                                       NSForegroundColorAttributeName: fontColor,
                                       NSKernAttributeName : @(1.3f)};
    
    
    NSMutableAttributedString *unusedText = [[NSMutableAttributedString alloc] initWithString:[self getStringForButtonState:NO] attributes: commonAttrs];
    [unusedText setAttributes:superscriptAttrs range:NSMakeRange(0, 1)];
    
    NSMutableAttributedString *usedText = [[NSMutableAttributedString alloc] initWithString:[self getStringForButtonState:YES] attributes:commonAttrs];
    [usedText setAttributes:superscriptAttrs range:NSMakeRange(0, 1)];
    //[usedText setAttributes:@{NSForegroundColorAttributeName : tintColor} range:NSMakeRange(0, 2)];
    
    self.button.frame = BUTTON_RECT;
    self.button.titleLabel.numberOfLines = 2;
    
    
    [self.button setAttributedTitle:unusedText forState:UIControlStateNormal];
    [self.button setAttributedTitle:usedText forState:UIControlStateSelected];
    
    if (self.isEnabled)
    {
        [self.button bk_addEventHandler:^(id sender) {
            UIButton *button = (UIButton *)sender;
            button.selected = !button.selected;
            [self.delegate.delegate stressTrackBoxStateChanged:self.delegate withIndex:self.index];
        } forControlEvents:UIControlEventTouchUpInside];
    }
    self.button.selected = self.isUsed;
    
    if (![self.subviews containsObject:self.button])
    {
        [self addSubview:self.button];
    }
    
    self.leftMargin = 2;
}

- (NSString *)getStringForState:(BOOL)isUsed
{
    if (isUsed)
        return STRESS_USED;
    return STRESS_UNUSED;
}

- (NSString *)getStringForIndex
{
    return [NSString stringWithFormat:@"%d", self.index + 1];
}

- (NSString *)getStringForButtonState:(BOOL)isUsed
{
    NSMutableString *str = [NSMutableString stringWithString:[self getStringForIndex]];
    [str appendString:[self getStringForState:isUsed]];
    return str;
}


@end
