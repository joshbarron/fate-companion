//
//  FTCCreateCharacterViewController.h
//  Fate Companion
//
//  Created by Joshua Barron on 4/12/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGScrollView.h"
#import "FTCCharacter.h"

@class FTCCreateCharacterViewController;

@protocol FTCCreateCharacterDelegate

- (void) characterWasCreatedWithName:(NSString *)name andType:(CharacterType)type;

@end

@interface FTCCreateCharacterViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet MGScrollView *scroller;

@property (weak) id<FTCCreateCharacterDelegate> delegate;

@end
