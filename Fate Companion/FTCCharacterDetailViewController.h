//
//  FTCCharacterDetailViewController.h
//  Fate Companion
//
//  Created by Joshua Barron on 1/25/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FTCOperationResult.h"
#import "MGScrollView.h"
#import "FTCAspectLine.h"
#import "FTCApproachLine.h"
#import "FTCSkillLine.h"
#import "FTCStuntLine.h"
#import "FTCExtraLine.h"
#import "FTCStressTrackLine.h"
#import "FTCConsequenceLine.h"
#import "FTCSkillEntryViewController.h"
#import "FTCStressTrackEntryViewController.h"
#import "FTCApproachEntryViewController.h"
#import "RMPickerViewController.h"
#import "FTCUserSettings.h"
#import "FTCDocumentManager.h"
#import "BNHtmlPdfKit.h"

@class FTCDocument;
@class FTCCharacterDetailViewController;

@protocol FTCCharacterDetailViewControllerDelegate
- (void)detailViewControllerDidClose:(FTCCharacterDetailViewController *)detailViewController;
- (void)detailViewControllerDidDelete:(FTCCharacterDetailViewController *)detailViewController;
@end

@interface FTCCharacterDetailViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, UITextFieldDelegate, UITextViewDelegate, UIScrollViewDelegate, RMPickerViewControllerDelegate, BNHtmlPdfKitDelegate,
    FTCAspectLineDelegate, FTCApproachLineDelegate, FTCApproachEntryDelegate, FTCSkillLineDelegate, FTCSkillEntryDelegate, FTCStuntLineDelegate, FTCExtraLineDelegate, FTCStressTrackLineDelegate, FTCStressTrackEntryDelegate, FTCConsequenceLineDelegate>

@property (strong, nonatomic) FTCDocument * doc;

@property (weak, nonatomic) IBOutlet MGScrollView *scroller;
@property (strong) id <FTCCharacterDetailViewControllerDelegate> delegate;

- (IBAction)iPadDoneButtonTapped:(id)sender;

@end
