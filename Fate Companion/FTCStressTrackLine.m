//
//  FTCStressTrackLine.m
//  Fate Companion
//
//  Created by Joshua Barron on 2/28/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCStressTrackLine.h"
#import "FontAwesomeKit/FAKIonIcons.h"
#import "FTCStressTrackBox.h"
#import "UIControl+BlocksKit.h"
#import "FTCUserSettings.h"
#import "FTCLayoutHelper.h"
#import "SIAlertView.h"

#define MAX_STRESS_BOXES 5

#define NAME_HEIGHT 15
#define BOX_HEIGHT 40

@interface FTCStressTrackLine()

@property NSArray *stressTrackBoxes;

@property MGLine *nameLine;
@property MGLine *boxLine;
@property MGBox *mainBox;

@end

@implementation FTCStressTrackLine
@synthesize stressTrack = _stressTrack;
@synthesize delegate = _delegate;
@synthesize stressTrackBoxes = _stressTrackBoxes;
@synthesize lineWidth = _lineWidth;
@synthesize nameLine = _nameLine;
@synthesize boxLine = _boxLine;
@synthesize mainBox = _mainBox;

+ (double) getInputWidth:(double)totalWidth
{
    return totalWidth - 35;
}

+ (FTCStressTrackLine *) stressTrackLineWithStressTrack:(FTCStressTrack *)stressTrack delegate:(id<FTCStressTrackLineDelegate>)delegate
{
//    CGSize mainSize = CGSizeMake(width - 35, 15);
    double totalWidth = [FTCLayoutHelper getRowWidth:NO] - 8;
    
    double inputWidth = [FTCStressTrackLine getInputWidth:totalWidth];
    
    FAKIcon *settingsIcon = [FAKIonIcons ios7GearOutlineIconWithSize:24];
    [settingsIcon addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor]];
    UIImage *settingsIconImage = [settingsIcon imageWithSize:(CGSize){35, 25}];
    
    UIButton *settingsButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, settingsIconImage.size.width, settingsIconImage.size.height)];
    [settingsButton setBackgroundImage:settingsIconImage forState:UIControlStateNormal];
    settingsButton.showsTouchWhenHighlighted = YES;
    
    FAKIcon *deleteIcon = [FAKIonIcons ios7CloseOutlineIconWithSize:24];
    [deleteIcon addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]];
    UIImage *deleteIconImage = [deleteIcon imageWithSize:(CGSize){35, 25}];
    
    UIButton *actionButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 25, deleteIconImage.size.width, deleteIconImage.size.height)];
    [actionButton setBackgroundImage:deleteIconImage forState:UIControlStateNormal];
    actionButton.showsTouchWhenHighlighted = YES;
    
    MGBox *imgBox = [MGBox boxWithSize:(CGSize){35, 50}];
    [imgBox addSubview:settingsButton];
    [imgBox addSubview:actionButton];
    
    MGLine *nameLine = [MGLine lineWithLeft:stressTrack.name right:nil size:(CGSize){inputWidth, NAME_HEIGHT}];
    nameLine.font = [UIFont boldSystemFontOfSize:16];
    
    MGLine *boxLine = [MGLine lineWithSize:(CGSize){inputWidth, BOX_HEIGHT}];
    boxLine.contentLayoutMode = MGLayoutGridStyle;
    
    MGBox *mainBox = [MGBox boxWithSize:(CGSize){inputWidth, nameLine.height + boxLine.height}];
    mainBox.contentLayoutMode = MGLayoutTableStyle;
    [mainBox.boxes addObject:nameLine];
    [mainBox.boxes addObject:boxLine];
    
    FTCStressTrackLine *stressTrackLine = [FTCStressTrackLine lineWithLeft:mainBox right:imgBox size:(CGSize){totalWidth, mainBox.size.height}];
    stressTrackLine.stressTrack = stressTrack;
    stressTrackLine.delegate = delegate;
    stressTrackLine.lineWidth = totalWidth;
    stressTrackLine.nameLine = nameLine;
    stressTrackLine.boxLine = boxLine;
    stressTrackLine.mainBox = mainBox;
    
    //Initialize boxes
    NSMutableArray *stressBoxes = [[NSMutableArray alloc] init];
    for (int i = 0; i < [stressTrack.slots intValue]; i++) {
        FTCStressTrackBox *box = [FTCStressTrackBox stressTrackBoxWithIndex:i andState:[stressTrack isStressBoxUsed:i] enabled:YES delegate:stressTrackLine];
        [stressBoxes addObject:box];
    }
    
    //Only show disables stress boxes if user setting is enabled
    if ([FTCUserSettings showDisabledStressBoxes])
    {
        for (int i = [stressTrack.slots intValue]; i < MAX_STRESS_BOXES; i++) {
            FTCStressTrackBox *box = [FTCStressTrackBox stressTrackBoxWithIndex:i andState:NO enabled:NO delegate:stressTrackLine];
            [stressBoxes addObject:box];
        }
    }
    [boxLine setLeftItems:stressBoxes];
    //[boxLine layout];
    
    [actionButton bk_addEventHandler:^(id sender) {
        NSString *message = [NSString stringWithFormat:@"Are you sure you want to remove your %@ stress track?", stressTrackLine.stressTrack.name];
        
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Remove Stress Track?" andMessage:message];
        //alertView.messageFont = [UIFont systemFontOfSize:24];
        
        [alertView addButtonWithTitle:@"Cancel"
                                 type:SIAlertViewButtonTypeDefault
                              handler:nil];
        [alertView addButtonWithTitle:@"Remove" type:SIAlertViewButtonTypeDestructive handler:^(SIAlertView *alert) {
            [stressTrackLine.delegate stressTrackWasRemoved:stressTrackLine];
        }];
        alertView.transitionStyle = SIAlertViewTransitionStyleFade;
        
        [alertView show];
    } forControlEvents:UIControlEventTouchUpInside];
    
    [settingsButton bk_addEventHandler:^(id sender) {
        [stressTrackLine.delegate stressTrackWasTapped:stressTrackLine];
    } forControlEvents:UIControlEventTouchUpInside];
    
    [stressTrackLine layout];
    return stressTrackLine;
}

- (void) setup
{
    [super setup];
    self.topMargin = 4;
    self.bottomPadding = 8;
    self.bottomMargin = 8;
    
    self.borderStyle = MGBorderEtchedBottom;
    self.bottomBorderColor = [UIColor blackColor];
    self.rasterize = YES;
}

- (void) layout {
    [self setSize:(CGSize){self.lineWidth, NAME_HEIGHT + BOX_HEIGHT}];
    double inputWidth = [FTCStressTrackLine getInputWidth:self.lineWidth];
    [self.nameLine setSize:(CGSize){inputWidth, NAME_HEIGHT}];
    [self.boxLine setSize:(CGSize){inputWidth, BOX_HEIGHT}];
    [self.mainBox setSize:(CGSize){inputWidth, NAME_HEIGHT + BOX_HEIGHT}];
    [super layout];
}

@end
