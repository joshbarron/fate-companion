//
//  FTCNewsHelper.h
//  Fate Companion
//
//  Created by Joshua Barron on 4/5/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FTCNewsHelper : NSObject

+ (FTCNewsHelper *) sharedNewsHelper;

- (void) getNewsEntriesWithSuccess:(void (^)(NSArray *newsEntries))success andFailure:(void (^)(NSError *error))failure;

@end
