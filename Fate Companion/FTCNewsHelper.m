//
//  FTCNewsHelper.m
//  Fate Companion
//
//  Created by Joshua Barron on 4/5/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCNewsHelper.h"
#import "AFNetworking.h"
#import "FTCNewsStory.h"

@interface FTCNewsHelper()
{
    NSString *newsURL;
}

@end
@implementation FTCNewsHelper

+ (FTCNewsHelper *) sharedNewsHelper
{
    static FTCNewsHelper *_sharedInstance = nil;
    
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[FTCNewsHelper alloc] init];
    });
    
    return _sharedInstance;
}

- (id) init
{
    if (self = [super init])
    {
        newsURL = @"http://barronsoftware.com/fatecompanion/api/news";
    }
    return self;
}

- (NSDate *)dateFromString:(NSString *)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // this is imporant - we set our input date format to match our input string
    // if format doesn't match you'll get nil from your string, so be careful
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromString = [[NSDate alloc] init];
    // voila!
    dateFromString = [dateFormatter dateFromString:dateString];
    
    return dateFromString;
}

- (NSArray *)convertJSONToNewsEntries:(id)jsonEntries
{
    NSMutableArray *newsEntries = [[NSMutableArray alloc] init];
    
    for (id entry in jsonEntries) {
        NSString *title = [entry objectForKey:@"title"];
        NSString *body = [entry objectForKey:@"body"];
        NSString *posted = [entry objectForKey:@"posted"];
        NSDate *postedDate = [self dateFromString:posted];
        
        NSLog(@"%@ %@ %@", title, body, posted);
        
        FTCNewsStory *story = [[FTCNewsStory alloc] initWithTitle:title andBody:body andPostedDate:postedDate];
        [newsEntries addObject:story];
    }
    
    return newsEntries;
}

- (void) getNewsEntriesWithSuccess:(void (^)(NSArray *newsEntries))success andFailure:(void (^)(NSError *error))failure
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:newsURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSArray *newsEntries = [self convertJSONToNewsEntries:[responseObject objectForKey:@"news_entries"]];
        
        success(newsEntries);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        failure(error);
    }];
}

@end
