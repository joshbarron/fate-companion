//
//  FTCAspect.m
//  Fate Companion
//
//  Created by Joshua Barron on 2/17/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import "FTCAspect.h"

@implementation FTCAspect
@synthesize aspectText = _aspectText;

-(id) initWithText:(NSString *)aspectText
{
    if (self = [super init])
    {
        self.aspectText = aspectText;
    }
    return self;
}


#pragma mark NSCoding

#define kAspectVersionKey @"AspectVersion"
#define kAspectTextKey @"AspectText"

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeInt:1 forKey:kAspectVersionKey];
    [encoder encodeObject:self.aspectText forKey:kAspectTextKey];
}

- (id)initWithCoder:(NSCoder *)decoder {
    [decoder decodeIntForKey:kAspectVersionKey];
    NSString *aspectText = [decoder decodeObjectForKey:kAspectTextKey];
    
    return [self initWithText:aspectText];
}

@end
