//
//  FTCNewsStory.h
//  Fate Companion
//
//  Created by Joshua Barron on 4/5/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FTCNewsStory : NSObject

@property (strong) NSString *title;
@property (strong) NSString *body;
@property (strong) NSDate *posted;

- (id) initWithTitle:(NSString *)title andBody:(NSString *)body andPostedDate:(NSDate *)posted;
- (NSString *)mushedBody;
- (NSString *)postedDateString;

@end
