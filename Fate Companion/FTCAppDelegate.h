//
//  FTCAppDelegate.h
//  Fate Companion
//
//  Created by Joshua Barron on 1/25/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FTCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong) UIPopoverController *menuPopover;
@property (strong) UIBarButtonItem *menuNavItem;

@end
