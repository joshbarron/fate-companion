//
//  Fate_CompanionTests.m
//  Fate CompanionTests
//
//  Created by Joshua Barron on 1/25/14.
//  Copyright (c) 2014 Joshua Barron. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface Fate_CompanionTests : XCTestCase

@end

@implementation Fate_CompanionTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
